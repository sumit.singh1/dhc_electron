import Client from 'ftp';
import { find } from 'lodash';
import { createReadStream } from 'fs';
import { IncomingForm } from 'formidable';
import { Router } from 'express';
let router = Router();
import { waterfall } from 'async';

import { STATUS_CODES, RESPONSE_MESSAGES } from '../utils/constants';

import { ensureAuthenticated } from './common';
import { getClient, rename, getOrCreateFolder, destroyClient } from '../utils/ftpClient';
import { getTaskNumber } from '../utils/common';

function haltOnTimedout(req, res, next) {
    req.connection.setTimeout(30 * 60 * 1000);
    next();
}

router.post('/rename', ensureAuthenticated, async (req, res) => {
    try {
        const { oldPath, newPath } = req.body;
        console.log(oldPath, newPath)
        const client = await getClient().catch(e => { throw e });
        const result = await rename(client, oldPath, newPath);
        destroyClient(client);
        res.json(result);

    } catch (e) {
        console.log('Error in post ftp/rename API', e);
        res.status(500).send(e.message)
    }
});

router.post('/initial-setup', ensureAuthenticated, async (req, res) => {
    try {
        const { path } = req.body;
        const client = await getClient().catch(e => { throw e });
        const result = await getOrCreateFolder(client, process.env.FTP_BASE_FOLDER, path);
        destroyClient(client);
        res.json(result);

    } catch (e) {
        console.log('Error in post ftp/rename API', e);
        res.status(500).send(e.message)
    }
});

// router.post('/folder', ensureAuthenticated, function (req, res) {
//     let path = process.env.FTP_BASE_FOLDER + req.body.path;
//     let folderName = req.body.folderName;
//     getClient(function (client) {
//         getOrCreateFolder(client, path, folderName, function (err) {
//             if (!err) {
//                 res.json({
//                     ok: true
//                 });
//             } else {
//                 console.log(err);
//                 res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
//             }
//             destroyClient(client);
//         });
//     });
// });

// router.put('/file/upload', ensureAuthenticated, haltOnTimedout, function (req, res) {
//     let form = new IncomingForm();
//     form.parse(req, function (err, fields, files) {
//         if (req.timedout) {
//             console.log(err);
//             res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ATTACHMENT_UPLOAD.REQUEST_TIMEOUT);
//         }
//         if (!err) {
//             getClient(function (client) {
//                 let dest = fields.dest;
//                 let file = files.file;

//                 uploadFile(client, createReadStream(file.path), dest, function (err) {
//                     if (req.timedout) {
//                         console.log(err);
//                         res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ATTACHMENT_UPLOAD.REQUEST_TIMEOUT);
//                     }
//                     if (!err) {
//                         res.json({
//                             ok: true
//                         });
//                     } else {
//                         console.log(err);
//                         res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
//                     }
//                 });
//             });
//         } else {
//             console.log(err);
//             res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
//         }
//     });
// });

// router.post('/file/download', function (req, res) {
//     getClient(function (client) {
//         downloadFile(client, req.body.filePath, function (err, stream) {
//             if (!err) {
//                 res.attachment(req.body.fileName);
//                 stream.pipe(res);
//             } else {
//                 console.log(err);
//                 res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
//             }
//         });
//     });
// });

// router.post('/delete', ensureAuthenticated, function (req, res) {
//     getClient(function (client) {
//         deleteFile(client, req.body.filePath, function (err) {
//             if (!err) {
//                 res.json({
//                     ok: true
//                 });
//             } else {
//                 console.log(err);
//                 res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
//             }
//         });
//     });
// });

export default router;
