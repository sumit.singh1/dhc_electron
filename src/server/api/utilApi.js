import { Router } from 'express';
import moment from 'moment';

import Scope from '../models/Scope';
import ItemType from '../models/ItemType';
import User from '../models/User';
import { createScopeKeyword, createKeyword } from '../utils/milestonesCreate';
import { userRoleLocationPopulation } from '../utils/populations.js';

let router = Router();

router.get('/create-scope-highlights', async (req, res) => {
    try {
        let users = await User.find().lean().exec().catch(e => { throw e });
        let promises = [];
        users.forEach(({ scopeHighlights, _id }) => {
            let obj = {};
            scopeHighlights.forEach(scopeHighlight => {
                obj[scopeHighlight.scope] = scopeHighlight;
            });

            Object.values(obj).forEach(scopeHighlight => {
                console.log(scopeHighlight.color, scopeHighlight.scope, _id);
                promises.push(Scope.findOneAndUpdate(
                    { _id: scopeHighlight.scope },
                    {
                        $push: {
                            highlights: {
                                color: scopeHighlight.color,
                                user: _id
                            }
                        }
                    }
                ).exec());
            })
        });

        const result = await Promise.all(promises);
        res.json(result)
    } catch (e) {
        console.log('Error in tasks/:id/comment post API', e);
        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/update-scopes-keywords', async (req, res) => {
    try {
        const { fromYear, toYear } = req.body;
        const startDate = new Date(moment([fromYear]).startOf('year'));
        const endDate = new Date(moment([toYear]).endOf('year'));

        const query = {
            dueDate: { $gte: startDate, $lte: endDate },
            parent: { $exists: false }
        };

        const scopes = await Scope.find(query).lean().exec().catch(e => { throw e });
        const promises = [];
        scopes.map(item =>
            promises.push(
                createScopeKeyword(item.itemType).then(scopeKeyword =>
                    Scope.findOneAndUpdate({ _id: item._id }, { scopeKeywords: [scopeKeyword._id] }, { new: true }).lean().exec()
                )
            ));

        const updatedScopes = await Promise.all(promises).catch(e => { throw e });

        res.json(updatedScopes.map(({ _id, scopeKeyword }) => ({ _id, scopeKeyword })))

    } catch (error) {
        console.log('error', error);
        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);

    }
});

router.post('/update-multiple', (req, res) => {
    const users = req.body;
    Promise.all(users.map(user => User.findOneAndUpdate({ _id: user._id }, user).populate(userRoleLocationPopulation).exec().catch(e => { throw e; })))
        .then(updatedUsers => {
            res.json(users);
        }).catch(error => {
            res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
        })
});

router.get('/create-itemtype-keywords', async (req, res) => {
    try {
        const allCustomerKeyword = await createKeyword({ title: 'All Customers' })

        let promises = Object.keys(keywordsObj).map(async id => {
            let keywords = await createKeywordsForItemtype(id);
            return ItemType.findOneAndUpdate({ id }, { keywords: [allCustomerKeyword, ...keywords] }, { new: true }).catch(e => { throw e });
        });

        let results = await Promise.all(promises).catch(e => { throw e });
        res.json(results);
    } catch (error) {
        console.log('error in post /create-itemtype-keywords api', error);
        res.status(STATU.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
    }
});

const keywordsObj = {
    'HkebO_kkb': [  // 1. Bridge Platforms/Containment
        'QuikDeck',
        'Safespan',
        'Overstamp',
        'Contained',
        'Ventilation',
        'Str. Check'
    ],
    'By7zOdJyW': [  // 2.  Building Shoring-no scaffold
        'Post Shore',
        'Jacking',
        'Re-Shore',
        'Timber',
        'Steel',
        'Concrete'
    ],
    'BkTGOdyk-': [  // 3. Equipment Fabrication
        'Slide Rail',
        'Shields',
        'Aluminum',
        'Arch',
        'Tab Data',
        'Canada'
    ],
    'BJKmdukyb': [  // 4. Excavation Safety (open cut)
        'Clay',
        'Sand',
        'Rock',
        'Mixed Layer',
        'Surcharge',
        'Vertical'
    ],
    'H1xVO_kkZ': [  // 5. Excavation Shoring
        'Slide Rail',
        'Soldier Pile',
        'Sheet Pile',
        'Railroad',
        'Deflection',
        'Tie-Back'
    ],
    'SkFN_Oyy-': [  // 6. Falsework / Formwork
        'Bridge',
        'Wall',
        'Overhang',
        'Box Girder',
        'Traffic',
        'CalTrans'
    ],
    'ryMS_Oyk-': [  // 7. Foundation Design
        'Spread',
        'Piles',
        'Underpin.',
        'Seismic',
        'Concrete',
        'Anchorage',
    ],
    'H118duJkZ': [  // 8. Jacking - Bridges / Girders
        'Hyd. Jacks',
        'Shoring',
        'Foundation',
        'Str. Check',
        'Seismic',
        'Wind',
    ],
    'rJP8udyJW': [  // 9. Miscellaneous-Permant Works
        'Concrete',
        'Steel',
        'Timber',
        'Aluminum',
        'Buried',
        'Pipe',
    ],
    'BJyDdu11-': [  // 10. Miscellaneous-Temp Works
        'Traffic Deck',
        'Sound Wall',
        'Pipe Support',
        'Hoist',
        'Wall Brace',
        'Bypass'
    ],
    'r19Dddy1-': [  // 11. Reimbursable Expenses
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
    ],
    'rJMuudkJW': [  // 12. Retaining Walls-Permanent
        'Concrete',
        'Masonry',
        'Tie-Back',
        'Piles',
        'Cantilever',
        'Soil Nail'
    ],
    'rJtdu_yJW': [  // 13. Roof Mounted Equipment
        'Davit',
        'Socket',
        'Pedestal',
        'HLL',
        'BMU',
        'Str. Check',
    ],
    'HymtOdJ1Z': [  // 14. Scaffold-Access & Non Shore
        'Stair Tower',
        'Ramp',
        'Boiler',
        'Canopy',
        'Cantilever',
        'OSHPD',
    ],
    'rJ5YOdJ1-': [  // 15. Scaffold - Shoring
        'Jacking',
        'Post-Shore',
        'Needle',
        'Reshore',
        'Equipment',
        'Multi-Level'
    ],
    'SySid_1y-': [  // 16. Seismic Anchorage
        'Tank',
        'Sloshing',
        'Cracked C.',
        'Adhesive',
        'CIP',
        'Unistrut'
    ],
    'HkjjO_kyW': [  // 17. SWPPP
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA'
    ],
    'SJEh_O11-': [  // 18. Traffic Control Plans
        'NA',
        'NA',
        'NA',
        'NA',
        'NA',
        'NA'
    ],
    'rkj3dd11b': [  // 19. Cranes & Rigging
        'Crane Pad',
        'Pick Plan',
        'Spreader',
        'Tipping',
        'Hoist',
        'Lift Device'
    ],
    'HkSpOuJ1b': [  // 20. Demolition
        'Building',
        'Bridge',
        'Concrete',
        'Timber',
        'Str. Check',
        'Equip Load'
    ],
    'HkVAOukJ-': [  // 21. Residential
        'Beams',
        'Footings',
        'Retain. Wall',
        'Shear Walls',
        'Timber',
        'Concrete'
    ],
    'r1xkK_1kb': [  // 22. Rebar Cage Support
        'Cable',
        'Braced',
        'Freestand.',
        'Cage Check',
        'Deadmen',
        'Soil Anchor'
    ],
    'ryn6ddk1b': [  // 23. Tunneling, pipe design and
        'Thrust Block',
        'Jack Force',
        'Hobas',
        'Casing',
        'Timber',
        'Settlement'
    ],
    'rkDJtOJ1b': [  // 24. Dewatering
        'Bypass',
        'Wellpoint',
        'Deep Well',
        'Overstamp',
        'Shoring',
        'Flow Net'
    ],
    'ryCAfBuob': [  // 25. DHC Task
        'PLP',
        'IT',
        'Social M.',
        'Blog',
        'Web',
        'Marketing'
    ]
}


function createKeywordsForItemtype(id) {
    const promises = keywordsObj[id].map(item => createKeyword({ title: item }));
    return Promise.all(promises).catch(e => { throw e })
}

export default router;
