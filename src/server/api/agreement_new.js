import express from 'express';

import Agreement from '../models/Agreement';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { addTaskMilestone, createAgreement } from '../utils/milestonesCreate';
import { selectedScopePopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common, { ensureAuthenticated } from './common';

const router = express.Router();


router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const agreement = await Agreement.findOne({ id }).populate(selectedScopePopulation).exec();
        res.json(agreement);
    } catch (e) {
        console.log('Error in get agreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { agreement, taskId } = req.body;
    console.log(agreement)
    try {
        const result = await Agreement
            .findOneAndUpdate({ id }, agreement, { new: true, select: generateSelection(agreement) })
            .populate(selectedScopePopulation).exec();
        res.json(result);
        emitScoket(req, socketType.UPDATE_AGREEMENT, result);
    } catch (e) {
        console.log('Error in put agreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const agreement = await Agreement
            .findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) })
            .exec();
        res.json(agreement);
        emitScoket(req, socketType.ARCHIVE_AGREEMENT, agreement);
    } catch (e) {
        console.log('Error in put agreement/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', ensureAuthenticated, async (req, res) => {
    const { taskId, agreement } = req.body;
    try {
        const result = await createAgreement(taskId, agreement, true);
        const data = await Agreement.findById(result._id).populate(selectedScopePopulation).exec();
        const task = await addTaskMilestone('agreements', taskId, data)
        res.json(task);
        emitScoket(req, socketType.ADD_AGREEMENT, task);
    } catch (e) {
        console.log('Error in post agreement/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        await Agreement.findOneAndRemove({ id }).exec().catch(e => { throw e.message });
        res.json({ id });
        emitScoket(req, socketType.DELETE_AGREEMENT, { id });
    } catch (e) {
        console.log('Error in delete agreement/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
