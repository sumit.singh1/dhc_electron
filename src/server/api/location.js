var _ = require('lodash');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var Location = require('../models/Location');

//common
var common = require('./common');

var error = constants.RESPONSES;

function getLocation(query, population, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    Location.findOne(query)
        .populate(population)
        .exec(function(err, location) {
            if (!err) {
                callback(null, location);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function getLocations(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    Location.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .lean()
        .exec(function(err, locations) {
            if (!err) {
                callback(null, locations);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.post('/', common.ensureAuthenticated, function(req, res) {
    var payload = req.body;
    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_PERSONAL_UPDATE.NAME, constants.ACCESS_LEVELS.WRITE, function() {
        var location = new Location({
            name: payload.name,
            icon: payload.icon,
            iconColor: payload.iconColor
        });

        location.save(function(err, savedLocation) {
            if (!err) {
                res.json(savedLocation);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        })
    });
});

router.put('/:locationId', common.ensureAuthenticated, function(req, res) {
    var locationId = req.params.locationId;
    var params = req.body;
    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_PERSONAL_UPDATE.NAME, constants.ACCESS_LEVELS.WRITE, function() {
        Location.findOneAndUpdate({
            id: locationId
        }, params, {
            new: true
        }, function(err, updatedLocation) {
            if (!err) {
                res.json(updatedLocation);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        })
    });
});

router.get('/search/:query', common.ensureAuthenticated, function(req, res) {
    var query = {
        name: {
            '$regex': '.*' + req.params.query + '.*',
            '$options': 'i'
        }
    };

    getLocations(query, null, null, null, function(err, locations) {
        if (!err) {
            res.json(locations);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function(req, res) {
    getLocations(null, null, null, null, function(err, locations) {
        if (!err) {
            res.json(locations);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});


module.exports = router;
