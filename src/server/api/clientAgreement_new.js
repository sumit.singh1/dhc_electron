import express from 'express';

import ClientAgreement from '../models/ClientAgreement';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { addTaskMilestone, createClientAgreement } from '../utils/milestonesCreate';
import { emitScoket, socketType } from '../utils/socket';
import common, { ensureAuthenticated } from './common';
import { selectedScopePopulation } from '../utils/populations';

const router = express.Router();


router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const clientAgreement = await ClientAgreement.findOne({ id }).populate(selectedScopePopulation).exec();
        res.json(clientAgreement);
    } catch (e) {
        console.log('Error in get clientAgreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { clientAgreement, taskId } = req.body;
    try {
        const result = await ClientAgreement
            .findOneAndUpdate({ id }, clientAgreement, { new: true, select: generateSelection(clientAgreement) })
            .populate(selectedScopePopulation).exec();
        res.json(result);
        emitScoket(req, socketType.UPDATE_CLIENT_AGREEMENT, result);
    } catch (e) {
        console.log('Error in put clientAgreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const clientAgreement = await ClientAgreement
            .findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) })
            .exec();
        res.json(clientAgreement);
        emitScoket(req, socketType.ARCHIVE_CLIENT_AGREEMENT, clientAgreement);
    } catch (e) {
        console.log('Error in put clientAgreement/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', ensureAuthenticated, async (req, res) => {
    const { taskId, clientAgreement } = req.body;
    try {
        const result = await createClientAgreement(taskId, clientAgreement);
        const data = await ClientAgreement.findById(result._id).populate(selectedScopePopulation).exec();
        const task = await addTaskMilestone('clientAgreements', taskId, data)
        res.json(task);
        emitScoket(req, socketType.ADD_CLIENT_AGREEMENT, task);
    } catch (e) {
        console.log('Error in post clientAgreement/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        await ClientAgreement.findOneAndRemove({ id }).exec().catch(e => { throw e.message });
        res.json({ id });
        emitScoket(req, socketType.DELETE_CLIENT_AGREEMENT, { id });
    } catch (e) {
        console.log('Error in delete clientAgreement/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
