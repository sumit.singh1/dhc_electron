import express from 'express';

import CustDrawing from '../models/CustDrawing';

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { generateSelection } from '../utils/common';
import { createCustDrawing } from '../utils/milestonesCreate';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const custDrawing = await createCustDrawing();
        res.json(custDrawing);
        emitScoket(req, socketType.ADD_CUST_DRAWING, custDrawing);
    } catch (e) {
        console.log('Error in custDrawing/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, custDrawing } = req.body;
        const result = await CustDrawing
            .findOneAndUpdate({ id }, custDrawing, { new: true, select: generateSelection(custDrawing) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_CUST_DRAWING, { taskId, custDrawing: result });
    } catch (e) {
        console.log('Error in custDrawing/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, custDrawing } = req.body;
        const result = await CustDrawing
            .findOneAndUpdate({ id }, custDrawing, { new: true, select: generateSelection(custDrawing) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_CUST_DRAWING, { taskId, custDrawing: result });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const custDrawing = await CustDrawing.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_CUST_DRAWING, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
