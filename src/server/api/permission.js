var _ = require('lodash');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var Permission = require('../models/Permission');

//common
var common = require('./common');

var error = constants.RESPONSES;

function getPermission(query, population, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    Permission.findOne(query)
        .populate(population)
        .exec(function(err, permission) {
            if (!err) {
                callback(null, permission);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function getPermissions(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    Permission.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .exec(function(err, permissions) {
            if (!err) {
                callback(null, permissions);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.get('/search/:query', common.ensureAuthenticated, function(req, res) {
    var query = {
        isActive: true,
        $or: [{
            name: {
                '$regex': '.*' + req.params.query + '.*',
                '$options': 'i'
            }
        }, {
            description: {
                '$regex': '.*' + req.params.query + '.*',
                '$options': 'i'
            }
        }]
    };

    getPermissions(query, null, null, null, function(err, permissions) {
        if (!err) {
            res.json(permissions);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/', function(req, res) {
    var permission = new Permission({
        name: req.body.name,
        description: req.body.description
    });

    permission.save(function(err, savedPermission) {
        if (!err) {
            res.json(savedPermission);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function(req, res) {
    getPermissions(null, null, null, null, function(err, permissions) {
        if (!err) {
            res.json(permissions);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

module.exports = router;
