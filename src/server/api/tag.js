import { Router } from 'express';
import { emitScoket, socketType } from '../utils/socket';

// constants
import {STATUS_CODES, RESPONSE_MESSAGES } from '../utils/constants';

import Tag, { find } from '../models/Tag';
import { ensureAuthenticated } from './common';

let router = Router();

router.post('/', ensureAuthenticated, async (req, res) => {
    try {
        const newTag = await new Tag({ name: req.body.name }).save().catch(e => { throw e });
        res.json(newTag);
        emitScoket(req, socketType.ADD_TAG, newTag);
    } catch (e) {
        console.log('Error in tasks/:id/comment post API', e);
        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/:searchQuery', ensureAuthenticated, function (req, res) {
    find({
        name: {
            $regex: '.*' + req.params.searchQuery + '.*',
            $options: 'i'
        }
    }, function (err, tags) {
        if (!err) {
            res.json(tags);
        } else {
            console.log(err);
            res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
        }
    });
});

export default router;
