import { Router } from 'express';
import { find, isEmpty } from 'lodash';

import Role from '../models/Role';
import { ACCESS_LEVELS, PERMISSIONS, RESPONSE_MESSAGES, SOCKET_EVENTS, STATUS_CODES } from '../utils/constants';
import { ensureAuthenticated, hasPermission } from './common';

let router = Router();

function getRoles(query, population, sort, limit, callback) {
    if (isEmpty(query)) {
        query = {

        };
    }
    if (isEmpty(population)) {
        population = '';
    }
    if (isEmpty(sort)) {
        sort = '';
    }

    Role.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .exec(function (err, roles) {
            if (!err) {
                callback(null, roles);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.post('/', ensureAuthenticated, function (req, res) {
    let name = req.body.name;
    let population = [{
        path: 'workload'
    }];
    hasPermission(req, res, PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, ACCESS_LEVELS.WRITE, function () {
        let role = new Role({
            name: name
        });

        role.save(function (err, savedRole) {
            if (!err) {
                savedRole.populate(population, function (populateErr, populatedRole) {
                    if (!populateErr) {
                        res.json(populatedRole);
                    } else {
                        console.log(err);
                        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log(err);
                res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.get('/search/:query', ensureAuthenticated, function (req, res) {
    let query = {
        name: {
            $regex: '.*' + req.params.query + '.*',
            $options: 'i'
        }
    };

    getRoles(query, null, null, null, function (err, roles) {
        if (!err) {
            res.json(roles);
        } else {
            console.log(err);
            res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', ensureAuthenticated, function (req, res) {
    let population = [{
        path: 'workload'
    }];
    getRoles(null, population, null, null, function (err, roles) {
        if (!err) {
            res.json(roles);
        } else {
            console.log(err);
            res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/:roleId', ensureAuthenticated, function (req, res) {
    let roleId = req.params.roleId;
    let body = req.body;
    let population = [{
        path: 'workload'
    }];
    let io = req.app.get('socketio');
    let socket = find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    hasPermission(req, res, PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, ACCESS_LEVELS.WRITE, function () {
        Role.findOneAndUpdate({
            id: roleId
        },
        body,
        {
            new: true
        }).populate(population).exec(function (err, updatedRole) {
            if (!err) {
                if (socket) {
                    socket.broadcast.emit(SOCKET_EVENTS.ROLE_UPDATE, { role: updatedRole });
                }
                res.json(updatedRole);
            } else {
                console.log(err);
                res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.put('/groupstatus/:roleId', ensureAuthenticated, function (req, res) {
    let roleId = req.params.roleId;
    let params = req.body;
    let population = [{
        path: 'workload'
    }];
    let io = req.app.get('socketio');
    let socket = find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    hasPermission(req, res, PERMISSIONS.MANAGE_GROUP_STATUS.NAME, ACCESS_LEVELS.WRITE, function () {
        Role.findOneAndUpdate({
            id: roleId
        }, params, {
            new: true
        }).populate(population).exec(function (err, updatedRole) {
            if (!err) {
                if (socket) {
                    socket.broadcast.emit(SOCKET_EVENTS.ROLE_UPDATE, { role: updatedRole });
                }
                res.json(updatedRole);
            } else {
                console.log(err);
                res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

export default router;
