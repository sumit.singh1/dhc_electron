import express from 'express';

import PurchaseOrder from '../models/PurchaseOrder';
import constants from '../utils/constants';
import { addTaskMilestone, createPurchaseOrder } from '../utils/milestonesCreate';
import { selectedScopePopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common from './common';

const router = express.Router();

const population = [
    {
        path: 'selectedScopes.scope',
        select: 'id _id price note number definition isArchived'
    }
];

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const { taskID, purchaseOrder } = req.body;
        const result = await createPurchaseOrder(taskID, purchaseOrder).catch(e => { throw e });
        const data = await PurchaseOrder.findById(result._id).populate(selectedScopePopulation).exec().catch(e => { throw e });
        const task = await addTaskMilestone('purchaseOrders', taskID, data);
        res.json(task);
        emitScoket(req, socketType.ADD_PURCHASE_ORDER, task);
    } catch (e) {
        console.log('Error in post purchaseOrder/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, purchaseOrder } = req.body;
        const result = await PurchaseOrder
            .findOneAndUpdate({ id }, purchaseOrder, { new: true })
            .populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_PURCHASE_ORDER, { taskId, purchaseOrder: result });
    } catch (e) {
        console.log('Error in purchaseOrder/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { purchaseOrder, taskId } = req.body;
    try {
        const result = await PurchaseOrder
            .findOneAndUpdate({ id }, purchaseOrder, { new: true })
            .populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_PURCHASE_ORDER, { taskId, purchaseOrder: result });
    } catch (e) {
        console.log('Error in put purchaseOrder/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const purchaseOrder = await PurchaseOrder.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_PURCHASE_ORDER, { id });
    } catch (e) {
        console.log('Error in delete purchaseOrder/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
