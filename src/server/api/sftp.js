var _ = require('lodash');
var Client = require('ssh2').Client;
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var AppSetting = require('../models/AppSetting');

//others
var common = require('./common');

var connSettings = {
    host: 'ftp.charlesengineering.com',
    username: 'swftpuser',
    password: '123erd456!',
    port: 990,
    algorithms: {
        serverHostKey: ['ssh-rsa', 'ssh-dss']
    }
};

function createConnection(callback) {
    var conn = new Client();

    conn.on('ready', function () {
        callback(conn);
    }).connect(connSettings);
}

function closeConnection(conn) {
    conn.end();
}

function getListing(conn, path, callback) {
    conn.sftp(function (err, sftp) {
        if (!err) {
            sftp.readdir(path, function (err, list) {
                if (!err) {
                    callback(null, list);
                } else {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function createFolder(conn, path, callback) {
    conn.sftp(function (err, sftp) {
        if (!err) {
            sftp.mkdir(path, function (err) {
                if (!err) {
                    callback(null);
                } else {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function getOrCreateFolder(conn, path, folderName, callback) {
    var newPath = path + '/' + folderName;

    getListing(conn, path, function (err, list) {
        if (!err) {
            var exist = _.find(list, function (entity) {
                return entity.filename === folderName;
            });
            if (exist) {
                callback(null);
            } else {
                createFolder(conn, newPath, function (err) {
                    if (!err) {
                        callback(null);
                    } else {
                        callback(err);
                    }
                });
            }
        } else {
            callback(err);
        }
    });
}

function deleteFile(conn, path, callback) {
    conn.sftp(function (err, sftp) {
        if (!err) {
            sftp.unlink(path, function (err) {
                if (!err) {
                    callback(null);
                } else {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function rename(conn, oldPath, newPath, callback) {
    conn.sftp(function (err, sftp) {
        if (!err) {
            sftp.rename(oldPath, newPath, function (err) {
                if (!err) {
                    callback(null);
                } else {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function downloadFile(conn, path, callback) {
    conn.sftp(function (err, sftp) {
        if (!err) {
            sftp.readFile(path, function (err, buffer) {
                if (!err) {
                    callback(null, buffer);
                } else {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function uploadFile(conn, localPath, remotePath, callback) {
    conn.sftp(function (err, sftp) {
        if (!err) {
            sftp.fastPut(localPath, remotePath, function (err) {
                if (!err) {
                    callback(null);
                } else {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

module.exports = router;
