import express from 'express';

import ScopeKeyword from '../models/ScopeKeyword';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { createScopeKeyword } from '../utils/milestonesCreate';
import { emitScoket, socketType } from '../utils/socket';
import common from './common';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const scopeKeyword = await createScopeKeyword();
        res.json(scopeKeyword);
        emitScoket(req, socketType.ADD_SCOPE_KEYWORD, scopeKeyword);
    } catch (e) {
        console.log('Error in scopeKeyword/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, scopeKeyword } = req.body;
        const result = await ScopeKeyword
            .findOneAndUpdate({ id }, scopeKeyword, { new: true, select: generateSelection(scopeKeyword) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_SCOPE_KEYWORD, { taskId, scopeKeyword: result });
    } catch (e) {
        console.log('Error in scopeKeyword/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, scopeKeyword } = req.body;
        const result = await ScopeKeyword
            .findOneAndUpdate({ id }, scopeKeyword, { new: true })
            .populate([{
                path: 'customKeyword.keyword',
            }, {
                path: 'itemTypeKeywords.keyword',
            }])
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_SCOPE_KEYWORD, { taskId, scopeKeyword: result });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const scopeKeyword = await ScopeKeyword.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_SCOPE_KEYWORD, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
