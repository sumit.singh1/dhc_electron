import _ from 'lodash';
import express from 'express';
import mongoose from 'mongoose';
import PdfPrinter from 'pdfmake/src/printer';
import moment from 'moment';

import HourTracker from '../models/HourTracker';
import TimeInTimeOut from '../models/TimeInTimeOut';
import Scope from '../models/Scope';
import Task from '../models/Task';
import User from '../models/User';

import constants from '../utils/constants';
import { getClient, uploadFile } from '../utils/ftpClient';
import { hourTrackerPopulation, hourTrackerEmployeePopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import PaydayReport from '../models/PaydayReport';
import { getImageBuffer, ensureAuthenticated } from './common';


import { generateSelection, formattedDate } from '../utils/common';

const router = express.Router();

router.post('/', ensureAuthenticated, async (req, res) => {
    try {
        const { id } = await (new HourTracker(req.body)).save();
        const result = await HourTracker.findOne({ id }).populate(hourTrackerPopulation).lean().exec();
        res.json(result);
        emitScoket(req, socketType.ADD_HOUR_TRACKER, result);
    } catch (e) {
        console.log('Error in post hourtracker/ API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const hourtracker = await HourTracker.findOneAndUpdate({ id }, body, { new: true }).populate(hourTrackerPopulation).lean().exec();
        res.json(hourtracker);
        emitScoket(req, socketType.UPDATE_HOUR_TRACKER, { hourtracker, scopeId: body.scopeID || '', taskId: body.taskId });
    } catch (e) {
        console.log('Error in put hourtracker/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const hourTracker = await HourTracker.findOneAndRemove({ id }).exec();
        res.json({ id });
        emitScoket(req, socketType.DELETE_HOUR_TRACKER, { id });
    } catch (e) {
        console.log('Error in delete hourtracker/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const hourTracker = await HourTracker.findOne({ id }).exec();
        res.json(hourTracker);
    } catch (e) {
        console.log('Error in get hourtracker/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/user/:userId/:year/:month/:needYear', ensureAuthenticated, async (req, res) => {
    let month = parseInt(req.params.month);
    let year = parseInt(req.params.year);
    let startDate = new Date(moment([year, month]));
    let endDate = new Date(moment(startDate).endOf('month'));
    let needYear = req.params.needYear === 'true';
    let query = {
        employee: req.params.userId,
        date: { $gte: startDate, $lte: endDate }
    };
    let drafterAdminData;

    try {
        drafterAdminData = await TimeInTimeOut.find({
            user: req.params.userId,
            $and: [{
                date: {
                    $gte: moment(startDate).valueOf(),
                    $lt: moment(endDate).valueOf()
                }
            }]
        }).exec();
        const hourTrackers = await HourTracker.find(query).populate([{ path: 'employee', select: 'id _id firstName lastName picture' }]).lean();

        Promise.all(
            _.map(hourTrackers, hourTracker => {
                return new Promise(async resolve => {
                    const scope = await Scope.findOne({ hourTrackers: hourTracker._id }).select('number note id _id');
                    if (scope) {
                        hourTracker.scope = scope;
                        const task = await Task.findOne({ scopes: scope._id }).select('contractor title id _id taskNumber createdAt updatedAt isBidding isFromTaskGroup');
                        if (task) {
                            hourTracker.task = task;
                        }
                        resolve();
                    } else {
                        resolve();
                    }
                });
            })
        ).then(async () => {
            let oldestDoc;
            let latestDoc;
            if (needYear) {
                oldestDoc = await HourTracker.find({ employee: req.params.userId }).sort({ date: 1 }).limit(1).select('date');
                latestDoc = await HourTracker.find({ employee: req.params.userId }).sort({ date: -1 }).limit(1).select('date');
                res.json({ hourTrackers, drafterAdminData, oldestDate: oldestDoc[0] && oldestDoc[0].date, latestDate: latestDoc[0] && latestDoc[0].date });
            } else {
                res.json({ hourTrackers, drafterAdminData });
            }

        });
    } catch (hourTrackerError) {
        console.log('Error in get hourtracker/user/:userId/:year/:month API', hourTrackerError);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

function updateTimeInOutObject(item) {
    return TimeInTimeOut.findByIdAndUpdate(
        { _id: item._id },
        {
            timeIn: item.timeIn,
            timeOut: item.timeOut,
            lunchIn: item.lunchIn,
            lunchOut: item.lunchOut
        }
    ).exec();
}

function createTimeInOutObject(item) {
    return new TimeInTimeOut({
        user: mongoose.Types.ObjectId(item.userId),
        timeIn: item.timeIn,
        timeOut: item.timeOut,
        lunchIn: item.lunchIn,
        lunchOut: item.lunchOut,
        date: item.date
    }).save();
}

function addTimeInOutData(timeInOutArray) {
    const promises = timeInOutArray.map(item => {
        if (item._id) {
            return updateTimeInOutObject(item)
        }
        return createTimeInOutObject(item)
    });
    return Promise.all(promises);
}

function getDrafterRows(data, timeZoneOffset) {
    let totalRegular = 0,
        totalOvertime = 0,
        totalPto = 0;

    let rows = data.map((row, index) => {
        let {
            date,
            lunchIn,
            lunchOut,
            timeIn,
            timeOut,
            overtime,
            pto,
            regular
        } = row;

        // console.log('\n\Date: ',moment(date).format('MM/DD/YY'), '\tMoment Time in: ', moment(timeIn).format("hh:mm a"),
        // '\tMoment Lunch in: ', moment(lunchIn).format('hh:mm a'), '\tMoment Lunch out:', moment(lunchOut).format('hh:mm a'),
        // '\tMoment Time out:', moment(timeOut).format('hh:mm a'));

        //         console.log("Date", formattedDate(date, timeZoneOffset, 'MM/DD/YY'))
        //         console.log("timeIn", formattedDate(timeIn, timeZoneOffset, 'MM/DD/YY', 'hh:mm a'))
        //         console.log("lunchIn", formattedDate(lunchIn, timeZoneOffset, 'MM/DD/YY', 'hh:mm a'))
        //         console.log("timeOut", formattedDate(timeOut, timeZoneOffset, 'MM/DD/YY', 'hh:mm a'))
        //         console.log("lunchOut", formattedDate(lunchOut, timeZoneOffset, 'MM/DD/YY', 'hh:mm a'))

        // console.log('\n\Date: ',moment(formattedDate(date)).format('MM/DD/YY'), '\tTime in: ', new Date(timeIn).getHours(), ':', new Date(timeIn).getMinutes(),
        // '\tLunch in: ', new Date(lunchIn).getHours(), ':', new Date(lunchIn).getMinutes(), '\tLunch out:', new Date(lunchOut).getHours(), ':', new Date(lunchOut).getMinutes(),
        // '\tTime out:', new Date(timeOut).getHours(), ':', new Date(timeOut).getMinutes());
        totalOvertime += overtime;
        totalRegular += regular;
        totalPto += pto;
        return (
            [
                {
                    text: formattedDate(date, timeZoneOffset, 'MM/DD/YY'),
                    alignment: 'center'
                },
                {
                    text: formattedDate(date, timeZoneOffset, 'ddd'),
                    alignment: 'center'
                },
                {
                    text: formattedDate(timeIn, timeZoneOffset, "hh:mm a"),
                    alignment: 'center'
                },
                {
                    text: formattedDate(lunchIn, timeZoneOffset, 'hh:mm a'),
                    alignment: 'center'
                },
                {
                    text: formattedDate(lunchOut, timeZoneOffset, 'hh:mm a'),
                    alignment: 'center'
                },
                {
                    text: formattedDate(timeOut, timeZoneOffset, 'hh:mm a'),
                    alignment: 'center'
                },
                {
                    text: regular.toFixed(2),
                    alignment: 'center'
                },
                {
                    text: overtime.toFixed(2),
                    alignment: 'center'
                },
                {
                    text: pto.toFixed(2),
                    alignment: 'center'
                }
            ]
        );
    });
    rows.push(
        [
            { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: 'Totals', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: totalRegular.toFixed(2), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: totalOvertime.toFixed(2), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
            { text: totalPto.toFixed(2), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' }
        ], [
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: 'Total', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: (totalPto + totalOvertime + totalRegular).toFixed(2), alignment: 'center', bold: true, margin: [0, 0, 0, 0], color: '#9F3A38', fillColor: '#FFF6F6' }
        ]);
    console.log("pdf rows", rows);
    return rows;
}
function getPDFDocDefinitionObjectDrafter(drafterData, employeeCode, employeeName, signature, notes, timeZoneOffset) {
    let rows = getDrafterRows(drafterData, timeZoneOffset);
    let docDefinition = {
        pageMargins: [10, 10, 40, 10],
        pageSize: 'LETTER',
        content: [
            {
                text: '\n\nTimesheet',
                alignment: 'center',
                bold: true
            },
            {
                text: `Employee Id: ${employeeCode}`,
                alignment: 'right',
                textSize: '14px',
                margin: [0, 0, 0, 20]
            },
            {
                style: 'tableExample',
                color: '#444',
                table: {
                    widths: [60, 30, 60, 60, 70, 60, 50, 55, 50],
                    headerRows: 1,
                    body: [
                        [
                            { text: 'Date', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                            {},
                            { text: 'In', style: 'tableHeader', alignment: 'center' },
                            { text: 'Lunch (In)', style: 'tableHeader', alignment: 'center' },
                            { text: 'Lunch (Out)', style: 'tableHeader', alignment: 'center' },
                            { text: 'Out', style: 'tableHeader', alignment: 'center' },
                            { text: 'Regular', style: 'tableHeader', alignment: 'center' },
                            { text: 'Overtime', style: 'tableHeader', alignment: 'center' },
                            { text: 'PTO', style: 'tableHeader', alignment: 'center' }
                        ]
                    ].concat(rows)
                }
            }
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true,
                margin: [0, 0, 0, 10]
            },
            subheader: {
                fontSize: 16,
                bold: true,
                margin: [0, 10, 0, 5]
            },
            tableExample: {
                margin: [0, 5, 0, 15],
                alignment: 'center'
            },
            tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
            },
            values: rows
        }
    };

    docDefinition.content.push({
        alignment: 'left',
        margin: [20, 0, 0, 0],
        fontSize: 10,
        stack: [
            {
                text: 'Note: ',
                height: 100,
                width: 500,
                bold: true
            },
            `${notes}`
        ]

    });
    if (signature) {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    image: signature,
                    height: 40,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    } else {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    text: '',
                    height: 36,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    }
    return docDefinition;

}

function getValues(groups) {
    let weeklyTotal = { hours: 0, pto: 0, total: 0 };
    let sheetTotal = { hours: 0, pto: 0, total: 0 };
    const values = groups.map(entry => {
        if (entry.date === 'total') {
            let data = [
                { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: 'WEEK TOTAL', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: weeklyTotal.hours.toPrecision(3), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: weeklyTotal.pto.toPrecision(3), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: weeklyTotal.total.toPrecision(3), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' }
            ];

            sheetTotal.hours += weeklyTotal.hours;
            sheetTotal.pto += weeklyTotal.pto;
            sheetTotal.total += weeklyTotal.total;
            weeklyTotal = { hours: 0, pto: 0, total: 0 };
            return data;
        }

        let dt = moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSS');

        let day = dt.format('ddd');
        let date = dt.format('MM/D/YY');
        let hourArray = entry.data && entry.data.filter(item => item.other !== 'PTO').map(item => item.hoursSpent) || [];

        let hours = hourArray.length > 0 ? hourArray.reduce((hour1, hour2) => hour1 + hour2) : 0;

        let ptoArray = entry.data.filter(item => item.other === 'PTO').map(item => item.hoursSpent) || [];
        let pto = ptoArray.length > 0 || ptoArray[0] ? ptoArray.reduce((hour1, hour2) => hour1 + hour2) : 0;
        weeklyTotal.hours += hours;
        weeklyTotal.pto += pto;
        weeklyTotal.total = weeklyTotal.total + hours + pto;
        return (
            [
                { text: day, alignment: 'center' },
                { text: date, alignment: 'center' },
                { text: hours.toFixed(2), alignment: 'center' },
                { text: pto.toFixed(2), alignment: 'center' },
                { text: (hours + pto).toFixed(2), alignment: 'center' }
            ]
        );
    });
    values.push([
        { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: 'TOTALS', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: sheetTotal.hours.toFixed(2), alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: sheetTotal.pto.toFixed(2), alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: sheetTotal.total.toFixed(2), alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' }
    ]);
    return values;
}

function getPDFDocDefinitionObject(timeSheet, employeeCode, employeeName, signature, notes, timeZoneOffset) {
    let values = getValues(timeSheet);
    let docDefinition = {
        pageMargins: [40, 20, 40, 20],
        pageSize: 'LETTER',
        content: [
            {
                text: '\n\n\n\nTimesheet',
                alignment: 'center',
                bold: true
            },
            {
                text: `Employee Id: ${employeeCode}`,
                alignment: 'right',
                textSize: '14px',
                margin: [0, 0, 0, 20]
            },
            {
                style: 'tableExample',
                color: '#444',
                table: {
                    widths: ['*', '*', '*', '*', '*'],
                    headerRows: 1,
                    body: [
                        [
                            { text: 'Day', style: 'tableHeader', alignment: 'center' },
                            { text: 'Date', style: 'tableHeader', alignment: 'center' },
                            { text: 'Hours', style: 'tableHeader', alignment: 'center' },
                            { text: 'PTO', style: 'tableHeader', alignment: 'center' },
                            { text: 'Total', style: 'tableHeader', alignment: 'center' }
                        ]
                    ].concat(values)
                }
            }
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true,
                margin: [0, 0, 0, 10]
            },
            subheader: {
                fontSize: 16,
                bold: true,
                margin: [0, 10, 0, 5]
            },
            tableExample: {
                margin: [0, 5, 0, 15],
                alignment: 'center'
            },
            tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
            },
            values: values
        }
    };

    docDefinition.content.push({
        alignment: 'left',
        margin: [20, 0, 0, 0],
        fontSize: 13,
        stack: [
            {
                text: 'Note: ',
                height: 100,
                width: 500,
                bold: true
            },
            `${notes}`
        ]

    });
    if (signature) {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    image: signature,
                    height: 40,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    } else {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    text: '',
                    height: 36,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    }
    return docDefinition;
}
function getHourTrackersForDateRange({ startDate, endDate, userId }) {
    let query = {
        employee: userId,
        $and: [
            {
                date: {
                    $gte: startDate,
                    $lt: endDate
                }
            }
        ]

    };
    return HourTracker.find(query).lean().exec();
}

function createPdf(data, endDate, signature, employeeCode = 0, employeeName = '', notes = '', isManagerEngineer, timeZoneOffset) {

    return new Promise((resolve, reject) => {
        let fonts = {
            Roboto: {
                normal: './src/client/assets/fonts/Roboto-Regular.ttf',
                bold: './src/client/assets/fonts/Roboto-Medium.ttf',
                italics: './src/client/assets/fonts/Roboto-Italic.ttf',
                bolditalics: './src/client/assets/fonts/Roboto-MediumItalic.ttf'
            }
        };
        let printer = new PdfPrinter(fonts);
        let docDefinition;
        if (isManagerEngineer) {
            docDefinition = getPDFDocDefinitionObject(data, employeeCode, employeeName, signature, notes, timeZoneOffset);
        } else {
            docDefinition = getPDFDocDefinitionObjectDrafter(data, employeeCode, employeeName, signature, notes, timeZoneOffset);
        }
        let pdfDoc = printer.createPdfKitDocument(docDefinition);
        let chunks = [];
        pdfDoc.on('data', chunk => chunks.push(chunk));
        pdfDoc.on('end', () => resolve(Buffer.concat(chunks)));
        pdfDoc.on('error', () => reject());
        pdfDoc.end();
    });
}

function getDateRange(startDate, stopDate) {
    Date.prototype.addDays = function (days) {
        let dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };
    let dateArray = [];
    let currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate);
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function getPDFData(trackers, date) {
    const days = 13;
    const startDate = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate() - 1);
    const hourTrackerGrouped = [];
    const lastDate = new Date(startDate.getTime() - days * 24 * 60 * 60 * 1000);
    const twoWeekDatesArray = getDateRange(lastDate, startDate);
    twoWeekDatesArray.map((weekDate, index) => {
        const oneDayData = trackers.filter(hourTracker => weekDate.getDate() === new Date(hourTracker.date).getDate());
        if (index === 6) {
            hourTrackerGrouped.push({
                data: oneDayData,
                date: weekDate
            });
            hourTrackerGrouped.push({
                data: 'week one',
                date: 'total'
            });
        } else {
            hourTrackerGrouped.push({
                data: oneDayData,
                date: weekDate
            });
        }
    });

    hourTrackerGrouped.push({
        data: 'week two',
        date: 'total'
    });

    return hourTrackerGrouped;
}

function updateUserPayDayDate(_id) {
    let todayDate = new Date();
    todayDate.setHours(0, 0, 0, 0);
    return User.findOneAndUpdate({ _id }, { payDayFormDate: todayDate.getTime() }, { new: true, select: { id: 1, payDayFormDate: 1 } }).exec();
}
async function updatePaydayReportModal({ userId, note, endDate }) {
    const payDate = new Date(endDate);
    const data = await PaydayReport.findOne({
        employee: userId,
        payDate
    }).exec();
    if (data) {
        data.note = note;
        return PaydayReport.findOneAndUpdate({ id: data.id }, data).exec();
    }

    return new PaydayReport({
        note,
        payDate,
        employee: userId
    }).save();
}

router.post('/generate-payday-report', async (req, res) => {
    const {
        endDate,
        startDate,
        userId,
        note,
        timeInOutObject,
        isManagerEngineer,
        timeZoneOffset
    } = req.body;
    try {
        await addTimeInOutData(timeInOutObject).catch(e => { throw e });
        const trackers = await getHourTrackersForDateRange(req.body).catch(e => { throw e });
        console.log(trackers);
        const user = await User.findOne({ _id: userId }).exec().catch(e => { throw e });
        const data = await updatePaydayReportModal(req.body).catch(e => { throw e });
        const userName = user.firstName + ' ' + user.lastName;
        let signature = null;
        if (user && user.signature) {
            signature = await getImageBuffer(user.signature).catch(e => { throw e });
        }
        let pdfData = null;
        if (isManagerEngineer) {
            const group = getPDFData(trackers, endDate);
            pdfData = await createPdf(group,
                endDate,
                signature,
                user.employeeCode,
                userName,
                note,
                true,
                timeZoneOffset
            ).catch(e => { throw e });
        } else {
            pdfData = await createPdf(timeInOutObject,
                endDate,
                signature,
                user.employeeCode,
                userName,
                note,
                false,
                timeZoneOffset
            ).catch(e => { throw e });
        }

        if (!pdfData) {
            throw new Error('unable to create PDF');
        }


        const client = await getClient().catch(e => { throw e });
        const destination = `${process.env.FTP_TIMESHEET_FOLDER}_${user.employeeCode}-${user.firstName}-${user.lastName}-${moment(endDate, 'YYYY-MM-DDTHH:mm:ss.SSS').add(1, 'day').format('MM_D_YY')}.pdf`;
        await uploadFile(client, pdfData, destination).catch(e => { throw e });
        const result = await updateUserPayDayDate(userId).catch(e => { throw e });
        res.json(result);
    } catch (e) {
        console.log('Error in post hourtracker/generate-payday-report API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/update-pay-date', ensureAuthenticated, async (req, res) => {

    const {
        userId,
        timeInOutObject,
        isManagerEngineer,
        todaysTime
    } = req.body;

    try {

        const result = await updateUserPayDayDate(userId).catch(e => { throw e });
        if (!isManagerEngineer) {
            const arr = [];
            timeInOutObject[0] && arr.push(timeInOutObject[0]);
            todaysTime && arr.push(todaysTime)
            await addTimeInOutData(arr).catch(e => { throw e });
        }
        res.json(result);
    } catch (e) {
        console.log('Error in post hourtracker/update-pay-date API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
