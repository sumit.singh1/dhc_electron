import express from 'express';
import { validationResult } from 'express-validator/check';
import moment from 'moment';
import mongoose from 'mongoose';

import Invoice from '../models/Invoice';
import Scope from '../models/Scope';
import Task from '../models/Task';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { getCounter, incrementRevScopeCounter, incrementScopeCounter } from '../utils/counterUtils';
import {
    createCustDrawing,
    createLetter,
    createScope,
    createScopeKeyword,
    createScopePlanning,
    createTabData,
    createTaskGroupScope,
} from '../utils/milestonesCreate';
import {
    genetatePopulation,
    scopePlannings as scopePlanningsPopulation,
    scopePopulation,
    scopePopulationNew,
} from '../utils/populations';
import { defaultSelection, scopeByGroupSelection } from '../utils/selection';
import { emitScoket, socketType } from '../utils/socket';
import { validate } from '../utils/Validation';
import common from './common';

const router = express.Router();

function getGroupFilteredScopes(groupId, filters) {
    const startDate = new Date(moment([filters.fromYear]).startOf('year'));
    const endDate = new Date(moment([filters.toYear]).endOf('year'));
    let query = [
        {
            $match: {
                $and: [
                    {
                        dueDate: { $gte: startDate, $lte: endDate }
                    },
                    {
                        group: mongoose.Types.ObjectId(groupId)
                    }
                ]
            }
        },
        {
            $lookup: {
                from: 'tasks',
                localField: 'task',
                foreignField: '_id',
                as: 'task'
            }
        },
        {
            $match: {
                $and: []
            }
        }
    ];
    const SCOPE_MATCH_INDEX = 2;
    if (filters.contractors && filters.contractors.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            $or: [
                { 'task.contractor.name': { $in: filters.contractors.filter(item => !item.includes(' [All]')) } }
            ]
        });
        // As regex doesn't work with $in operator, used an extra $or.
        filters.contractors.filter(item => item.includes(' [All]')).forEach(title => {
            query[SCOPE_MATCH_INDEX].$match.$and[0].$or.push({
                'task.contractor.name': { $regex: new RegExp(title.split(' [All]')[0], 'i') }
            });
        });
    }

    if (filters.itemTypes && filters.itemTypes.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            'itemType': { $in: filters.itemTypes.map(itemType => mongoose.Types.ObjectId(itemType)) }
        });
    }

    if (filters.status && filters.status.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            $or: [{
                'engineerDetails.status': {
                    $in: filters.status
                }
            },
            {
                'drafterDetails.status': {
                    $in: filters.status
                }
            },
            {
                'managerDetails.status': {
                    $in: filters.status
                }
            }]
        });
    }
    if (filters.cities && filters.cities.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            'task.city': { $in: filters.cities }
        });
    }
    if (filters.states && filters.states.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            'task.state': { $in: filters.states }
        });
    }
    if (filters.engineers && filters.engineers.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            'engineerDetails.engineer': { $in: filters.engineers.map(engineer => mongoose.Types.ObjectId(engineer)) }
        });
    }
    if (filters.drafters && filters.drafters.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            'drafterDetails.drafter': { $in: filters.drafters.map(drafter => mongoose.Types.ObjectId(drafter)) }
        });
    }
    if (filters.managers && filters.managers.length) {
        query[SCOPE_MATCH_INDEX].$match.$and.push({
            'managerDetails.manager': { $in: filters.managers.map(manager => mongoose.Types.ObjectId(manager)) }
        });
    }

    const keyWordsQuery = [
        {
            $lookup: {
                from: 'scopekeywords',
                localField: 'scopeKeywords',
                foreignField: '_id',
                as: 'scopeKeywords'
            }
        },
        {
            $unwind: "$scopeKeywords"
        },
        {
            $unwind: "$scopeKeywords.itemTypeKeywords"
        },
        {
            $lookup: {
                from: 'keywords',
                localField: 'scopeKeywords.itemTypeKeywords.keyword',
                foreignField: '_id',
                as: 'itemTypeKeyword'
            }
        },
        {
            $lookup: {
                from: 'keywords',
                localField: 'scopeKeywords.customKeyword.keyword',
                foreignField: '_id',
                as: 'customKeywords'
            }
        },
        {
            $unwind: '$itemTypeKeyword'
        },
        {
            $match: {
                $and: []
            }
        }
    ]

    const KEYWORDS_MATCH_INDEX = 6;

    if (filters.scopeKeywords && filters.scopeKeywords.length) {
        keyWordsQuery[KEYWORDS_MATCH_INDEX].$match.$and.push({
            'scopeKeywords.enabled': true
        }, {
                $or: [
                    {
                        $and: [
                            { 'itemTypeKeyword.title': { $in: filters.scopeKeywords.map(keyword => keyword.title) } },
                            { 'scopeKeywords.itemTypeKeywords.isDone': true }
                        ]
                    },
                    {
                        "customKeywords.title": { $in: filters.scopeKeywords.map(keyword => keyword.title) }
                    }
                ]
            });
    }

    // if (keyWordsQuery[KEYWORDS_MATCH_INDEX].$match && !keyWordsQuery[KEYWORDS_MATCH_INDEX].$match.$and.length) {
    //     query.splice(KEYWORDS_MATCH_INDEX, 1);
    // }

    keyWordsQuery.push(
        {
            $group: {
                _id: '$_id',
                task: { $addToSet: '$task' },
                drawings: { $addToSet: '$drawings' },
                calcs: { $addToSet: '$calcs' },
                custDrawings: { $addToSet: "$custDrawings" },
                letters: { $addToSet: "$letters" },
                tabData: { $addToSet: "$tabData" },
                managerDetails: { $addToSet: "$managerDetails" },
                engineerDetails: { $addToSet: "$engineerDetails" },
                drafterDetails: { $addToSet: "$drafterDetails" },
                definition: { $addToSet: "$definition" },
                createdAt: { $addToSet: "$createdAt" },
                completedDate: { $addToSet: "$completedDate" },
                hourTrackers: { $addToSet: "$hourTrackers" },
                isArchived: { $addToSet: "$isArchived" },
                id: { $addToSet: "$id" },
                note: { $addToSet: "$note" },
                dueDate: { $addToSet: "$dueDate" },
                price: { $addToSet: "$price" },
                group: { $addToSet: "$group" },
                status: { $addToSet: "$status" },
                customerContact: { $addToSet: "$customerContact" },
                itemType: { $addToSet: "$itemType" },
                number: { $addToSet: "$number" },
                updatedAt: { $addToSet: "$updatedAt" },
                scopeKeywords: { $addToSet: "$scopeKeywords" },
                highlights: { $addToSet: "$highlights" },
                scopePlannings: { $addToSet: "$scopePlannings" }

            }
        },
        {
            $project: {
                id: { '$arrayElemAt': ['$id', 0] },
                note: { '$arrayElemAt': ['$note', 0] },
                task: { '$arrayElemAt': [{ '$arrayElemAt': ['$task', 0] }, 0] },
                drawings: { '$arrayElemAt': ['$drawings', 0] },
                calcs: { '$arrayElemAt': ['$calcs', 0] },
                letters: { '$arrayElemAt': ['$letters', 0] },
                tabData: { '$arrayElemAt': ['$tabData', 0] },
                managerDetails: { '$arrayElemAt': ['$managerDetails', 0] },
                engineerDetails: { '$arrayElemAt': ['$engineerDetails', 0] },
                drafterDetails: { '$arrayElemAt': ['$drafterDetails', 0] },
                createdAt: { '$arrayElemAt': ['$createdAt', 0] },
                completedDate: { '$arrayElemAt': ['$completedDate', 0] },
                hourTrackers: { '$arrayElemAt': ['$hourTrackers', 0] },
                isArchived: { '$arrayElemAt': ['$isArchived', 0] },
                dueDate: { '$arrayElemAt': ['$dueDate', 0] },
                price: { '$arrayElemAt': ['$price', 0] },
                group: { '$arrayElemAt': ['$group', 0] },
                customerContact: { '$arrayElemAt': ['$customerContact', 0] },
                itemType: { '$arrayElemAt': ['$itemType', 0] },
                number: { '$arrayElemAt': ['$number', 0] },
                updatedAt: { '$arrayElemAt': ['$updatedAt', 0] },
                scopeKeywords: { '$arrayElemAt': ['$scopeKeywords', 0] },
                highlights: { '$arrayElemAt': ['$highlights', 0] },
                scopePlannings: { '$arrayElemAt': ['$scopePlannings', 0] }
            }
        }
    );

    query = query.concat(keyWordsQuery);

    query.push({
        $addFields: {
            task: '$task._id'
        }
    }, {
            $sort: { 'dueDate': -1 }
        }, {
            $limit: 40
        });
    if (!query[SCOPE_MATCH_INDEX].$match.$and.length) {
        query.splice(SCOPE_MATCH_INDEX, 1);
    }
    let matchIndex = query[8].$match ? 8 : query[9].$match ? 9 : 10;
    if (query[matchIndex].$match && !query[matchIndex].$match.$and.length) {
        query.splice(matchIndex, 1);
    }

    return Scope.aggregate(query).exec()
}

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { scope, taskId } = req.body;
        const result = await Scope
            .findOneAndUpdate({ id }, scope, { new: true, select: generateSelection(scope) })
            .exec();
        res.json(result);
        const scopeNew = await Scope.findOne({ id }).populate(scopePopulationNew).exec();
        emitScoket(req, socketType.UPDATE_SCOPE, { taskId: scope.task, scope: scopeNew });
        // emitScoket(req, socketType.UPDATE_SCOPE, { taskId, scope: result });
    } catch (e) {
        console.log('Error in put scope/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        if (Object.keys(req.body).includes('itemType')) {
            const scopeKeywords = await createScopeKeyword(body.itemType);
            body.scopeKeywords = [scopeKeywords._id];
        }
        if (Object.keys(body).includes('definition')) {
            console.log('Definition:', req.body.definition);
            await Invoice.updateMany(
                { 'selectedScopes.scope': body._id },
                { $set: { "selectedScopes.$.description": req.body.definition } },
                { multi: true, new: true }
            );
        }
        const scope = await Scope.findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) })
            .populate(genetatePopulation(body))
            .exec();
        res.json(scope);
        const scopeNew = await Scope.findOne({ id }).populate(scopePopulationNew).exec();
        emitScoket(req, socketType.UPDATE_SCOPE, { taskId: scope.task, scope: scopeNew });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/customer_drawing', common.ensureAuthenticated, async (req, res) => {
    try {
        const _id = req.params.id;
        const { number, taskId } = req.body;
        const customerDrawing = await createCustDrawing(number || 0);
        if (!customerDrawing) {
            throw 'error in creating customer drawing setup'
        }
        const body = {
            custDrawings: [customerDrawing._id]
        };
        const scope = await Scope.findOneAndUpdate({ _id }, body, { new: true, select: defaultSelection }).exec().catch(e => { throw e });
        scope.custDrawings = [customerDrawing];
        res.json(scope);
        emitScoket(req, socketType.ADD_SCOPE_MILESTONE, { scope, taskId });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/letter', common.ensureAuthenticated, async (req, res) => {
    try {
        const _id = req.params.id;
        const { number, taskId } = req.body;
        const letter = await createLetter(number || 0);
        if (!letter) {
            throw 'error in creating customer drawing setup'
        }
        const body = {
            letters: [letter._id]
        };
        const scope = await Scope
            .findOneAndUpdate({ _id }, body, { new: true, select: defaultSelection })
            .exec()
            .catch(e => { throw e });
        scope.letters = [letter];
        res.json(scope);
        emitScoket(req, socketType.ADD_SCOPE_MILESTONE, { scope, taskId });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/scopeplannings', common.ensureAuthenticated, async (req, res) => {
    try {
        const _id = req.params.id;
        const { number, taskId } = req.body;
        const scopePlanning = await createScopePlanning(number || 0);
        if (!scopePlanning) {
            throw 'error in creating scope planning'
        }

        const scope = await Scope
            .findOneAndUpdate({ _id },
                { $push: { scopePlannings: scopePlanning._id } },
                {
                    new: true, select: { ...defaultSelection, scopePlannings: 1 }
                })
            .populate(scopePlanningsPopulation)
            .exec()
            .catch(e => { throw e });
        res.json(scope);
        emitScoket(req, socketType.ADD_SCOPE_MILESTONE, { scope, taskId });
    } catch (e) {
        console.log('Error in put scope/:id/scopeplannings API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/tab_data', common.ensureAuthenticated, async (req, res) => {
    try {
        const _id = req.params.id;
        const { number, taskId } = req.body;
        const tabData = await createTabData(number || 0);
        if (!tabData) {
            throw 'error in creating customer drawing setup'
        }
        const body = {
            tabData: [tabData._id]
        };
        const scope = await Scope.findOneAndUpdate({ _id }, body, { new: true, select: defaultSelection }).exec().catch(e => { throw e });
        scope.tabData = [tabData];
        res.json(scope);
        emitScoket(req, socketType.ADD_SCOPE_MILESTONE, { scope, taskId });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/group/:id', common.ensureAuthenticated, async (req, res) => {
    const groupId = req.params.id;
    const { userId, fromYear, toYear } = req.body;
    const startDate = new Date(moment([fromYear]).startOf('year'));
    const endDate = new Date(moment([toYear]).endOf('year'));

    const query = {
        $and: [
            {
                $or: [
                    { 'engineerDetails.engineer': userId },
                    { 'drafterDetails.drafter': userId },
                    { 'managerDetails.manager': userId }
                ]
            },
            { group: groupId },
            { isArchived: false },
            { dueDate: { $gte: startDate, $lte: endDate } }
        ]
    };

    const sort = {
        createdAt: -1
    };

    try {
        const scopes = await Scope.find(query).populate(scopePopulationNew).select(scopeByGroupSelection).sort(sort).exec();
        res.json(scopes);
    } catch (e) {
        console.log('Error in post scope/group/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', common.ensureAuthenticated, async (req, res) => {
    const { scope, taskID } = req.body;
    try {
        const counter = await getCounter(taskID);
        const updatedCounter = await incrementScopeCounter(counter, 1);
        if (!updatedCounter) {
            throw Error('counter not updated');
        }
        scope.number = String.fromCharCode(65 + updatedCounter.scopeNumber)
        console.log('&*&*&*&*&', updatedCounter.scopeNumber, scope.number)
        let newScope = null;
        const { isFromTaskGroup } = await Task.findOne({ id: taskID }).exec().catch(e => { throw e });
        if (isFromTaskGroup) {
            newScope = await createTaskGroupScope(scope);
        } else {
            newScope = await createScope(scope);
        }
        if (!newScope) {
            throw Error('unable to create scope');
        }
        await Task.findOneAndUpdate({ id: taskID }, { $push: { scopes: newScope._id } }).exec().catch(e => { throw e });
        const result = await Scope.findOne({ id: newScope.id }).populate(scopePopulation).lean().exec();
        res.json(result);
        const socketData = await Scope.findOne({ id: newScope.id }).populate(scopePopulationNew).exec();
        emitScoket(req, socketType.ADD_SCOPE, socketData);
        emitScoket(req, socketType.ADD_TASK_SCOPE, result);

    } catch (e) {
        console.log('Error in post scope/ API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/hourtracker/add', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const hourTracker = req.body;
    try {
        const scope = await Scope.findOneAndUpdate({ id }, { $push: { hourTrackers: hourTracker._id } }, { new: true, select: defaultSelection }).lean().exec();
        scope.hourtracker = hourTracker;
        res.json(scope);
        emitScoket(req, socketType.ADD_HOUR_TRACKER_TO_SCOPE, scope);
    } catch (e) {
        console.log('Error in post scope/:id/hourtracker/add API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/hourtracker/remove', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { hourId } = req.body;
    try {
        const scope = await Scope
            .findOneAndUpdate(
                { id },
                { $pull: { hourTrackers: mongoose.Types.ObjectId(hourId) } },
                { select: defaultSelection }
            )
            .lean()
            .exec()
            .catch(e => { throw e });
        scope.hourTracker = hourId;
        res.json(scope);
        emitScoket(req, socketType.REMOVE_HOUR_TRACKER_FROM_SCOPE, scope);
    } catch (e) {
        console.log('Error in post scope/:id/hourtracker/remove API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/highlight', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const highlight = req.body;
    try {
        let scope = null;
        if (!highlight.hasOwnProperty('_id')) {
            // create new highlight
            scope = await Scope
                .findOneAndUpdate(
                    { id },
                    { $push: { highlights: highlight } },
                    { new: true }
                )
                .lean()
                .exec();
        } else {
            // update highlight
            scope = await Scope
                .findOneAndUpdate(
                    { id, 'highlights._id': highlight._id },
                    { $set: { 'highlights.$': highlight } },
                    { new: true }
                )
                .lean()
                .exec();
        }

        if (!scope) {
            throw error('not able to update scope');
        }
        res.json(scope.highlights);
        emitScoket(req, socketType.UPDATE_SCOPE_HIGHLIGHTS, { id: scope.id, highlights: scope.highlights })
    } catch (e) {
        console.log('Error in put scope/:id/highlight API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/add-rev-scope', common.ensureAuthenticated, async (req, res) => {
    let scope = req.body;
    try {
        const { nextRev } = await incrementRevScopeCounter(scope._id);
        const revScope = {
            group: scope.group,
            parent: scope._id,
            task: scope.task,
            itemType: scope.itemType,
            dueDate: new Date(),
            price: 0,
            note: scope.note,
            managerDetails: {
                ...scope.managerDetails,
                nonUrgentHours: 0,
                urgentHours: 0
            },
            engineerDetails: {
                ...scope.engineerDetails,
                nonUrgentHours: 0,
                urgentHours: 0
            },
            drafterDetails: {
                ...scope.drafterDetails,
                nonUrgentHours: 0,
                urgentHours: 0
            },
            number: `${scope.number}-${nextRev}`
        };
        let newScope = null;
        const { isFromTaskGroup } = await Task.findOne({ _id: scope.task }).exec().catch(e => { throw e });
        if (isFromTaskGroup) {
            newScope = await createTaskGroupScope(revScope, nextRev);
        } else {
            newScope = await createScope(revScope, nextRev, true);
        }
        if (!newScope) {
            throw Error('unable to create scope');
        }
        await Task.findOneAndUpdate({ _id: scope.task }, { $push: { scopes: newScope._id } }).exec();
        const result = await Scope.findOne({ id: newScope.id }).populate(scopePopulation).lean().exec();
        res.json(result);
        const socketData = await Scope.findOne({ id: newScope.id }).populate(scopePopulationNew).exec();
        emitScoket(req, socketType.ADD_SCOPE, socketData);
        emitScoket(req, socketType.ADD_TASK_SCOPE, result);
    } catch (e) {
        console.log('Error in put scope/:id/add-rev-sope API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/get-reference-scopes', async (req, res) => {

    const {
        userId,
        keywords,
        contractor,
        customKeywordTitle,
        scopeIds,
        tags,
        itemType
    } = req.body;


    let query = [
        {
            $match: {
                $or: [
                    {
                        'drafterDetails.drafter': mongoose.Types.ObjectId(userId)
                    },
                    {
                        'managerDetails.manager': mongoose.Types.ObjectId(userId)
                    },
                    {
                        'engineerDetails.engineer': mongoose.Types.ObjectId(userId)
                    }
                ],
                id: { $nin: scopeIds },
                isArchived: false,
                group: { $ne: mongoose.Types.ObjectId('58bed84f62b976001126f7a8') },
                itemType: mongoose.Types.ObjectId(itemType),
            }
        },
        {
            $lookup: {
                from: 'tasks',
                localField: 'task',
                foreignField: '_id',
                as: 'task'
            }
        },
        {
            $match: {
                "task.tags": { $size: tags.length }
            }
        }
    ];

    if (contractor && contractor !== '') {
        query.push({
            $match: {
                $and: [
                    {
                        'task.contractor.name': contractor
                    }
                ]
            }
        });
    }

    query.push(
        {
            $lookup: {
                from: 'scopekeywords',
                localField: 'scopeKeywords',
                foreignField: '_id',
                as: 'scopeKeywords'
            }
        },
        {
            $lookup: {
                from: 'keywords',
                localField: 'scopeKeywords.customKeyword.keyword',
                foreignField: '_id',
                as: 'customKeyword'
            }
        }, {
            $unwind: {
                path: '$scopeKeywords',
            }
        }, {
            $unwind: {
                path: '$scopeKeywords.itemTypeKeywords',
            }
        }, {
            $lookup: {
                from: "keywords",
                localField: "scopeKeywords.itemTypeKeywords.keyword",
                foreignField: "_id",
                as: "itemTypeKeyword"
            }
        }, {
            $unwind: {
                path: "$itemTypeKeyword"
            }
        }, {
            $unwind: {
                path: "$customKeyword"
            }
        }

    );

    let keywordQuery = {
        $match: {
            'scopeKeywords.enabled': true,
            'customKeyword.title': customKeywordTitle
        }
    };

    if (keywords.length > 0) {
        keywordQuery.$match['itemTypeKeyword.title'] = { $in: keywords };
        keywordQuery.$match['scopeKeywords.itemTypeKeywords.isDone'] = true;
    } else {
        keywordQuery.$match['scopeKeywords.itemTypeKeywords.isDone'] = false;
        keywordQuery.$match['itemTypeKeyword.title'] = { $ne: 'All Customers' };
    }

    query.push(
        keywordQuery,
        {
            $group: {
                _id: '$_id',
                itemTypeKeywords: { $push: '$scopeKeywords.itemTypeKeywords' },
                task: { $addToSet: '$task' },
                number: { $addToSet: '$number' },
                note: { $addToSet: '$note' },
                city: { $addToSet: '$city' },
                state: { $addToSet: '$state' },
                id: { $addToSet: '$id' }
            }
        });

    if (keywords.length > 0) {
        query.push({
            $match: {
                'itemTypeKeywords': { $size: keywords.length },
            }
        });
    } else {
        query.push({
            $match: {
                'itemTypeKeywords': { $size: 6 },
            }
        });
    }



    if (tags.length > 0) {

        query.push(
            {
                $project: {
                    task: { '$arrayElemAt': [{ '$arrayElemAt': ['$task', 0] }, 0] },
                    number: { '$arrayElemAt': ['$number', 0] },
                    id: { '$arrayElemAt': ['$id', 0] },
                    note: { '$arrayElemAt': ['$note', 0] },
                    city: { '$arrayElemAt': ['$city', 0] },
                    state: { '$arrayElemAt': ['$state', 0] }
                }
            },
            {
                $unwind: {
                    path: '$task',
                }
            },
            {
                $unwind: {
                    path: '$task.tags',
                }
            },
            {
                $match: {
                    'task.tags': { $in: tags.map(item => mongoose.Types.ObjectId(item)) }
                }
            },
            {
                $group: {
                    _id: '$_id',
                    tags: { $push: '$task.tags' },
                    task: { $addToSet: '$task' },
                    number: { $addToSet: '$number' },
                    note: { $addToSet: '$note' },
                    city: { $addToSet: '$city' },
                    state: { $addToSet: '$state' },
                    id: { $addToSet: '$id' }
                }
            },
            {
                $match: {
                    tags: { $size: tags.length }
                }
            },
            {
                $sort: { createdAt: -1 }
            },
            {
                $project: {
                    task: { '$arrayElemAt': ['$task', 0] },
                    number: { '$arrayElemAt': ['$number', 0] },
                    id: { '$arrayElemAt': ['$id', 0] },
                    note: { '$arrayElemAt': ['$note', 0] },
                    city: { '$arrayElemAt': ['$city', 0] },
                    state: { '$arrayElemAt': ['$state', 0] }
                }
            },
            {
                $limit: 10
            }
        );
    } else {
        query.push(
            {
                $sort: { createdAt: -1 }
            },
            {
                $project: {
                    task: { '$arrayElemAt': [{ '$arrayElemAt': ['$task', 0] }, 0] },
                    number: { '$arrayElemAt': ['$number', 0] },
                    id: { '$arrayElemAt': ['$id', 0] },
                    note: { '$arrayElemAt': ['$note', 0] },
                    city: { '$arrayElemAt': ['$city', 0] },
                    state: { '$arrayElemAt': ['$state', 0] }
                }
            },
            {
                $limit: 10
            }
        );
    }

    try {
        const scopes = await Scope.aggregate(query).exec();
        res.json(scopes);
    } catch (error) {
        console.error('Error in post scope/get-reference-scopes API', error);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/filtered', validate('filtered'), async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.error('Error in filtered scopes:\n', errors.array());
        return res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }

    try {
        const filters = req.body;
        const scopes = await Promise.all(filters.group.map(groupId => getGroupFilteredScopes(groupId, filters))).catch(e => { throw e });

        Scope.populate(scopes, scopePopulationNew, (populationError, populatedScopes) => {
            if (!populationError) {
                res.json(populatedScopes.reduce((arr, item) => [...arr, ...item], []));
            } else {
                console.error('Error in populating scopes:\n', populationError);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } catch (error) {
        console.error('Error in populating scopes:\n', error);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const scopes = await Scope.findOne({ id: req.params.id }).populate(scopePopulationNew).select(scopeByGroupSelection).exec();
        res.json(scopes);
    } catch (e) {
        console.log('Error in get scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
})

export default router;
