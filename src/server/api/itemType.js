var _ = require('lodash');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var ItemType = require('../models/ItemType');
var Keyword = require('../models/Keyword');

//common
var common = require('./common');

import { createKeyword } from '../utils/milestonesCreate';
import { itemTypePopulation } from '../utils/populations';

var error = constants.RESPONSES;

function getItemType(query, population, callback) {
    if (_.isEmpty(query)) {
        query = {};
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    ItemType.findOne(query).populate(population).exec(function (err, itemType) {
        if (!err) {
            callback(null, itemType);
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function getItemTypes(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {};
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    ItemType.find(query).populate(population).sort(sort).limit(limit).exec(function (err, itemTypes) {
        if (!err) {
            callback(null, itemTypes);
        } else {
            console.log(err);
            callback(err);
        }
    });
}

router.post('/', common.ensureAuthenticated, function (req, res) {
    var name = req.body.name;
    var io = req.app.get('socketio');
    var socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, constants.ACCESS_LEVELS.WRITE, async function () {
        let allCustomerKeyword = await Keyword.findOne({ title: 'All Customers' }).exec();
        if (!allCustomerKeyword) {
            allCustomerKeyword = await createKeyword({ title: 'All Customers' })
        }
        const keywords = await Promise.all(req.body.keywords.map(item => createKeyword(item)))
        var itemType = new ItemType({
            name: name,
            keywords: [allCustomerKeyword, ...keywords]
        });
        itemType.save(function (err, savedItemType) {
            if (!err) {
                res.json(savedItemType);
                if (socket) {
                    socket.broadcast.emit(constants.SOCKET_EVENTS.ITEM_TYPE_CREATE, { newItemType: savedItemType });
                }
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.get('/search/:query', common.ensureAuthenticated, function (req, res) {
    var query = {
        name: {
            '$regex': '.*' + req.params.query + '.*',
            '$options': 'i'
        }
    };

    getItemTypes(query, itemTypePopulation, null, null, function (err, itemTypes) {
        if (!err) {
            res.json(itemTypes);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function (req, res) {
    getItemTypes(null, itemTypePopulation, null, null, function (err, itemTypes) {
        if (!err) {
            res.json(itemTypes);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/:itemTypeId', common.ensureAuthenticated, function (req, res) {
    var itemTypeId = req.params.itemTypeId;
    var params = req.body;
    var io = req.app.get('socketio');
    var socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, constants.ACCESS_LEVELS.WRITE, async () => {

        let allCustomerKeyword = await Keyword.findOne({ title: 'All Customers' }).exec();
        let keywords = [];
        if (!allCustomerKeyword) {
            allCustomerKeyword = await createKeyword({ title: 'All Customers' })
        }
        !params.keywords.find(item => item.title === 'All Customers') && keywords.push(allCustomerKeyword);
        keywords.push(...params.keywords);

        ItemType.findOneAndUpdate({
            id: itemTypeId
        },
            { $set: { "name": params.name, keywords } }
            , {
                new: true
            }).populate(itemTypePopulation).exec(function (err, updatedItemType) {
                if (!err) {
                    res.json(updatedItemType);
                    if (socket) {
                        socket.broadcast.emit(constants.SOCKET_EVENTS.ITEM_TYPE_UPDATE, { newItemType: updatedItemType });
                    }
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
    });
});

module.exports = router;
