import express from 'express';

import TabData from '../models/TabData';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { createTabData } from '../utils/milestonesCreate';
import { emitScoket, socketType } from '../utils/socket';
import common from './common';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const TabData = await createTabData();
        res.json(TabData);
        emitScoket(req, socketType.ADD_TAB_DATA, tabData);
    } catch (e) {
        console.log('Error in tabData/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, tabData } = req.body;
        const result = await TabData
            .findOneAndUpdate({ id }, tabData, { new: true, select: generateSelection(tabData) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_TAB_DATA, { taskId, tabData: result });
    } catch (e) {
        console.log('Error in tabData/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, tabData } = req.body;
        const result = await TabData
            .findOneAndUpdate({ id }, tabData, { new: true, select: generateSelection(tabData) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_TAB_DATA, { taskId, tabData: result });
    } catch (e) {
        console.log('Error in put tabData/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const tabData = await TabData.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_TAB_DATA, { id });
    } catch (e) {
        console.log('Error in put tabData/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
