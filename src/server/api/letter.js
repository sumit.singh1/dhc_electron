import express from 'express';

import Letter from '../models/Letter';

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { generateSelection } from '../utils/common';
import { createLetter } from '../utils/milestonesCreate';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const Letter = await createLetter();
        res.json(Letter);
        emitScoket(req, socketType.ADD_LETTER, letter);
    } catch (e) {
        console.log('Error in letter/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, letter } = req.body;
        const result = await Letter
            .findOneAndUpdate({ id }, letter, { new: true, select: generateSelection(letter) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_LETTER, { taskId, letter: result });
    } catch (e) {
        console.log('Error in letter/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, letter } = req.body;
        const result = await Letter
            .findOneAndUpdate({ id }, letter, { new: true, select: generateSelection(letter) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_LETTER, { taskId, letter: result });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const letter = await Letter.findByIdAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_LETTER, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
