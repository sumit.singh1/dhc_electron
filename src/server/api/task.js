import async from 'async';
import Promise from 'bluebird';
import { CronJob } from 'cron';
import xl from 'excel4node';
import express from 'express';
import _ from 'lodash';
import moment from 'moment';
import shortid from 'shortid';

import CustomerImages from '../models/CustomerImage';
import Scope from '../models/Scope';
import Task from '../models/Task';
import TaskGroup from '../models/TaskGroup';
import User from '../models/User';
import { generateSelection, padWithZeroes } from '../utils/common';
import constants from '../utils/constants';
import {
    createCounter,
    incrementBidNumberCounter,
    incrementScopeCounter,
    incrementTaskGroupNumberCounter,
    incrementTaskNumberCounter,
} from '../utils/counterUtils';
import { getClient } from '../utils/ftpClient';
import {
    addTaskMilestone,
    createAgreement,
    createClientAgreement,
    createInvoice,
    createMasterAgreement,
    createModifiedAgreement,
    createPurchaseOrder,
    createScope,
    createTaskGroupScope,
} from '../utils/milestonesCreate';
import { getTaskPopulation, scopePopulation, updateTaskPopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common, { ensureAuthenticated } from './common';

const router = express.Router();

function getTaskGroups(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    TaskGroup.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .lean()
        .exec(function (err, taskGroups) {
            if (!err) {
                callback(null, taskGroups);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function insertHeaderRow(workbook, toRowNumber, headerStyle) {
    let columnLength = constants.TABLE_HEADERS.length;
    for (let i = 0; i < columnLength; i++) {
        workbook.cell(toRowNumber, i + 1).string(constants.TABLE_HEADERS[i].title).style(headerStyle);
    }
    workbook.column(1).setWidth(13);
    workbook.column(2).setWidth(30);
    workbook.column(5).setWidth(20);
    workbook.column(6).setWidth(20);
    workbook.column(7).setWidth(20);
    workbook.column(8).setWidth(13);
    workbook.column(12).setWidth(20);
    workbook.column(13).setWidth(20);
}

function formatDate(date) {
    let now = moment();
    let formattedDate = moment(date);
    let isThisWeek = now.isoWeek() === formattedDate.isoWeek();

    let dateString = '';

    if (isThisWeek) {
        dateString += formattedDate.format('ddd');
    } else {
        dateString += formattedDate.format('M/D/YY');
    }
    return dateString;
}

function destroyClient(client) {
    client.end();
}

function uploadBackUpExcel(client, source, destination, callback) {
    destination = process.env.FTP_BACKUP_FOLDER + destination;
    client.put(source, destination, function (err) {
        if (!err) {
            callback(null);
        } else {
            console.log(err);
            callback(err);
        }
        destroyClient(client);
    });
}

function backUpData() {
    let tasksPopulation = [
        {
            path: 'teamMembers',
            populate: [{
                path: 'role'
            }]
        }, {
            path: 'createdBy'
        }, {
            path: 'scopes',
            match: {
                isArchived: false
            },
            populate: [{
                path: 'engineerDetails.engineer',
                populate: [{
                    path: 'role'
                }]
            }, {
                path: 'drafterDetails.drafter',
                populate: [{
                    path: 'role'
                }]
            }, {
                path: 'milestone.templates.metaData'
            }, {
                path: 'drawings',
                match: {
                    isArchived: false
                }
            }, {
                path: 'calcs',
                match: {
                    isArchived: false
                }
            }]
        }, {
            path: 'purchaseOrders',
            match: {
                isArchived: false
            }
        }, {
            path: 'agreements',
            match: {
                isArchived: false
            }
        }, {
            path: 'invoices',
            match: {
                isArchived: false
            }
        }
    ];
    let userPopulation = [
        {
            path: 'role'
        }, {
            path: 'locationInfo.location'
        }, {
            path: 'roleLevel'
        }
    ];
    User.find(null).populate(userPopulation).exec(function (err, users) {
        if (!err) {
            let taskGroupPromise = new Promise(function (resolve, reject) {
                getTaskGroups(null, null, null, null, function (err, taskGroups) {
                    if (!err) {
                        resolve(taskGroups);
                    } else {
                        reject(err);
                        console.log(err);
                    }
                });
            });
            taskGroupPromise.then(function (taskGroups) {
                let currentRowNumber = 1;
                let team,
                    taskNumber,
                    manager,
                    engineer,
                    drafter,
                    poText,
                    csaText,
                    invoiceText,
                    isCalc,
                    isEngineerOrManager,
                    taskOrBidding;

                getTasks(null, tasksPopulation, null, null, function (err, tasks) {
                    if (!err) {
                        _.forEach(users, function (currentUser) {
                            isEngineerOrManager = false;
                            let wb = new xl.Workbook({
                                defaultFont: {
                                    size: 10,
                                    name: 'calibri'
                                }
                            });
                            let headerStyle = wb.createStyle({
                                font: {
                                    color: 'FFFFFF',
                                    bold: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: '424242'
                                },
                                alignment: {
                                    horizontal: 'center'
                                }
                            });

                            let rowStyle = wb.createStyle({
                                alignment: {
                                    horizontal: 'center'
                                }
                            });
                            let ws = wb.addWorksheet('Sheet 1', {
                                margins: {
                                    left: 0.44,
                                    right: 0.44
                                },
                                pageSetup: {
                                    paperSize: 'A4_PAPER',
                                    scale: 80
                                },
                                printOptions: {
                                    centerHorizontal: true
                                }
                            });
                            currentRowNumber = 1;
                            _.forEach(taskGroups, function (currentTaskGroup) {
                                ws.cell(currentRowNumber, 1).string(currentTaskGroup.title).style({ font: { color: 'ff0000', bold: true } });
                                currentRowNumber += 2;
                                insertHeaderRow(ws, currentRowNumber, headerStyle);
                                currentRowNumber += 2;
                                _.forEach(tasks, function (task) {
                                    poText = csaText = invoiceText = 0;
                                    if (task.isBidding) {
                                        taskOrBidding = 'B';
                                    }
                                    taskOrBidding = task.isBidding ? 'B' : task.isFromTaskGroup ? 'T' : '';
                                    taskNumber = taskOrBidding + new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber;
                                    _.forEach(task.purchaseOrders, function (purchaseOrder) {
                                        if (purchaseOrder.templates[purchaseOrder.templates.length - 1].isDone) {
                                            poText += 1;
                                        }
                                    });
                                    _.forEach(task.agreements, function (agreement) {
                                        if (agreement.templates[agreement.templates.length - 1].isDone) {
                                            csaText += 1;
                                        }
                                    });
                                    _.forEach(task.invoices, function (invoice) {
                                        if (invoice.templates[invoice.templates.length - 1].isDone) {
                                            invoiceText += 1;
                                        }
                                    });
                                    _.forEach(task.scopes, function (scope) {
                                        team = '';
                                        if (task.createdBy._id !== scope.engineerDetails.engineer._id) {
                                            manager = task.createdBy.firstName[0] + task.createdBy.lastName[0] + ', ';
                                        }
                                        isCalc = scope.calcs.length > 0 ? 'Y' : 'N';
                                        engineer = scope.engineerDetails.engineer.firstName[0] + scope.engineerDetails.engineer.lastName[0] + ', ';
                                        drafter = scope.drafterDetails.drafter ? scope.drafterDetails.drafter.firstName[0] + scope.drafterDetails.drafter.lastName[0] : ' - ';
                                        team = manager + engineer + drafter;
                                        if (scope.group.equals(currentTaskGroup._id)) {
                                            if (task.createdBy._id.equals(currentUser._id) || scope.engineerDetails.engineer._id.equals(currentUser._id)) {
                                                isEngineerOrManager = true;
                                                ws.cell(currentRowNumber, 1).string(team).style(rowStyle);
                                                ws.cell(currentRowNumber, 2).string(task.contractor.name).style(rowStyle);
                                                ws.cell(currentRowNumber, 3).string(taskNumber).style(rowStyle);
                                                ws.cell(currentRowNumber, 4).string(scope.number).style(rowStyle);
                                                ws.cell(currentRowNumber, 5).string(task.title).style(rowStyle);
                                                ws.cell(currentRowNumber, 6).string(task.city).style(rowStyle);
                                                ws.cell(currentRowNumber, 7).string(task.state).style(rowStyle);
                                                ws.cell(currentRowNumber, 8).string(scope.note).style(rowStyle);
                                                ws.cell(currentRowNumber, 9).string(scope.engineerDetails.status).style(rowStyle);
                                                ws.cell(currentRowNumber, 10).string(formatDate(scope.dueDate)).style(rowStyle);
                                                ws.cell(currentRowNumber, 11).string('$ ' + scope.price.toString()).style(rowStyle);
                                                ws.cell(currentRowNumber, 12).string(scope.engineerDetails.urgentHours.toString()).style(rowStyle);
                                                ws.cell(currentRowNumber, 13).string(scope.engineerDetails.nonUrgentHours.toString()).style(rowStyle);
                                                ws.cell(currentRowNumber, 14).string(csaText.toString()).style(rowStyle);
                                                ws.cell(currentRowNumber, 15).string(poText.toString()).style(rowStyle);
                                                ws.cell(currentRowNumber, 16).string(invoiceText.toString()).style(rowStyle);
                                                ws.cell(currentRowNumber, 17).string(isCalc).style(rowStyle);
                                                currentRowNumber += 1;
                                            }
                                        }
                                    });
                                });
                                currentRowNumber += 2;
                            });
                            if (isEngineerOrManager) {
                                let fileName = currentUser.firstName + '-' + currentUser.lastName + '.xlsx';

                                wb.writeToBuffer().then(function (buffer) {
                                    getClient.then(client => {
                                        uploadBackUpExcel(client, buffer, fileName, function (uploadErr) {
                                            if (!uploadErr) {
                                                console.log('Backup completed successfully for user : ' + currentUser.firstName + ' ' + currentUser.lastName);
                                            } else {
                                                console.log(err);
                                            }
                                        });
                                    }).catch(e => console.log(error))
                                    // getClient(function (client) {

                                    // });
                                });
                            }
                        });
                    } else {
                        console.log(err);
                    }
                });
            });
        } else {
            console.log(err);
        }
    });
}

router.get('/state-cities', ensureAuthenticated, (req, res) => {
    Task.aggregate([
        {
            $project: {
                city: 1,
                state: 1
            }
        },
        {
            $group: { _id: null, uniqueCities: { $addToSet: '$city' }, uniqueStates: { $addToSet: '$state' } }
        }
    ]).exec((error, result) => {
        if (!error) {
            res.json(result);
        } else {
            console.log('Error in get unique cities, states API', error);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});


let job = new CronJob({
    cronTime: '0 10-18/2 * * *',
    onTick: function () {
        if (process.env.NODE_ENV === 'production') {
            backUpData();
        }
    },
    start: false,
    timeZone: 'America/Los_Angeles'
});
job.start();

router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await Task.findOne({ id }).populate(getTaskPopulation).exec();
        res.json(task);
    } catch (e) {
        console.log('Error in get task/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/', ensureAuthenticated, async (req, res) => {
    const contractor = req.query.contractor;
    const title = req.query.title;
    try {
        const query = {
            $and: [
                {
                    title: {
                        $regex: '^' + title + '$',
                        $options: 'i'
                    }
                },
                {
                    'contractor.name': contractor
                }
            ]
        };
        const tasks = await Task.find(query).exec();
        res.json(tasks);
    } catch (e) {
        console.log('Error in get task/?title=""&contractor="" API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', ensureAuthenticated, async (req, res) => {
    try {
        const { task, scopes } = req.body;
        task.teamMembers = []; // remove this after removing team member field from task model
        let taskNumber = null;
        if (task.isBidding) {
            taskNumber = await incrementBidNumberCounter();
        } else if (task.isFromTaskGroup) {
            taskNumber = await incrementTaskGroupNumberCounter();
        } else {
            taskNumber = await incrementTaskNumberCounter();
        }
        if (!taskNumber) {
            throw 'unable to create a task number';
        }
        task.taskNumber = padWithZeroes(taskNumber, 4);
        const newTask = await (new Task(task)).save();
        const counter = await createCounter(newTask.id);
        const updatedCounter = await incrementScopeCounter(counter, scopes.length);
        if (!updatedCounter) {
            throw 'counter not updated';
        }
        scopes.forEach((scope, index) => scope.number = String.fromCharCode(65 + index));
        let newScopes = [];
        if (task.isFromTaskGroup) {
            newScopes = await Promise.all(scopes.map(scope => createTaskGroupScope({ ...scope, task: newTask._id }))).catch(e => { throw e });
        } else {
            newScopes = await Promise.all(scopes.map(scope => createScope({ ...scope, task: newTask._id }))).catch(e => { throw e });
        }

        if (newScopes.length === 0) {
            throw Error('Error in creating scopes')
        }
        // add logic of creating milestones
        const promises = [];
        const milestoneTypes = [];

        let selectedScopes = newScopes.map(item => item._id);

        let data = {
            scopes: selectedScopes
        };

        if (!task.isFromTaskGroup) {

            if (task.contractor.poRequired) {
                promises.push(createPurchaseOrder(newTask.id));
                milestoneTypes.push('purchaseOrders');
            }

            switch (task.contractor.contract.toLowerCase()) {
                case 'csa':
                    promises.push(createAgreement(newTask.id, { selectedScopes }, false));
                    milestoneTypes.push('agreements');
                    break;
                case 'ma':
                    promises.push(createMasterAgreement(newTask.id, { selectedScopes }));
                    milestoneTypes.push('masterAgreements');
                    break;
                case 'mcsa':
                    promises.push(createModifiedAgreement(newTask.id, { selectedScopes }));
                    milestoneTypes.push('modifiedAgreements');
                    break;
                case 'ca':
                    promises.push(createClientAgreement(newTask.id, { selectedScopes }));
                    milestoneTypes.push('clientAgreements');
                    break;
            }

            promises.push(createInvoice(newTask.id, {
                selectedScopes: newScopes.map(item => ({ scope: item._id, oldPrice: item.price, description: item.definition }))
            }));

            milestoneTypes.push('invoices');

            const milestones = await Promise.all(promises).catch(e => {
                throw e;
            });

            milestoneTypes.forEach((item, index) => data[item] = milestones[index]);
        }

        const result = await Task
            .findOneAndUpdate({ id: newTask.id }, data, { new: true })
            .populate(getTaskPopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.ADD_TASK, result);
    } catch (e) {
        console.log('Error in post task/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

// API to update manager of respective task
router.put('/update/manager/:taskId', ensureAuthenticated, async (req, res) => {
    const taskId = req.params.taskId;
    const { managerId, groupId } = req.body;
    try {

        let scopeData = await Scope.updateMany({ task: taskId }, { $set: { managerDetails: { manager: managerId } } }).exec();

        // update data to the user using socket
        // const io = req.app.get('socketio');
        // const socket = io.sockets.sockets[req.headers.socketid];

        const scopeQuery = {
            $and: [
                { 'managerDetails.manager': managerId },
                { group: groupId },
                { isArchived: false },
                { task: taskId }
            ]
        };
        if (!scopeData) {
            throw 'err';
        }

        const updatedScopes = await Scope.find(scopeQuery).populate(scopePopulation).exec();
        emitScoket(req, constants.SOCKET_EVENTS.MANAGER_UPDATE, { task: updatedScopes })
        if (!updatedScopes) {
            throw 'err';
        } else {
            res.json(updatedScopes)
        }
        if (socket) {
            // socket.broadcast.emit(constants.SOCKET_EVENTS.MANAGER_UPDATE, { task: updatedTask });
            socket.broadcast.emit('123', updatedScopes);
        }

    } catch (e) {
        console.log('error inside update manager api', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }

});

router.post('/exist', async (req, res) => {
    try {
        let projectName = req.body.projectName;
        let contractorName = req.body.contractorName;
        const task = await Task.find({
            $and: [
                {
                    title: {
                        $regex: '^' + projectName + '$',
                        $options: 'i'
                    }
                },
                {
                    'contractor.name': contractorName
                }
            ]
        }).exec()
            .catch(e => { throw e });
        res.json(task);
    } catch (e) {
        console.log('Error in post tasks/:id/comment API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/getScopesAndContactsOfCustomer', ensureAuthenticated, function (req, res) {
    let customerName = req.body.companyName;
    let query = customerName;
    query = query.replace('\'', '\'\'');
    query = encodeURIComponent(query);
    let accessToken = req.cookies.OFFICE_ACCESS_TOKEN;
    let population = [
        {
            path: 'scopes',
            populate: [
                {
                    path: 'task',
                    populate: [
                        {
                            path: 'invoices',
                            populate: [{
                                path: 'selectedScopes.scope',
                                select: '_id id number note price isArchived definition'
                            }],
                            match: {
                                isArchived: false
                            },
                            select: '_id id templates isArchived paid hold poNumber isUpdated sentDate paidDate selectedScopes'
                        },
                        {
                            path: 'teamMembers',
                            select: 'firstName lastName _id id email role picture employeeCode'
                        },
                        {
                            path: 'purchaseOrders',
                            match: {
                                isArchived: false
                            },
                            select: '_id id  isArchived number'
                        }
                    ]
                }, {
                    path: 'engineerDetails.engineer',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id email role picture employeeCode'
                }, {
                    path: 'drafterDetails.drafter',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id email id role picture employeeCode'
                }, {
                    path: 'managerDetails.manager',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id email role picture employeeCode'
                }, {
                    path: 'itemType'
                }, {
                    path: 'group'
                }, {
                    path: 'hourTrackers',
                    populate: [{
                        path: 'employee',
                        populate: [{
                            path: 'role'
                        }]
                    }]
                }
            ],
            match: {
                isArchived: false
            }
        }
    ];
    if (accessToken) {
        async.parallel({
            customerScopes: function (callback) {
                let query;
                if (customerName.indexOf('[') === -1) {
                    query = {
                        'contractor.name': {
                            $regex: `.*${customerName}.*`,
                            $options: 'i'
                        }
                    }
                } else {
                    query = {
                        'contractor.name': customerName
                    }
                }
                Task.find(query).populate(population).lean().exec(function (customerScopesError, customerTasks) {
                    let allScopes = [];
                    let customerScopes = {};
                    if (!customerScopesError) {
                        if (customerTasks.length > 0) {
                            customerTasks.forEach(function (task) {
                                allScopes = allScopes.concat(task.scopes);
                            });
                        }
                        callback(null, allScopes);
                    } else {
                        callback(customerScopesError);
                    }
                });
            },
            contacts: function (callback) {
                common.getContacts(accessToken, query, req, res, function (contactsError, contacts) {
                    if (!contactsError) {
                        if (contacts.length > 0) {
                            let promises = [];
                            contacts.map(function (contact) {
                                let photoPromise = new Promise(function (resolve, reject) {
                                    common.getPhoto(accessToken, contact.id, req, res, function (photoError, photo) {
                                        let newContact = contact;
                                        if (!photoError) {
                                            newContact.photo = new Buffer(photo, 'binary').toString('base64');
                                            resolve(newContact);
                                        } else {
                                            newContact.photo = null;
                                            resolve(newContact);
                                        }
                                    });
                                });
                                promises.push(photoPromise);
                            });
                            Promise.all(promises).then(function (contactsWithPhotos) {
                                callback(null, contactsWithPhotos);
                            }).catch(function (photoError) {
                                callback(photoError);
                            });
                        } else {
                            callback(null, []);
                        }
                    } else if (contactsError.statusCode) {
                        callback({ code: constants.STATUS_CODES.INVALID_TOKEN, message: constants.RESPONSE_MESSAGES.INVALID_TOKEN });
                    } else {
                        callback({ code: constants.STATUS_CODES.INTERNAL_SERVER_ERROR, message: constants.RESPONSE_MESSAGES.ERROR });
                    }
                });
            },
            images: function (callback) {
                CustomerImages.findOne(
                    { name: customerName.split('[')[0].trim() },
                    function (error, result) {
                        if (error) {
                            callback(error);
                            return;
                        }
                        callback(null, result);
                    }
                );
            }

        }, function (error, results) {
            if (!error) {
                res.json(results);
            } else if (error.code) {
                res.status(error.code).send(error.message);
            }
        });
    } else {
        console.log('No access token');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.OFFICE_365.TOKEN_NOT_FOUND);
    }
});

router.post('/getScopesAndContactsOfCustomerNew', ensureAuthenticated, async (req, res) => {
    let { userId } = req.body;
    let user = null;
    try {
        user = await User.findOne({ _id: userId }).exec();
        console.log('User', user);
        if (!user.companyName) {
            return res.status(constants.STATUS_CODES.UNAUTHORIZED).send(constants.RESPONSE_MESSAGES.USER.UNAUTHORIZED);
        }
    } catch (e) {
        return res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.USER.NOT_FOUND);
    }

    let query = user.companyName.replace('\'', '\'\'');
    query = encodeURIComponent(query);
    let accessToken = req.cookies.OFFICE_ACCESS_TOKEN;
    let population = [
        {
            path: 'scopes',
            populate: [
                {
                    path: 'task',
                    populate: [
                        {
                            path: 'invoices',
                            populate: [{
                                path: 'selectedScopes.scope',
                                select: '_id id number note price isArchived definition'
                            }],
                            match: {
                                isArchived: false
                            },
                            select: '_id id templates isArchived paid hold poNumber isUpdated sentDate paidDate selectedScopes'
                        },
                        {
                            path: 'teamMembers',
                            select: 'firstName lastName _id id role picture employeeCode'
                        },
                        {
                            path: 'purchaseOrders',
                            match: {
                                isArchived: false
                            },
                            select: '_id id  isArchived number'
                        }
                    ]
                }, {
                    path: 'engineerDetails.engineer',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id role picture employeeCode skypeId email'
                }, {
                    path: 'drafterDetails.drafter',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id role picture employeeCode skypeId email'
                }, {
                    path: 'managerDetails.manager',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id role picture employeeCode skypeId email'
                }, {
                    path: 'itemType'
                }, {
                    path: 'group'
                }, {
                    path: 'hourTrackers',
                    populate: [{
                        path: 'employee',
                        populate: [{
                            path: 'role'
                        }]
                    }]
                }
            ],
            match: {
                isArchived: false
            }
        }
    ];
    if (accessToken) {
        async.parallel({
            customerScopes: function (callback) {
                Task.find({ 'contractor.name': user.companyName }).populate(population).lean().exec(function (customerScopesError, customerTasks) {
                    let allScopes = [];
                    if (!customerScopesError) {
                        if (customerTasks.length > 0) {
                            customerTasks.forEach(function (task) {
                                allScopes = allScopes.concat(task.scopes);
                            });
                        }
                        callback(null, allScopes);
                    } else {
                        callback(customerScopesError);
                    }
                });
            },
            contacts: function (callback) {
                common.getContacts(accessToken, query, req, res, function (contactsError, contacts) {
                    if (!contactsError) {
                        if (contacts.length > 0) {
                            let promises = [];
                            contacts.map(function (contact) {
                                let photoPromise = new Promise(function (resolve, reject) {
                                    common.getPhoto(accessToken, contact.id, req, res, function (photoError, photo) {
                                        let newContact = contact;
                                        if (!photoError) {
                                            newContact.photo = new Buffer(photo, 'binary').toString('base64');
                                            resolve(newContact);
                                        } else {
                                            newContact.photo = null;
                                            resolve(newContact);
                                        }
                                    });
                                });
                                promises.push(photoPromise);
                            });
                            Promise.all(promises).then(function (contactsWithPhotos) {
                                callback(null, contactsWithPhotos);
                            }).catch(function (photoError) {
                                callback(photoError);
                            });
                        } else {
                            callback(null, []);
                        }
                    } else if (contactsError.statusCode) {
                        callback({ code: constants.STATUS_CODES.INVALID_TOKEN, message: constants.RESPONSE_MESSAGES.INVALID_TOKEN });
                    } else {
                        callback({ code: constants.STATUS_CODES.INTERNAL_SERVER_ERROR, message: constants.RESPONSE_MESSAGES.ERROR });
                    }
                });
            },
            images: function (callback) {
                CustomerImages.findOne(
                    { name: user.companyName.split('[')[0].trim() },
                    function (error, result) {
                        if (error) {
                            callback(error);
                            return;
                        }
                        callback(null, result);
                    }
                );
            }

        }, function (error, results) {
            if (!error) {
                res.json(results);
            } else if (error.code) {
                res.status(error.code).send(error.message);
            }
        });
    } else {
        console.log('No access token');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.OFFICE_365.TOKEN_NOT_FOUND);
    }
});

// API to retrieve list of task and their respective scopes for showing in multiple hourtracker dropdown
router.post('/user/task_scope', ensureAuthenticated, async (req, res) => {
    const { userId, keyword } = req.body;
    const query = [
        {
            $addFields: {
                taskNumberFull: { $concat: [{ $substr: [{ $year: '$createdAt' }, 0, -1] }, '-', '$taskNumber'] },
            }
        },
        {
            $match: {
                $and: [
                    {
                        $or: [
                            { title: { $regex: new RegExp(keyword, 'i') } },
                            { taskNumberFull: { $regex: new RegExp(keyword, 'i') } }
                        ]
                    }
                ]
            }
        },
        {
            $lookup: {
                from: 'scopes',
                localField: 'scopes',
                foreignField: '_id',
                as: 'scopes'
            }
        },
        {
            $project: {
                scopes: {
                    $filter: {
                        input: "$scopes",
                        as: "scopes",
                        cond: { $eq: ["$$scopes.isArchived", false] }
                    }
                },
                taskNumber: 1,
                isFromTaskGroup: 1,
                isBidding: 1,
                createdAt: 1,
                id: 1,
                title: 1,
                scopes: {
                    _id: 1,
                    id: 1,
                    number: 1,
                    note: 1,
                    hourTrackers: 1,
                    group: 1
                },
                contractor: {
                    name: 1
                }
            }
        }
    ];

    try {
        const task = await Task.aggregate(query).exec().catch(e => { throw e });
        res.json(task);
    } catch (e) {
        console.log('Error in /user/task_scope API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/maxTaskNumber', ensureAuthenticated, async (req, res) => {
    try {
        const taskNumber = await incrementTaskNumberCounter();
        res.json(taskNumber);
    } catch (e) {
        console.log('Error in /task/maxTaskNumber API: ', e);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const task = await Task
            .findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) })
            .exec();
        res.json(task);
        emitScoket(req, socketType.UPDATE_TASK, task)
    } catch (e) {
        console.log('Error in put task/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/purchase_order', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await addTaskMilestone('purchaseOrders', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_PURCHASE_ORDER, task);
    } catch (e) {
        console.log('Error in put task/:id/purchase_order API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = addTaskMilestone('agreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/master_agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = addTaskMilestone('masterAgreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_MASTER_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/master_agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/modified_agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = addTaskMilestone('modifiedAgreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_MODIFIED_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/modified_agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/client_agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await addTaskMilestone('clientAgreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_CLIENT_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/client_agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/:id/comment', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const comment = req.body;
    comment.isEdited = false;
    comment.id = shortid.generate();
    try {
        const { comments } = await Task
            .findOneAndUpdate({ id }, { $push: { comments: comment } }, { new: true })
            .exec()
            .catch(e => { throw e });
        const createdComment = comments[comments.length - 1];
        res.json(createdComment);
        emitScoket(req, socketType.ADD_TASK_COMMENT, { taskId: id, comment: createdComment })
    } catch (e) {
        console.log('Error in post tasks/:id/comment API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/comment', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const comment = req.body;
    comment.isEdited = true;
    try {
        const { comments } = await Task.findOneAndUpdate({ id, "comments._id": comment._id },
            { $set: { "comments.$": comment } },
            { new: true })
            .exec()
            .catch(e => { throw e });
        const updatedComment = comments.find(item => item._id == comment._id);
        res.json(updatedComment);
        emitScoket(req, socketType.UPDATE_TASK_COMMENT, { taskId: id, comment: updatedComment })
    } catch (e) {
        console.log('Error in put tasks/:id/comment API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/tags', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    const select = {
        id: 1,
        tags: 1
    };
    try {
        const { tags } = await Task.findOneAndUpdate({ id },
            body,
            { new: true, select })
            .exec()
            .catch(e => { throw e });
        res.json(tags);
        emitScoket(req, socketType.UPDATE_TASK_TAGS, { taskId: id, tags })
    } catch (e) {
        console.log('Error in tasks/:id/tags post API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/update-bid-scope-task/:taskID', ensureAuthenticated, (req, res) => {
    const taskID = req.params.taskID;
    let updateData = {
        group: constants.TASK_GROUP__ID.ACTIVE_PROJECTS
    };
    const io = req.app.get('socketio');
    const socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    const population = [{
        path: 'task',
        populate: [
            {
                path: 'agreements',
                match: {
                    isArchived: false
                },
                populate: [{
                    path: 'selectedScopes',
                    select: '_id id number note isArchived'
                }],
                select: '_id id number note isArchived selectedScopes templates'
            },
            {
                path: 'modifiedAgreements',
                match: {
                    isArchived: false
                },
                populate: [{
                    path: 'selectedScopes',
                    select: '_id id number note isArchived'
                }],
                select: '_id id number note isArchived selectedScopes templates'
            },
            {
                path: 'masterAgreements',
                match: {
                    isArchived: false
                },
                populate: [{
                    path: 'selectedScopes',
                    select: '_id id number note isArchived'
                }],
                select: '_id id number note isArchived selectedScopes templates'
            },
            {
                path: 'clientAgreements',
                match: {
                    isArchived: false
                },
                populate: [{
                    path: 'selectedScopes',
                    select: '_id id number note isArchived'
                }],
                select: '_id id number note isArchived selectedScopes templates'
            },
            {
                path: 'purchaseOrders',
                match: {
                    isArchived: false
                },
                populate: [{
                    path: 'selectedScopes',
                    select: '_id id number note isArchived'
                }],
                select: '_id id number note isArchived selectedScopes templates'
            },
            {
                path: 'invoices',
                match: {
                    isArchived: false
                },
                populate: [{
                    path: 'selectedScopes.scope',
                    select: '_id id number note isArchived'
                }],
                select: '_id id number note definition isArchived selectedScopes templates'
            },
            {
                path: 'teamMembers',
                select: 'firstName lastName _id id role picture employeeCode'
            }
        ]
    }, {
        path: 'group'
    }, {
        path: 'engineerDetails.engineer',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    }, {
        path: 'drafterDetails.drafter',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    }, {
        path: 'managerDetails.manager',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    }, {
        path: 'milestone.templates.metaData'
    }, {
        path: 'itemType'
    }, {
        path: 'hourTrackers',
        populate: [{
            path: 'employee',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode'
        }]
    }, {
        path: 'drawings',
        match: {
            isArchived: false
        }
    }, {
        path: 'custDrawings',
        match: {
            isArchived: false
        }
    }, {
        path: 'calcs',
        match: {
            isArchived: false
        }
    }, {
        path: 'letters',
        match: {
            isArchived: false
        }
    }, {
        path: 'tabData',
        match: {
            isArchived: false
        }
    }];

    Task.findOne(
        { _id: taskID },
        (findTaskError, task) => {
            if (findTaskError) {
                res.status(501).json({
                    msg: 'Failed to update task scopes'
                });
                return;
            }
            if (!task) {
                res.status(501).json({
                    msg: 'Someone has already initiated task movement to Active group'
                });
                return;
            }
            if (task.scopes && task.scopes.length > 0) {
                let promises = [];
                task.scopes.map(_id => {
                    let promise = new Promise((resolve, reject) => {
                        Scope.findOneAndUpdate(
                            { _id: _id },
                            updateData,
                            { new: true })
                            .populate(population)
                            .exec(
                                (findScopeError, scope) => {
                                    if (findScopeError) {
                                        reject(findScopeError);
                                    }
                                    scope.task.isBidding = false;
                                    resolve(scope);
                                });
                    }); // end of promise
                    promises.push(promise);
                }); // end of map

                Promise.all(promises).then(scopes => {
                    // if (socket) {
                    //     socket.broadcast.emit(constants.SOCKET_EVENTS.BID_SCOPE_UPDATE, { scopes });
                    // }
                    res.json(scopes);
                }).catch(promiseError => {
                    console.log(promiseError);
                    res.json('error');
                });

                // Scope.findByIdAndUpdate()
                // res.json('sueccs')
            }
        });
});

router.put('/change/group/:taskId', common.ensureAuthenticated, function (req, res) {
    let taskId = req.params.taskId;
    let params = req.body;
    let io = req.app.get('socketio');
    let socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    let population =
        [
            {
                path: 'teamMembers',
                populate: [{
                    path: 'role'
                }],
                select: 'firstName lastName _id id role picture employeeCode'
            },
            {
                path: 'scopes',
                populate: [{
                    path: 'group'
                }, {
                    path: 'task',
                    populate: [
                        {
                            path: 'purchaseOrders',
                            match: {
                                isArchived: false
                            }
                        }, {
                            path: 'agreements',
                            match: {
                                isArchived: false
                            }
                        }, {
                            path: 'invoices',
                            match: {
                                isArchived: false
                            }
                        }, {
                            path: 'teamMembers',
                            populate: [{
                                path: 'role'
                            }],
                            select: 'firstName lastName _id id role picture employeeCode'
                        }
                    ]
                }, {
                    path: 'engineerDetails.engineer',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id role picture employeeCode'
                }, {
                    path: 'drafterDetails.drafter',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id role picture employeeCode'
                }, {
                    path: 'managerDetails.manager',
                    populate: [{
                        path: 'role'
                    }],
                    select: 'firstName lastName _id id role picture employeeCode'
                }, {
                    path: 'milestone.templates.metaData'
                }, {
                    path: 'itemType'
                }, {
                    path: 'hourTrackers',
                    populate: [{
                        path: 'employee',
                        populate: [{
                            path: 'role'
                        }],
                        select: 'firstName lastName _id id role picture employeeCode'
                    }]
                }, {
                    path: 'drawings'
                }, {
                    path: 'calcs'
                }, {
                    path: 'custDrawings'
                }, {
                    path: 'letters'
                }, {
                    path: 'tabData'
                }]
            }, {
                path: 'comments.postedBy'
            }, {
                path: 'purchaseOrders'
            }, {
                path: 'agreements'
            }, {
                path: 'modifiedAgreements'
            }, {
                path: 'masterAgreements'
            }, {
                path: 'clientAgreements'
            }, {
                path: 'invoices',
                populate: [
                    {
                        path: 'lastModifiedBy',
                        select: 'firstName lastName _id id role picture employeeCode'
                    },
                    {
                        path: 'selectedScopes.scope'
                    }
                ]
            }
        ];
    Task.findById({ _id: taskId }, function (taskError, task) {
        if (!taskError) {
             if (!task) {
                res.status(501).json({
                    msg: 'Someone has already initiated task movement to Active group'
                });
                return;
            }
            let needToDeleteId = params._id;
            delete params._id;
            let updatableTask = new Task(params);
            updatableTask.createdAt = params.createdAt;
            updatableTask.scopes = task.scopes;
            updatableTask.save(function (saveError, savedTask) {
                if (!saveError) {
                    Task.remove({ _id: needToDeleteId }).exec(function (removeError, status) {
                        if (!removeError) {
                            savedTask.populate(population, function (populateErr, populatedTask) {
                                if (!populateErr) {
                                    populatedTask = populatedTask.toObject();
                                    let promises = [];
                                    _.forEach(populatedTask.scopes, function (scope) {
                                        let updatableScope = scope;
                                        updatableScope.task = populatedTask._id;
                                        let promise = new Promise(function (resolve, reject) {
                                            Scope.findOneAndUpdate({ id: scope.id }, updatableScope, { new: true }).populate(scopePopulation).lean().exec(function (scopeError, updatedScope) {
                                                if (!scopeError) {
                                                    resolve(updatedScope);
                                                } else {
                                                    reject(scopeError);
                                                }
                                            });
                                        });
                                        promises.push(promise);
                                    });
                                    Promise.all(promises).then(function (updatedScopes) {
                                        populatedTask.scopes = updatedScopes;
                                        emitScoket(req, constants.SOCKET_EVENTS.TASK_UPDATE, { task: populatedTask })
                                        // if (socket) {
                                        //     socket.broadcast.emit(constants.SOCKET_EVENTS.TASK_UPDATE, { task: populatedTask });
                                        // }
                                        res.json(populatedTask);
                                    }).catch(function (scopeError) {
                                        console.log(scopeError);
                                    });
                                } else {
                                    console.log(populateErr);
                                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                }
                            });
                        } else {
                            console.log(removeError);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });
                } else {
                    console.log(saveError);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(taskError);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});


router.delete('/:id/comment/:commentId', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const commentId = req.params.commentId;
    try {
        const { comments } = await Task
            .findOneAndUpdate({ id }, { $pull: { comments: { id: { $eq: commentId } } } }, { new: true })
            .exec()
            .catch(e => { throw e });
        res.json({ id: commentId });
        emitScoket(req, socketType.DELETE_TASK_COMMENT, { taskId: id, comment: { id: commentId } })

    } catch (e) {
        console.log('Error in delete tasks/:id/comment API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
