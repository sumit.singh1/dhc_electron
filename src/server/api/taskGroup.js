var _ = require('lodash');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var TaskGroup = require('../models/TaskGroup');

//common
var common = require('./common');

var error = constants.RESPONSES;

function getTaskGroup(query, population, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    TaskGroup.findOne(query)
        .populate(population)
        .exec(function(err, taskGroup) {
            if (!err) {
                callback(null, taskGroup);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function getTaskGroups(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    TaskGroup.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .lean()
        .exec(function(err, taskGroups) {
            if (!err) {
                callback(null, taskGroups);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.post('/', common.ensureAuthenticated, function(req, res) {
    var title = req.body.title;
    var createdBy = req.body.createdBy;

    var group = new TaskGroup({
        title: title,
        createdBy: createdBy
    });

    group.save(function(err, savedGroup) {
        if (!err) {
            res.json(savedGroup);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function(req, res) {
    getTaskGroups(null, null, null, null, function(err, taskGroups) {
        if (!err) {
            res.json(taskGroups);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/:groupId', common.ensureAuthenticated, function(req, res) {
    var groupId = req.params.groupId;
    var params = req.body;

    TaskGroup.findOneAndUpdate({
        id: groupId
    }, params, {
        new: true
    }, function(err, updatedGroup) {
        if (!err) {
            res.json(updatedGroup);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});


router.delete('/:groupId', common.ensureAuthenticated, function (req, res) {
    TaskGroup.findOneAndRemove({
        id: req.params.groupId
    }, function (err, status) {
        if (!err) {
            res.json(status);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

module.exports = router;
