import express from 'express';

import ScopePlanning from '../models/ScopePlanning';

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { generateSelection } from '../utils/common';
import { createScopePlanning } from '../utils/milestonesCreate';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const scopePlanning = await createScopePlanning();
        res.json(scopePlanning);
        emitScoket(req, socketType.ADD_SCOPE_PLANNING, scopePlanning);
    } catch (e) {
        console.log('Error in post scopePlanning/ API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, scopePlanning } = req.body;
        const result = await ScopePlanning
            .findOneAndUpdate({ id }, scopePlanning, { new: true, select: generateSelection(scopePlanning) })
            .exec().catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_SCOPE_PLANNING, { taskId, scopePlanning: result });
    } catch (e) {
        console.log('Error in put scopePlanning/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, scopePlanning } = req.body;
        const result = await ScopePlanning
            .findOneAndUpdate({ id }, scopePlanning, { new: true, select: generateSelection(scopePlanning) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_SCOPE_PLANNING, { taskId, scopePlanning: result });
    } catch (e) {
        console.log('Error in put scopePlanning/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const scopePlanning = await ScopePlanning.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_SCOPE_PLANNING, { id });
    } catch (e) {
        console.log('Error in delete scopePlanning/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
