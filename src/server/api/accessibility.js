var isEmpty = require('lodash/isEmpty');
var express = require('express');
var router = express.Router();

//constants
var constants = require('../utils/constants');

//models
var Accessibility = require('../models/Accessibility');

//common
var common = require('./common');

var error = constants.RESPONSES;

function getAccessibility(query, population, callback) {
    if (isEmpty(query)) {
        query = {

        };
    }
    if (isEmpty(population)) {
        population = '';
    }
    Accessibility.findOne(query)
        .populate(population)
        .exec(function(err, accessibility) {
            if (!err) {
                callback(null, accessibility);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function getAccessibilities(query, population, sort, limit, callback) {
    if (isEmpty(query)) {
        query = {

        };
    }
    if (isEmpty(population)) {
        population = '';
    }
    if (isEmpty(sort)) {
        sort = '';
    }

    Accessibility.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .exec(function(err, accessibilities) {
            if (!err) {
                callback(null, accessibilities);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.post('/', common.ensureAuthenticated, function(req, res) {
    var data = req.body;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, constants.ACCESS_LEVELS.WRITE, function() {
        var accessibility = new Accessibility(data);
        accessibility.save(function(err, savedAccessibility) {
            if (!err) {
                Accessibility.populate(savedAccessibility, [{
                    path: 'permissions.permission'
                }, {
                    path: 'role'
                }, {
                    path: 'level'
                }], function(err, accessibility) {
                    if (!err) {
                        res.json(accessibility);
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        })
    });
});

router.get('/', common.ensureAuthenticated, function(req, res) {
    var population = [{
        path: 'role'
    }, {
        path: 'level'
    }, {
        path: 'permissions.permission'
    }];
    getAccessibilities(null, population, null, null, function(err, accessibilities) {
        if (!err) {
            res.json(accessibilities);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/:accessId', common.ensureAuthenticated, function(req, res) {
    var accessId = req.params.accessId;
    var params = req.body;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, constants.ACCESS_LEVELS.WRITE, function() {
        Accessibility.findOneAndUpdate({
            id: accessId
        }, params, {
            new: true
        }, function(err, updatedAccessibility) {
            if (!err) {
                Accessibility.populate(updatedAccessibility, [{
                    path: 'permissions.permission'
                }, {
                    path: 'role'
                }, {
                    path: 'level'
                }], function(err, accessibility) {
                    if (!err) {
                        res.json(accessibility);
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

module.exports = router;
