let Client = require('ftp');
let _ = require('lodash');
let fs = require('fs');
let formidable = require('formidable');
let express = require('express');
let router = express.Router();
let async = require('async');
let Promise = require('bluebird');

// logger
let logger = require('../utils/logger');

// constants
let constants = require('../utils/constants');

// models
let AppSetting = require('../models/AppSetting');

// others
let common = require('./common');

function haltOnTimedout(req, res, next) {
    req.connection.setTimeout(30 * 60 * 1000);
    next();
}


function getClient(callback) {
    let config = {
        host: process.env.FTP_HOST_NAME,
        user: process.env.FTP_USER,
        password: process.env.FTP_PASSWORD,
        port: 21,
        secure: true,
        secureOptions: {
            rejectUnauthorized: false
        }
    };

    let client = new Client();
    client.connect(config);
    client.on('ready', function () {
        callback(client);
    });
}

function destroyClient(client) {
    client.end();
}

function uploadFile(client, source, destination, callback) {
    destination = process.env.FTP_BASE_FOLDER + destination;
    client.put(source, destination, function (err) {
        if (!err) {
            callback(null);
        } else {
            console.log(err);
            callback(err);
        }
        destroyClient(client);
    });
}

function downloadFile(client, source, callback) {
    source = process.env.FTP_BASE_FOLDER + source;
    client.get(source, function (err, stream) {
        if (!err) {
            stream.once('close', function () {
                destroyClient(client);
            });
            callback(null, stream);
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function deleteFile(client, path, callback) {
    path = process.env.FTP_BASE_FOLDER + path;
    client.delete(path, function (err) {
        if (!err) {
            callback(null);
        } else {
            console.log(err);
            callback(err);
        }
        destroyClient(client);
    });
}

function getOrCreateFolder(client, path, folderName, callback) {
    let newPath = path + '/' + folderName;
    client.list(path, function (err, list) {
        if (!err) {
            let exist = _.find(list, function (entity) {
                return entity.type === 'd' && entity.name === folderName;
            });
            if (exist) {
                callback(null);
            } else {
                client.mkdir(newPath, function (err) {
                    if (!err) {
                        callback(null);
                    } else {
                        console.log(err);
                        callback(err);
                    }
                });
            }
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function initialSetup(task, callback) {
    let baseFolder = process.env.FTP_BASE_FOLDER.slice(0, -1);
    getClient(function (client) {
        async.waterfall([
            function (innerCallback) {
                let contractorName = task.contractor.company ? task.contractor.company.trim() : task.contractor.name;
                getOrCreateFolder(client, baseFolder, contractorName, function (err) {
                    if (!err) {
                        baseFolder += '/' + contractorName;
                        innerCallback(null);
                    } else {
                        innerCallback(err);
                    }
                });
            },
            function (innerCallback) {
                let year = new Date(task.createdAt).getFullYear();
                year = year.toString();
                getOrCreateFolder(client, baseFolder, year, function (err) {
                    if (!err) {
                        baseFolder += '/' + year;
                        innerCallback(null);
                    } else {
                        innerCallback(err);
                    }
                });
            },
            function (innerCallback) {
                let taskNumber = task.isBidding ? 'B' + new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber :
                    new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber;
                let folderName = task.city + ', ' + task.state + ' - ' + task.title + ' - ' + taskNumber;
                getOrCreateFolder(client, baseFolder, folderName, function (err) {
                    if (!err) {
                        baseFolder += '/' + folderName;
                        innerCallback(null);
                    } else {
                        innerCallback(err);
                    }
                });
            }
            // function (innerCallback) {
            //     var folderName = 'Contracts';
            //     getOrCreateFolder(client, baseFolder, folderName, function (err) {
            //         if (!err) {
            //             innerCallback(null);
            //         } else {
            //             innerCallback(err);
            //         }
            //     });
            // },
            // function (innerCallback) {
            //     var folderName = 'DHC Calcs';
            //     getOrCreateFolder(client, baseFolder, folderName, function (err) {
            //         if (!err) {
            //             innerCallback(null);
            //         } else {
            //             innerCallback(err);
            //         }
            //     });
            // },
            // function (innerCallback) {
            //     var folderName = 'DHC Drawings';
            //     getOrCreateFolder(client, baseFolder, folderName, function (err) {
            //         if (!err) {
            //             innerCallback(null);
            //         } else {
            //             innerCallback(err);
            //         }
            //     });
            // },
            // function (innerCallback) {
            //     AppSetting.findOne(function (err, appSetting) {
            //         if (!err) {
            //             if (appSetting) {
            //                 var promises = [];
            //                 _.forEach(appSetting.attachments, function (attachment) {
            //                     var promise = new Promise(function (resolve) {
            //                         var folderName = attachment.name;
            //                         getOrCreateFolder(client, baseFolder, folderName, function (err) {
            //                             if (!err) {
            //                                 resolve();
            //                             } else {
            //                                 resolve();
            //                             }
            //                         });
            //                     });
            //                     promises.push(promise);
            //                 });
            //                 Promise.all(promises).then(function () {
            //                     innerCallback(null);
            //                 });
            //             } else {
            //                 console.log('No settings found');
            //                 innerCallback(null);
            //             }
            //         } else {
            //             console.log(err);
            //             innerCallback(null);
            //         }
            //     });
            // },
            // function (innerCallback) {
            //     var promises = [];
            //     _.forEach(task.scopes, function (scope) {
            //         var promise = new Promise(function (resolve) {
            //             var folderName = 'Scope ' + scope.number + ' - ' + scope.note;
            //             var calcFolder = baseFolder + '/DHC Calcs';
            //             var drawingFolder = baseFolder + '/DHC Drawings';
            //             getOrCreateFolder(client, calcFolder, folderName, function (err) {
            //                 if (!err) {
            //                     getOrCreateFolder(client, drawingFolder, folderName, function (err) {
            //                         if (!err) {
            //                             resolve();
            //                         } else {
            //                             resolve();
            //                         }
            //                     });
            //                 } else {
            //                     resolve();
            //                 }
            //             });
            //         });
            //         promises.push(promise);
            //     });
            //     Promise.all(promises).then(function () {
            //         innerCallback(null);
            //     });
            // }
        ], function (err) {
            destroyClient(client);
            if (!err) {
                callback(null);
            } else {
                callback(err);
            }
        });
    });
}

function rename(client, oldPath, newPath, callback) {
    client.rename(oldPath, newPath, function (err) {
        if (!err) {
            callback(null);
        } else {
            console.log(err);
            callback(err);
        }
        destroyClient(client);
    });
}

router.post('/initialSetup', common.ensureAuthenticated, function (req, res) {
    let task = req.body;
    initialSetup(task, function (err) {
        if (!err) {
            res.json(true);
        } else {
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/rename', common.ensureAuthenticated, function (req, res) {
    let oldPath = process.env.FTP_BASE_FOLDER + req.body.oldPath;
    let newPath = process.env.FTP_BASE_FOLDER + req.body.newPath;

    getClient(function (client) {
        rename(client, oldPath, newPath, function (err) {
            if (!err) {
                res.json({
                    ok: true
                });
            } else {
                console.log(err, '\n', 'oldPath', oldPath, 'newPath', newPath);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.post('/folder', common.ensureAuthenticated, function (req, res) {
    let path = process.env.FTP_BASE_FOLDER + req.body.path;
    let folderName = req.body.folderName;
    getClient(function (client) {
        getOrCreateFolder(client, path, folderName, function (err) {
            if (!err) {
                res.json({
                    ok: true
                });
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
            destroyClient(client);
        });
    });
});

router.put('/file/upload', common.ensureAuthenticated, haltOnTimedout, function (req, res) {
    let form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (req.timedout) {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ATTACHMENT_UPLOAD.REQUEST_TIMEOUT);
        }
        if (!err) {
            getClient(function (client) {
                let dest = fields.dest;
                let file = files.file;

                uploadFile(client, fs.createReadStream(file.path), dest, function (err) {
                    if (req.timedout) {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ATTACHMENT_UPLOAD.REQUEST_TIMEOUT);
                    }
                    if (!err) {
                        res.json({
                            ok: true
                        });
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/file/download', function (req, res) {
    getClient(function (client) {
        downloadFile(client, req.body.filePath, function (err, stream) {
            if (!err) {
                res.attachment(req.body.fileName);
                stream.pipe(res);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.post('/delete', common.ensureAuthenticated, function (req, res) {
    getClient(function (client) {
        deleteFile(client, req.body.filePath, function (err) {
            if (!err) {
                res.json({
                    ok: true
                });
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

module.exports = router;
