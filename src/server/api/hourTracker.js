import _ from 'lodash';
import express from 'express';
import Promise from 'bluebird';
import moment from 'moment';
import PdfPrinter from 'pdfmake/src/printer';
import Client from 'promise-ftp';
import mongoose from 'mongoose';
import xl from "excel4node";

// constants
import constants from '../utils/constants';
// models
import HourTracker from '../models/HourTracker';
import Scope from '../models/Scope';
import Task from '../models/Task';
import User from '../models/User';
import PaydayReport from '../models/PaydayReport';
import TimeInTimeOut from '../models/TimeInTimeOut';
import {
    hourTrackerPopulation,
    hourTrackerEmployeePopulation,
    hourTrackerScopeGroupPopulation,
    hourTrackerScopePopulation
} from '../utils/populations';

// common
import { ensureAuthenticated, getImageBuffer } from './common';

let router = express.Router();

function getInDateRangeRecords(startDate, endDate, userID) {
    return TimeInTimeOut.find({
        user: userID,
        $and: [{
            date: {
                $gte: moment(startDate).valueOf(),
                $lt: moment(endDate).valueOf()
            }
        }]
    }).exec();
}

async function addTimeInValueForToday({ user, todaysTimeIn, todaysDate, todayTimeOut, todayLunchIn, todayLunchOut }) {
    const result = await getInDateRangeRecords(moment(todaysDate).subtract(1, 'day'), moment(todaysDate).add(1, 'day'), user);
    if (result && result.length === 1) { // already filed todays time in
        await TimeInTimeOut.findOneAndUpdate({ _id: result[0]._id }, { timeIn: todaysTimeIn }).exec();
    } else {                            // not filled
        new TimeInTimeOut({
            user,
            timeOut: todayTimeOut,
            lunchIn: todayLunchIn,
            lunchOut: todayLunchOut,
            timeIn: todaysTimeIn,
            date: todaysDate
        }).save();
    }
}

async function saveDrafterData(array) {
    if (array.length === 1) {
        array.map(item => {
            if (item._id) {
                TimeInTimeOut.findOneAndUpdate({ _id: item._id }, {
                    timeIn: item.timeIn,
                    timeOut: item.timeOut,
                    lunchIn: item.lunchIn,
                    lunchOut: item.lunchOut
                }).exec();
                addTimeInValueForToday(item);
            } else {
                new TimeInTimeOut({
                    user: item.user,
                    timeIn: item.timeIn,
                    timeOut: item.timeOut,
                    lunchIn: item.lunchIn,
                    lunchOut: item.lunchOut,
                    date: item.date
                }).save();
                addTimeInValueForToday(item);
            }
        })
    } else {
        array.map(item => {
            if (item._id) {
                TimeInTimeOut.findOneAndUpdate({ _id: item._id }, {
                    timeIn: item.timeIn,
                    timeOut: item.timeOut,
                    lunchIn: item.lunchIn,
                    lunchOut: item.lunchOut
                }).exec();
            } else {
                new TimeInTimeOut({
                    user: item.user,
                    timeIn: item.timeIn,
                    timeOut: item.timeOut,
                    lunchIn: item.lunchIn,
                    lunchOut: item.lunchOut,
                    date: item.date
                }).save();
            }
        })
    }
}

// API to update time sheet report and update the same to FTP
router.post('/update/report', ensureAuthenticated, async (req, res) => {

    const {
        endDate,
        userId,
        type,
        hourTrackers,
        note,
        isManagerEngineer,
        timeInOutObject
    } = req.body;
    try {

        if (timeInOutObject.length > 0 && !isManagerEngineer) {
            await saveDrafterData(timeInOutObject);
        }

        if (hourTrackers) {
            await hourTrackers.map(hourTracker => HourTracker.findOneAndUpdate({ id: hourTracker.id }, hourTracker.data).exec());
        }

        if (type === 1) {
            await updateCurrentUserPayDayDate({ _id: userId })
                .catch(() => {
                    throw new Error(constants.RESPONSE_MESSAGES.DATE_UPDATE_ERROR);
                });
            return res.json(constants.STATUS_CODES.SUCCESS);
        }

        // update payday report data modal
        await updatePaydayReportModal(req.body)
            .catch(() => {
                throw new Error(constants.RESPONSE_MESSAGES.INTERNAL_SERVER_ERROR);
            });

        const trackers = await getHourTrackersForDateRange(req.body)
            .catch(() => {
                throw new Error(constants.RESPONSE_MESSAGES.REFRESH_PAGE);
            });

        const user = await User.findOne({ _id: userId }).exec()
            .catch(() => {
                throw new Error(constants.RESPONSE_MESSAGES.NO_USER_FOUND);
            });

        const userName = user.firstName + ' ' + user.lastName;

        let signature = null;
        if (user && user.signature) {
            signature = await getImageBuffer(user.signature).catch(() => {
            });
        }

        let pdfData;
        // if user is manager or engineer
        if (isManagerEngineer) {
            const trackers = await getHourTrackersForDateRange(req.body)
                .catch(() => {
                    throw new Error(constants.RESPONSE_MESSAGES.REFRESH_PAGE);
                });
            const group = getPDFData(trackers, endDate);
            pdfData = await createPdf(group, endDate, signature, user.employeeCode, userName, note, isManagerEngineer)
                .catch(() => {
                    throw new Error(constants.RESPONSE_MESSAGES.CREATE_PDF);
                });
        } else {
            const { timeInOutObject } = req.body;
            pdfData = await createPdf(timeInOutObject, endDate, signature, user.employeeCode, userName, note, isManagerEngineer)
                .catch((e) => {
                    console.log(e);
                    throw new Error(constants.RESPONSE_MESSAGES.CREATE_PDF);
                });
        }

        const client = await getClient()
            .catch(() => {
                throw new Error(constants.RESPONSE_MESSAGES.CIENT_CONNECTION_ERROR);
            });
        let fileName = `${process.env.FTP_TIMESHEET_FOLDER}_${user.employeeCode}-${user.firstName}-${user.lastName}-${moment(endDate, 'YYYY-MM-DDTHH:mm:ss.SSS').format('MM_D_YY')}.pdf`;
        await client.put(pdfData, fileName)
            .catch(() => {
                throw new Error(constants.RESPONSE_MESSAGES.CIENT_CONNECTION_ERROR);
            });
        client.end();
        await updateCurrentUserPayDayDate({ _id: userId })
            .catch(() => {
                throw new Error(constants.RESPONSE_MESSAGES.DATE_UPDATE_ERROR);
            });
        res.json(constants.STATUS_CODES.SUCCESS);
    } catch (e) {
        console.log('Error in update/report API : ', e.message);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message || constants.RESPONSE_MESSAGES.REFRESH_PAGE);
    }
});

// API to create hourTracker for a particular user
router.post('/', ensureAuthenticated, async (req, res) => {
    let hourTracker = {
        employee: req.body.employee,
        hoursSpent: req.body.hoursSpent,
        date: req.body.date,
        note: req.body.note,
        other: req.body.other,
        revNumber: req.body.revNumber
    };

    try {
        res.json(await addHourTracker(hourTracker, hourTrackerPopulation));
    } catch (e) {
        console.log('Error in hourtracker API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

// API to create multiple hourTrackers for a partucular user
router.post('/insert/multiple', ensureAuthenticated, async (req, res) => {
    let multipleHourTrackers = req.body;
    try {
        const trackers = await Promise.all(
            multipleHourTrackers.map(async hourTracker => {
                return hourTracker.scope ?
                    addHourTrackerAndUpdateTask(hourTracker, hourTrackerScopeGroupPopulation) :
                    addHourTracker(hourTracker, hourTrackerPopulation);
            })
        ).catch((e) => { throw new Error(e) });
        res.json(trackers);
    } catch (e) {
        console.log('Error in /insert/multiple API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

// This API is no more in use
router.get('/:scopeId', ensureAuthenticated, async (req, res) => {
    let scopeId = req.params.scopeId;
    let query = {
        scope: scopeId
    };
    try {
        res.json(await getHourTrackers(query, hourTrackerPopulation, null, null));
    } catch (e) {
        console.log('Error in hourtracker/:scopeId API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

// API to Update HourTracker  for a partucular user
router.put('/:trackerId', ensureAuthenticated, async (req, res) => {
    console.log('in uod');
    let trackerId = req.params.trackerId;
    let params = req.body;
    let io = req.app.get('socketio');
    let socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    try {
        const updatedHourTracker = await HourTracker.findOneAndUpdate({ id: trackerId }, params, { new: true })
            .populate(hourTrackerPopulation)
            .exec();

        let hourTrackerToSend = {};
        let length = Object.keys(params).length;
        if (length !== 1) {
            params.date = updatedHourTracker.date;
            res.json(params);
            if (params.scopeID && params.groupID) {
                hourTrackerToSend = {
                    scopeID: params.scopeID,
                    groupID: params.groupID
                };
                delete params.scopeID;
                delete params.groupID;
            }
            hourTrackerToSend.hourTracker = params;
            if (socket) {
                socket.broadcast.emit(constants.SOCKET_EVENTS.HOURTRACKER_UPDATE, { hourTracker: hourTrackerToSend });
            }
        } else {
            res.json(updatedHourTracker);
        }
    } catch (e) {
        console.log('Error in hourtracker/:trackerId API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

// Delete HourTrackers API
router.delete('/:trackerId', ensureAuthenticated, async (req, res) => {
    let trackerId = req.params.trackerId;
    try {
        await HourTracker.remove({ _id: trackerId }).exec();
        await Scope.findOneAndUpdate(
            { hourTrackers: mongoose.Types.ObjectId(trackerId) },
            { $pull: { hourTrackers: mongoose.Types.ObjectId(trackerId) } }
        ).exec();
        res.json('success');
    } catch (e) {
        console.log('Error in delete hourTracker API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

// Get Payday report data API
router.post(
    '/get-work-details',
    ensureAuthenticated,
    async (req, res) => {
        const {
            startDate,
            endDate,
            userId
        } = req.body;
        try {
            res.json(await getHourTrackersForTwoWeeks(startDate, endDate, userId));
        } catch (e) {
            console.log('Error in hourTracker/get-work-details API : ', e);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    }
);

// API to download payday report excel for all users
router.post('/download-payday-report', async (req, res) => {

    try {
        const trackers = await getHourtrackersForAllUser(req.body.payDate);
        const employeesData = formatReportData(trackers);
        const wb = await createPaydayReport(employeesData);
        const date = new Date(req.body.payDate);
        date.setDate(date.getDate() + 1);
        res.attachment('PaydayReport ' + date.toDateString() + '.xlsx');
        wb.write('PaydayReport ' + date.toDateString() + '.xlsx', res);
    } catch (e) {
        console.log('Error in delete download-payday-report API: ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

function isDrafterOrAdmin(role) {
    return role === "Admin/AP" || role === "Drafter"
}

function formatReportData(trackers) {
    const employees = [];
    trackers.forEach(tracker => {
        const currentEmployee = employees.find(employee => employee.name === tracker._id.employee);
        if (currentEmployee) {
            if (isDrafterOrAdmin(tracker._id.role)) {
                currentEmployee.regularHours += tracker.hoursSpent > 8 ? 8 : tracker.hoursSpent;
                currentEmployee.overtime += tracker.hoursSpent > 8 ? tracker.hoursSpent - 8 : 0;
                currentEmployee.total += tracker.hoursSpent + tracker.pto;
                currentEmployee.pto += tracker.pto;
            } else {
                currentEmployee.regularHours += tracker.hoursSpent;
                currentEmployee.total += tracker.hoursSpent + tracker.pto;
                currentEmployee.pto += tracker.pto;
            }
        } else {

            if (isDrafterOrAdmin(tracker._id.role)) {
                employees.push({
                    name: tracker._id.employee,
                    regularHours: tracker.hoursSpent > 8 ? 8 : tracker.hoursSpent,
                    overtime: tracker.hoursSpent > 8 ? tracker.hoursSpent - 8 : 0,
                    pto: tracker.pto,
                    total: tracker.hoursSpent + tracker.pto,
                    notes: sortTimesheetNoteByDate(tracker.timesheetNote)
                })
            } else {
                employees.push({
                    name: tracker._id.employee,
                    regularHours: tracker.hoursSpent,
                    overtime: 0,
                    pto: tracker.pto,
                    total: tracker.hoursSpent + tracker.pto,
                    notes: sortTimesheetNoteByDate(tracker.timesheetNote)
                })
            }
        }
    });
    return employees.sort((emp1, emp2) => {
        const emp1LastName = emp1.name.split(' ').pop();
        const emp2LastName = emp2.name.split(' ').pop();
        return emp1LastName > emp2LastName ? 1 : emp1LastName < emp2LastName ? -1 : 0;
    });
}

function sortTimesheetNoteByDate(notes) {
    const sortedNotes = notes.sort((note1, note2) => {
        const date1 = new Date(note1.payDate).getTime();
        const date2 = new Date(note2.payDate).getTime();
        if (date1 > date2) return -1;
        if (date1 < date2) return 1;
        return 0;
    });
    return sortedNotes.length > 0 && sortedNotes[0].note || '';
}

async function createPaydayReport(employees) {

    const wb = new xl.Workbook({
        defaultFont: {
            size: 10,
            name: 'Arial'
        }
    });
    const ws = wb.addWorksheet('Sheet 1', {
        margins: {
            left: 0.44,
            right: 0.44
        },
        pageSetup: {
            paperSize: 'A4_PAPER',
            scale: 80
        },
        printOptions: {
            centerHorizontal: true
        }
    });
    const alignCenter = wb.createStyle({
        alignment: {
            horizontal: 'center'
        }
    });

    const header = [
        "EmployeeName",
        "Regular Hours",
        "Over Time",
        "PTO",
        "Total",
        "Notes"
    ];

    let rowNumber = 1;

    header.forEach((item, index) => {
        ws.cell(rowNumber, index + 1).string(item).style(alignCenter);
        ws.column(index + 1).setWidth(20);
    });

    ws.column(6).setWidth(120);

    employees.forEach(employee => {
        rowNumber += 1;
        ws.cell(rowNumber, 1).string(employee.name).style(alignCenter);
        ws.cell(rowNumber, 2).string(parseFloat(employee.regularHours).toFixed(2)).style(alignCenter);
        ws.cell(rowNumber, 3).string(parseFloat(employee.overtime).toFixed(2)).style(alignCenter);
        ws.cell(rowNumber, 4).string(parseFloat(employee.pto).toFixed(2)).style(alignCenter);
        ws.cell(rowNumber, 5).string(parseFloat(employee.total).toFixed(2)).style(alignCenter);
        ws.cell(rowNumber, 6).string(employee.notes).style(alignCenter);
    });

    return wb;
}

function getHourtrackersForAllUser(date) {
    const endDate = new Date(date);
    return HourTracker.aggregate([
        {
            $match: {
                date: {
                    $gte: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() - 14),
                    $lt: endDate
                }
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'employee',
                foreignField: '_id',
                as: 'employee'
            }
        },
        { $unwind: '$employee' },
        {
            $lookup: {
                from: 'roles',
                localField: 'employee.role',
                foreignField: '_id',
                as: 'employee.role'
            }
        },
        { $unwind: '$employee.role' },
        {
            $match: {
                'employee.payDayFormDate': { $gte: endDate }
            }
        },
        {
            $addFields: {
                pto: { $cond: { if: { $eq: ['$other', 'PTO'] }, then: '$hoursSpent', else: 0 } },
                hoursSpent: { $cond: { if: { $ne: ['$other', 'PTO'] }, then: '$hoursSpent', else: 0 } },
            }
        },
        {
            $group: {
                _id: {
                    employeeId: '$employee._id',
                    employee: { $concat: ['$employee.firstName', ' ', '$employee.lastName'] },
                    date: '$date',
                    role: '$employee.role.name'
                },
                hoursSpent: { $sum: '$hoursSpent' },
                pto: { $sum: '$pto' }
            }
        },
        {
            $lookup: {
                from: 'paydayreports',
                localField: '_id.employeeId',
                foreignField: 'employee',
                as: 'timesheetNote'
            }
        }
    ]).exec();
}

async function updatePaydayReportModal({ userId, note, endDate }) {
    const payDate = new Date(endDate);
    const data = await PaydayReport.findOne({
        employee: userId,
        payDate
    }).exec();
    if (data) {
        data.note = note;
        return PaydayReport.findOneAndUpdate({ id: data.id }, data).exec();
    }

    return new PaydayReport({
        note,
        payDate,
        employee: userId
    }).save();
}

async function addHourTracker(hourTracker, population) {
    const updatedHourTracker = await new HourTracker(hourTracker).save();
    return updatedHourTracker.populate(population).execPopulate();
}

async function addHourTrackerAndUpdateTask(hourTracker, population) {
    const tracker = await addHourTracker(hourTracker, hourTrackerPopulation);
    const scope = await Scope.findOneAndUpdate({ _id: hourTracker.scope }, { $push: { hourTrackers: tracker._id } }, { safe: true })
        .populate(population)
        .lean()
        .exec();
    const task = await Task.findOne({ scopes: scope._id }).exec();
    tracker.scope = scope;
    tracker.task = task;
    return { ...tracker._doc, scope, task };
}

function getHourTrackers(query = {}, population = '', sort = '', limit) {
    return HourTracker.find(query).populate(population).sort(sort).limit(limit).exec();
}

function getDateRange(startDate, stopDate) {
    Date.prototype.addDays = function (days) {
        let dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };
    let dateArray = [];
    let currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate);
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

function getClient() {
    let config = {
        host: process.env.FTP_HOST_NAME,
        user: process.env.FTP_USER,
        password: process.env.FTP_PASSWORD,
        port: 21,
        secure: true,
        secureOptions: {
            rejectUnauthorized: false
        }
    };

    let client = new Client();
    return client.connect(config).then(() => client);
}

function getValues(groups) {
    let weeklyTotal = { hours: 0, pto: 0, total: 0 };
    let sheetTotal = { hours: 0, pto: 0, total: 0 };
    const values = groups.map(entry => {
        if (entry.date === 'total') {
            let data = [
                { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: 'WEEK TOTAL', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: weeklyTotal.hours.toPrecision(3), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: weeklyTotal.pto.toPrecision(3), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
                { text: weeklyTotal.total.toPrecision(3), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' }
            ];

            sheetTotal.hours += weeklyTotal.hours;
            sheetTotal.pto += weeklyTotal.pto;
            sheetTotal.total += weeklyTotal.total;
            weeklyTotal = { hours: 0, pto: 0, total: 0 };
            return data;
        }

        let dt = moment(entry.date, 'YYYY-MM-DDTHH:mm:ss.SSS');

        let day = dt.format('ddd');
        let date = dt.format('MM/D/YY');
        let hourArray = entry.data && entry.data.filter(item => item.other !== 'PTO').map(item => item.hoursSpent) || [];

        let hours = hourArray.length > 0 ? hourArray.reduce((hour1, hour2) => hour1 + hour2) : 0;

        let ptoArray = entry.data.filter(item => item.other === 'PTO').map(item => item.hoursSpent) || [];
        let pto = ptoArray.length > 0 || ptoArray[0] ? ptoArray.reduce((hour1, hour2) => hour1 + hour2) : 0;
        weeklyTotal.hours += hours;
        weeklyTotal.pto += pto;
        weeklyTotal.total = weeklyTotal.total + hours + pto;
        return (
            [
                { text: day, alignment: 'center' },
                { text: date, alignment: 'center' },
                { text: hours.toFixed(2), alignment: 'center' },
                { text: pto.toFixed(2), alignment: 'center' },
                { text: (hours + pto).toFixed(2), alignment: 'center' }
            ]
        );
    });
    values.push([
        { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: 'TOTALS', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: sheetTotal.hours.toFixed(2), alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: sheetTotal.pto.toFixed(2), alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
        { text: sheetTotal.total.toFixed(2), alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' }
    ]);
    return values;
}

function getDrafterRows(data) {
    let totalRegular = 0,
        totalOvertime = 0,
        totalPto = 0;
    let rows = data.map(row => {
        let {
            date,
            lunchIn,
            lunchOut,
            timeIn,
            timeOut,
            overtime,
            pto,
            regular
        } = row;
        totalOvertime += overtime;
        totalRegular += regular;
        totalPto += pto;
        let dayString = moment(date, 'YYYY-MM-DDTHH:mm:ss.SSS').format('MM/DD/YY');
        let dateShort = moment(date, 'YYYY-MM-DDTHH:mm:ss.SSS').format('ddd');
        return (
            [
                { text: dayString, alignment: 'center' },
                { text: dateShort, alignment: 'center' },
                { text: timeIn, alignment: 'center' },
                { text: lunchIn, alignment: 'center' },
                { text: lunchOut, alignment: 'center' },
                { text: timeOut, alignment: 'center' },
                { text: regular.toFixed(2), alignment: 'center' },
                { text: overtime.toFixed(2), alignment: 'center' },
                { text: pto.toFixed(2), alignment: 'center' }
            ]
        );
    });

    rows.push([
        { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: 'Totals', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: '', alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: totalRegular.toFixed(2), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: totalOvertime.toFixed(2), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' },
        { text: totalPto.toFixed(2), alignment: 'center', bold: true, color: '#2C662D', fillColor: '#FCFFF5' }
    ], [
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: '', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: 'Total', alignment: 'center', bold: true, color: '#9F3A38', fillColor: '#FFF6F6' },
            { text: (totalPto + totalOvertime + totalRegular).toFixed(2), alignment: 'center', bold: true, margin: [0, 0, 0, 0], color: '#9F3A38', fillColor: '#FFF6F6' }
        ],
    );

    return rows
}

function getPDFDocDefinitionObjectDrafter(drafterData, employeeCode, employeeName, signature, notes) {
    let rows = getDrafterRows(drafterData);
    let docDefinition = {
        pageMargins: [10, 10, 40, 10],
        pageSize: 'LETTER',
        content: [
            {
                text: '\n\nTimesheet',
                alignment: 'center',
                bold: true
            },
            {
                text: `Employee Id: ${employeeCode}`,
                alignment: 'right',
                textSize: '14px',
                margin: [0, 0, 0, 20]
            },
            {
                style: 'tableExample',
                color: '#444',
                table: {
                    widths: [60, 30, 60, 60, 70, 60, 50, 55, 50],
                    headerRows: 1,
                    body: [
                        [
                            { text: 'Date', style: 'tableHeader', colSpan: 2, alignment: 'center' },
                            {},
                            { text: 'In', style: 'tableHeader', alignment: 'center' },
                            { text: 'Lunch (In)', style: 'tableHeader', alignment: 'center' },
                            { text: 'Lunch (Out)', style: 'tableHeader', alignment: 'center' },
                            { text: 'Out', style: 'tableHeader', alignment: 'center' },
                            { text: 'Regular', style: 'tableHeader', alignment: 'center' },
                            { text: 'Overtime', style: 'tableHeader', alignment: 'center' },
                            { text: 'PTO', style: 'tableHeader', alignment: 'center' }
                        ]
                    ].concat(rows)
                }
            }
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true,
                margin: [0, 0, 0, 10]
            },
            subheader: {
                fontSize: 16,
                bold: true,
                margin: [0, 10, 0, 5]
            },
            tableExample: {
                margin: [0, 5, 0, 15],
                alignment: 'center'
            },
            tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
            },
            values: rows
        }
    };

    docDefinition.content.push({
        alignment: 'left',
        margin: [20, 0, 0, 0],
        fontSize: 10,
        stack: [
            {
                text: 'Note: ',
                height: 100,
                width: 500,
                bold: true
            },
            `${notes}`
        ]

    });
    if (signature) {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    image: signature,
                    height: 40,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    } else {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    text: '',
                    height: 36,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    }
    return docDefinition;

}

function getPDFDocDefinitionObject(timeSheet, employeeCode, employeeName, signature, notes) {
    let values = getValues(timeSheet);
    let docDefinition = {
        pageMargins: [40, 20, 40, 20],
        pageSize: 'LETTER',
        content: [
            {
                text: '\n\n\n\nTimesheet',
                alignment: 'center',
                bold: true
            },
            {
                text: `Employee Id: ${employeeCode}`,
                alignment: 'right',
                textSize: '14px',
                margin: [0, 0, 0, 20]
            },
            {
                style: 'tableExample',
                color: '#444',
                table: {
                    widths: ['*', '*', '*', '*', '*'],
                    headerRows: 1,
                    body: [
                        [
                            { text: 'Day', style: 'tableHeader', alignment: 'center' },
                            { text: 'Date', style: 'tableHeader', alignment: 'center' },
                            { text: 'Hours', style: 'tableHeader', alignment: 'center' },
                            { text: 'PTO', style: 'tableHeader', alignment: 'center' },
                            { text: 'Total', style: 'tableHeader', alignment: 'center' }
                        ]
                    ].concat(values)
                }
            }
        ],
        styles: {
            header: {
                fontSize: 18,
                bold: true,
                margin: [0, 0, 0, 10]
            },
            subheader: {
                fontSize: 16,
                bold: true,
                margin: [0, 10, 0, 5]
            },
            tableExample: {
                margin: [0, 5, 0, 15],
                alignment: 'center'
            },
            tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
            },
            values: values
        }
    };

    docDefinition.content.push({
        alignment: 'left',
        margin: [20, 0, 0, 0],
        fontSize: 13,
        stack: [
            {
                text: 'Note: ',
                height: 100,
                width: 500,
                bold: true
            },
            `${notes}`
        ]

    });
    if (signature) {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    image: signature,
                    height: 40,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    } else {
        docDefinition.content.push({
            alignment: 'right',
            margin: [20, 0, 0, 0],
            stack: [
                {
                    text: '',
                    height: 36,
                    width: 100
                },
                '_____________________',
                `${employeeName}`
            ]
        });
    }
    return docDefinition;
}

async function getHourTrackersForTwoWeeks(startDate, endDate, userId) {
    let query = {
        employee: userId,
        $and: [
            {
                date: {
                    $gte: startDate,
                    $lt: endDate
                }
            }
        ]

    };
    const drafterAdminData = await getInDateRangeRecords(startDate, endDate, userId);
    const hourTrackers = await HourTracker.find(query).populate(hourTrackerEmployeePopulation).lean().exec();
    await Promise.all(
        hourTrackers.map(async (hourTracker) => {
            let scope = await Scope.findOne({ hourTrackers: hourTracker._id }).populate(hourTrackerScopeGroupPopulation).exec();
            // In case if hourTracker is filled for other categoty. there is no scope associated wwith it.
            if (scope) {
                let task = await Task.findOne({ scopes: scope._id }).exec();
                hourTracker.scope = scope;
                hourTracker.task = task;
            }
        })
    ).catch(e => {
        throw new Error(e);
    });
    return { hourTrackers, drafterAdminData };
}

function getHourTrackersForDateRange({ startDate, endDate, userId }) {
    let query = {
        employee: userId,
        $and: [
            {
                date: {
                    $gte: startDate,
                    $lt: endDate
                }
            }
        ]

    };
    return HourTracker.find(query).populate(hourTrackerEmployeePopulation).lean().exec();
}

function updateCurrentUserPayDayDate(query = {}) {
    console.log('update date');
    let todayDate = new Date();
    todayDate.setHours(0, 0, 0, 0);
    return User.findOneAndUpdate(query, { payDayFormDate: todayDate.getTime() }).exec();
}

function getPDFData(trackers, date) {
    const days = 13;
    const startDate = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate() - 2);
    const hourTrackerGrouped = [];
    const lastDate = new Date(startDate.getTime() - days * 24 * 60 * 60 * 1000);
    const twoWeekDatesArray = getDateRange(lastDate, startDate);

    twoWeekDatesArray.map((weekDate, index) => {
        const oneDayData = trackers.filter(hourTracker => weekDate.getDate() === new Date(hourTracker.date).getDate());
        if (index === 6) {
            hourTrackerGrouped.push({
                data: oneDayData,
                date: weekDate
            });
            hourTrackerGrouped.push({
                data: 'week one',
                date: 'total'
            });
        } else {
            hourTrackerGrouped.push({
                data: oneDayData,
                date: weekDate
            });
        }
    });

    hourTrackerGrouped.push({
        data: 'week two',
        date: 'total'
    });

    return hourTrackerGrouped;
}

function createPdf(data, endDate, signature, employeeCode = 0, employeeName = '', notes = '', isManagerEngineer) {
    return new Promise((resolve, reject) => {
        let fonts = {
            Roboto: {
                normal: './src/client/assets/fonts/Roboto-Regular.ttf',
                bold: './src/client/assets/fonts/Roboto-Medium.ttf',
                italics: './src/client/assets/fonts/Roboto-Italic.ttf',
                bolditalics: './src/client/assets/fonts/Roboto-MediumItalic.ttf'
            }
        };
        let printer = new PdfPrinter(fonts);
        let docDefinition;
        if (isManagerEngineer) {
            docDefinition = getPDFDocDefinitionObject(data, employeeCode, employeeName, signature, notes);
        } else {
            docDefinition = getPDFDocDefinitionObjectDrafter(data, employeeCode, employeeName, signature, notes);
        }
        let pdfDoc = printer.createPdfKitDocument(docDefinition);
        let chunks = [];
        pdfDoc.on('data', chunk => chunks.push(chunk));
        pdfDoc.on('end', () => resolve(Buffer.concat(chunks)));
        pdfDoc.on('error', () => reject());
        pdfDoc.end();
    });
}

router.get('/user/:userId/:year/:month', ensureAuthenticated, async (req, res) => {
    let month = parseInt(req.params.month);
    let year = parseInt(req.params.year);
    let startDate = new Date(moment([year, month]));
    let endDate = new Date(moment(startDate).endOf('month'));
    console.log('Start Date: ', startDate, '\nEnd Date: ', endDate);
    let query = {
        employee: req.params.userId,
        date: { $gte: startDate, $lte: endDate }
    };
    const drafterAdminData = await TimeInTimeOut.find({
        user: req.params.userId,
        $and: [{
            date: {
                $gte: moment(startDate).valueOf(),
                $lt: moment(endDate).valueOf()
            }
        }]
    }).exec();
    HourTracker.find(query).populate(hourTrackerEmployeePopulation).lean().exec(function (err, hourTrackers) {
        if (!err) {
            let promises = [];
            _.forEach(hourTrackers, function (hourTracker) {
                let promise = new Promise(function (resolve) {
                    Scope.findOne({
                        hourTrackers: hourTracker._id
                    }).select('number note id _id').exec(function (scopeError, scope) {
                        if (!scopeError) {
                            if (scope) {
                                hourTracker.scope = scope;
                                Task.findOne({
                                    scopes: scope._id
                                }).select('contractor title id _id taskNumber createdAt updatedAt').exec(function (taskError, task) {
                                    if (!taskError) {
                                        if (task) {
                                            hourTracker.task = task;
                                        }
                                        resolve();
                                    } else {
                                        console.log(err);
                                        resolve();
                                    }
                                });
                            } else {
                                resolve();
                            }
                        } else {
                            console.log(err);
                            resolve();
                        }
                    });
                });
                promises.push(promise);
            });

            Promise.all(promises).then(function () {
                HourTracker.find({ employee: req.params.userId }).sort({ date: 1 }).limit(1).select('date').exec(function (oldErr, oldestDoc) {
                    if (!oldErr) {
                        HourTracker.find({ employee: req.params.userId }).sort({ date: -1 }).limit(1).select('date').exec(function (newErr, latestDoc) {
                            if (!newErr) {
                                res.json({ hourTrackers, drafterAdminData, oldestDate: oldestDoc[0] && oldestDoc[0].date, latestDate: latestDoc[0] && latestDoc[0].date });
                            } else {
                                console.log(err);
                                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                            }
                        });
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

export default router;
