var _ = require('lodash');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var RoleLevel = require('../models/RoleLevel');

//common
var common = require('./common');

var error = constants.RESPONSES;

function getRoleLevel(query, population, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    RoleLevel.findOne(query)
        .populate(population)
        .exec(function(err, roleLevel) {
            if (!err) {
                callback(null, roleLevel);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function getRoleLevels(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    RoleLevel.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .lean()
        .exec(function(err, roleLevels) {
            if (!err) {
                callback(null, roleLevels);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.post('/', common.ensureAuthenticated, function(req, res) {
    var name = req.body.name;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, constants.ACCESS_LEVELS.WRITE, function() {
        var level = new RoleLevel({
            name: name
        });

        level.save(function(err, savedLevel) {
            if (!err) {
                res.json(savedLevel);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.get('/search/:query', common.ensureAuthenticated, function(req, res) {
    var query = {
        name: {
            '$regex': '.*' + req.params.query + '.*',
            '$options': 'i'
        }
    };

    getRoleLevels(query, null, null, null, function(err, roleLevels) {
        if (!err) {
            res.json(roleLevels);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function(req, res) {
    getRoleLevels(null, null, null, null, function(err, roleLevels) {
        if (!err) {
            res.json(roleLevels);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/:levelId', common.ensureAuthenticated, function(req, res) {
    var levelId = req.params.levelId;
    var params = req.body;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_ACCESSIBILITY.NAME, constants.ACCESS_LEVELS.WRITE, function() {
        RoleLevel.findOneAndUpdate({
            id: levelId
        }, params, {
            new: true
        }, function(err, updatedLevel) {
            if (!err) {
                res.json(updatedLevel);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        })
    });
});

module.exports = router;
