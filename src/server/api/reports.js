const express = require('express');
const common = require('./common');
const moment = require('moment');
const mongoose = require('mongoose');
const _ = require('lodash');
const async = require('async');
const router = express.Router();

// Constants
const constants = require('../utils/constants');

// Modals
const Scope = require('../models/Scope');
const HourTracker = require('../models/HourTracker');

function getAllMonthsSum(scopes) {
    const months = _.keyBy(moment.monthsShort(), function (month) {
        return month;
    });
    for (const key in months) {
        if (months.hasOwnProperty(key)) {
            months[key] = 0;
        }
    }

    _.forEach(scopes, function (scope) {
        months[moment(new Date(scope.dueDate)).format('MMM')] += scope.price;
    });

    return months;
}

// Get productivity report data
function getProductivityReport(userID, year, callback) {
    const dateQuery = {
        $lt: new Date(Number(year) + 1, 0, 1),
        $gte: new Date(year, 0, 1)

    };
    const engineerQuery = {
        $and: [
            {
                'engineerDetails.engineer': userID
            },
            {

                dueDate: dateQuery
            },
            {
                isArchived: false
            },
            {
                group: { $eq: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS) }
            }
        ]
    };

    const managerQuery = {
        $and: [
            {
                'engineerDetails.engineer': {
                    $ne: userID
                }
            },
            {
                'managerDetails.manager': userID
            },
            {

                dueDate: dateQuery
            },
            {
                isArchived: false
            },
            {
                group: { $eq: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS) }
            }
        ]
    };


    async.parallel({
        engineer: function (asyncCallback) {
            Scope.find(engineerQuery, function (engineerError, engineerScopes) {
                if (!engineerError) {
                    asyncCallback(null, getAllMonthsSum(engineerScopes));
                } else {
                    asyncCallback(engineerError);
                }
            });
        },
        manager: function (asyncCallback) {
            Scope.find(managerQuery, function (managerError, managerScopes) {
                if (!managerError) {
                    asyncCallback(null, getAllMonthsSum(managerScopes));
                } else {
                    asyncCallback(managerError);
                }
            });
        }

    }, function (error, results) {
        if (!error) {
            const response = [];
            response.push({
                data: results.engineer,
                userName: 'AE'
            });
            response.push({
                data: results.manager,
                userName: 'PM Only'
            });

            callback(null, response);
        } else {
            console.error('Productivity report error: ', error);
            callback(error);
        }
    });
}

function isExistReporttype(reportType) {
    return Object.keys(constants.REPORT_TYPES).some(value => constants.REPORT_TYPES[value] === reportType);
}

function formatHourReportData(items, callback) {
    const response = [];
    _.forEach(items, function (item) {
        const data = _.find(response, function (user) {
            return user.userName === item.username;
        });

        if (data) {
            data.data[moment.monthsShort(item.month - 1)] = item.hours;
        } else {
            const userData = {};
            userData[moment.monthsShort(item.month - 1)] = item.hours;
            response.push({
                userName: item.username,
                employeeCode: item.employeeCode,
                role: item.role,
                data: userData
            });
        }
    });
    callback(null, response);
}

function formatReportData(items, callback) {
    const response = [];
    _.forEach(items, function (item) {
        response.push({
            employeeCode: item.employeeCode,
            role: 'Engineer',
            data: {
                totalAndHolidayAmount: Math.round(item.price / item.hours, 2),
                billedAmount: Math.round(item.price / item.billedHours, 2),
                totalAmount: Math.round(item.price / item.totalHours, 2),
                hours: item.hours,
                billedHours: item.billedHours,
                totalHours: item.totalHours,
                price: item.price
            },
            userName: item.engineer
        });
    });
    callback(null, response);
}

function getMonthWiseHoursForTotalBilledDollars(items, callback) {
    const response = [];
    _.forEach(items, item => {
        const data = _.find(response, function (user) {
            return user.userName === item._id.engineer;
        });
        const date = moment(item._id.date);
        if (data) {
            if (data.data[moment.monthsShort(date.month())]) {
                data.data[moment.monthsShort(date.month())] += Math.round(item.price, 2);
            } else {
                data.data[moment.monthsShort(date.month())] = Math.round(item.price, 2);
            }
        } else {
            const userData = {};
            userData[moment.monthsShort(date.month())] = Math.round(item.price, 2);
            response.push({
                userName: item._id.engineer,
                data: userData,
                role: item._id.engineerRole,
                employeeCode: item._id.engineerEmployeeCode
            });
        }
    });
    callback(null, response);
}

function getHoursReport(params, callback) {
    const { userID, year, customer, lineOfWork, reportType, role } = params;
    let matchQuery = {
        $and: [
            {
                date: {
                    $lt: new Date(Number(year) + 1, 0, 1),
                    $gte: new Date(year, 0, 1)
                }
            }
        ]
    };
    if (role === 'Engineer') {
        matchQuery.$and.push({
            employee: { $eq: mongoose.Types.ObjectId(userID) }
        });
    }

    const query = [{ $match: matchQuery }];

    if (reportType === constants.REPORT_TYPES.TOTAL_HOURS) {
        query.push({
            $match: {
                $and: [
                    { other: { $ne: 'PTO' } },
                    { other: { $ne: 'Holiday' } }
                ]
            }
        });
    } else if (reportType === constants.REPORT_TYPES.PTO) {
        query.push({
            $match: {
                other: { $eq: 'PTO' }
            }
        });
    } else if (reportType === constants.REPORT_TYPES.BILLED_HOURS) {
        query.push(
            {
                $match: {
                    other: { $eq: '' }
                }
            },
            {
                $lookup: {
                    from: 'scopes',
                    localField: '_id',
                    foreignField: 'hourTrackers',
                    as: 'scopes'
                }
            },
            {
                $lookup: {
                    from: 'tasks',
                    localField: 'scopes.task',
                    foreignField: '_id',
                    as: 'task'
                }
            },
            {
                $project: {
                    itemType: { $arrayElemAt: ['$scopes.itemType', 0] },
                    customer: { $arrayElemAt: ['$task.contractor.name', 0] },
                    employee: '$employee',
                    date: '$date',
                    other: '$other',
                    hoursSpent: '$hoursSpent'
                }
            }
        );

        if (lineOfWork) {
            query.push({
                $match: {
                    itemType: { $eq: mongoose.Types.ObjectId(lineOfWork) }
                }
            });
        }

        if (customer) {
            query.push({
                $match: {
                    customer: { $eq: customer }
                }
            });
        }
    }

    query.push(
        {
            $group: {
                _id: {
                    employee: '$employee',
                    month: { $month: '$date' }
                },
                hours: { $sum: '$hoursSpent' }
            }
        }
    );

    query.push(
        {
            $lookup: {
                from: 'users',
                localField: '_id.employee',
                foreignField: '_id',
                as: 'employee'
            }
        },
        {
            $lookup: {
                from: 'roles',
                localField: 'employee.role',
                foreignField: '_id',
                as: 'role'
            }
        },
        {
            $project: {
                hours: 1,
                month: '$_id.month',
                role: { $arrayElemAt: ['$role.name', 0] },
                username: { $concat: [{ $arrayElemAt: ['$employee.firstName', 0] }, ' ', { $arrayElemAt: ['$employee.lastName', 0] }] },
                employeeCode: { $arrayElemAt: ['$employee.employeeCode', 0] },
                _id: 0
            }
        },
        {
            $match: {
                $or: [
                    {
                        role: { $eq: 'Engineer' }
                    },
                    {
                        role: { $eq: 'Manager' }
                    }
                ]
            }
        }
    );

    HourTracker.aggregate(query, function (error, result) {
        if (!error) {
            callback(null, result);
        } else {
            callback(error);
        }
    });
}

function getTotalBillingDollarReport(params, callback) {
    const { year, lineOfWork, customer } = params;

    let conditions = [
        {
            group: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS)
        },
        {
            dueDate: {
                $lt: new Date(Number(year) + 1, 0, 1),
                $gte: new Date(year, 0, 1)
            }
        },
        {
            isArchived: false
        }
    ];
    if (lineOfWork) {
        conditions.push({
            itemType: { $eq: mongoose.Types.ObjectId(lineOfWork) }
        });
    }
    if (customer) {
        conditions.push({
            'task.contractor.name': { $eq: customer }
        });
    }
    const query = [
        {
            $lookup: {
                from: 'tasks',
                localField: 'task',
                foreignField: '_id',
                as: 'task'
            }
        },
        {
            $match: {
                $and: conditions
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'engineerDetails.engineer',
                foreignField: '_id',
                as: 'Engineer'
            }
        },
        {
            $lookup: {
                from: 'roles',
                localField: 'Engineer.role',
                foreignField: '_id',
                as: 'engineerRole'
            }
        },
        {
            $project: {
                dueDate: 1,
                price: 1,
                Manager: 1,
                Engineer: 1,
                engineerRole: '$engineerRole.name'
                // managerRole: '$managerRole.name'
            }
        },
        {
            $group: {
                _id: {
                    date: '$dueDate',
                    engineer: { $concat: [{ $arrayElemAt: ['$Engineer.firstName', 0] }, ' ', { $arrayElemAt: ['$Engineer.lastName', 0] }] },
                    engineerRole: { $arrayElemAt: ['$engineerRole', 0] },
                    engineerEmployeeCode: { $concat: [{ $arrayElemAt: ['$Engineer.employeeCode', 0] }] },
                    // managerEmployeeCode: { $concat: [{ $arrayElemAt: ['$Manager.employeeCode', 0] }] },
                    manager: { $concat: [{ $arrayElemAt: ['$Manager.firstName', 0] }, ' ', { $arrayElemAt: ['$Manager.lastName', 0] }] }
                    // managerRole: { $arrayElemAt: ['$managerRole', 0] }
                },
                price: { $sum: '$price' }
            }
        }
    ];

    Scope.aggregate(query, (error, result) => {
        if (!error) {
            callback(null, result);
        } else {
            callback(error);
        }
    });
}

function getDollarPerHourReport(params, callback) {
    const { year, customer, lineOfWork, reportType } = params;

    let conditions = [
        {
            group: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS)
        },
        {
            isArchived: false
        },
        {
            dueDate: {
                $lt: new Date(Number(year) + 1, 0, 1),
                $gte: new Date(year, 0, 1)
            }
        }
    ];
    if (lineOfWork) {
        conditions.push({
            itemType: { $eq: mongoose.Types.ObjectId(lineOfWork) }
        });
    }
    if (customer) {
        conditions.push({
            'task.contractor.name': { $eq: customer }
        });
    }
    const query = [
        {
            $match: {
                $and: conditions
            }
        },
        {
            $group: {
                _id: {
                    employeeId: '$engineerDetails.engineer'
                },
                price: { $sum: '$price' }
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: '_id.employeeId',
                foreignField: '_id',
                as: 'employee'
            }
        },
        {
            $lookup: {
                from: 'hourtrackers',
                localField: '_id.employeeId',
                foreignField: 'employee',
                as: 'hourtrackers'
            }
        },
        {
            $lookup: {
                from: 'roles',
                localField: 'employee.role',
                foreignField: '_id',
                as: 'role'
            }
        }
    ];

    // const reportCondition = [];

    // if (reportType === constants.REPORT_TYPES.DOLLAR_PER_BILLED_HOURS) {
    //     reportCondition.push({
    //         $eq: ['$$hourtracker.other', '']
    //     });
    // } else if (reportType === constants.REPORT_TYPES.DOLLAR_PER_TOTAL_HOURS) {
    //     reportCondition.push({
    //         $ne: ['$$hourtracker.other', 'Holiday']
    //     }, {
    //             $ne: ['$$hourtracker.other', 'PTO']
    //         });
    // }

    query.push(
        {
            $project: {
                price: 1,
                engineer: { $concat: [{ $arrayElemAt: ['$employee.firstName', 0] }, ' ', { $arrayElemAt: ['$employee.lastName', 0] }] },
                employeeCode: { $arrayElemAt: ['$employee.employeeCode', 0] },
                role: { $arrayElemAt: ['$role.name', 0] },
                hourtrackers: {
                    $filter: {
                        input: '$hourtrackers',
                        as: 'hourtracker',
                        cond: {
                            $and: [
                                { $gte: ['$$hourtracker.date', new Date(year, 0, 1)] },
                                { $lt: ['$$hourtracker.date', new Date(Number(year) + 1, 0, 1)] }
                            ]
                        }
                    }
                },
                billedHourtrackers: {
                    $filter: {
                        input: '$hourtrackers',
                        as: 'hourtracker',
                        cond: {
                            $and: [
                                { $eq: ['$$hourtracker.other', ''] },
                                { $gte: ['$$hourtracker.date', new Date(year, 0, 1)] },
                                { $lt: ['$$hourtracker.date', new Date(Number(year) + 1, 0, 1)] }
                            ]
                        }
                    }
                },
                totalHourtrackers: {
                    $filter: {
                        input: '$hourtrackers',
                        as: 'hourtracker',
                        cond: {
                            $and: [
                                { $ne: ['$$hourtracker.other', 'Holiday'] },
                                { $ne: ['$$hourtracker.other', 'PTO'] },
                                { $gte: ['$$hourtracker.date', new Date(year, 0, 1)] },
                                { $lt: ['$$hourtracker.date', new Date(Number(year) + 1, 0, 1)] }
                            ]
                        }
                    }
                }
            }
        },
        {
            $project: {
                hours: { $sum: '$hourtrackers.hoursSpent' },
                billedHours: { $sum: '$billedHourtrackers.hoursSpent' },
                totalHours: { $sum: '$totalHourtrackers.hoursSpent' },
                price: 1,
                engineer: 1,
                employeeCode: 1,
                role: 1
            }
        }
    );

    Scope.aggregate(query, (error, result) => {
        if (!error) {
            callback(null, result);
        } else {
            callback(error);
        }
    });
}

/**
 * get report details for user according filter
 */
router.post('/get-report-data', common.ensureAuthenticated, function (request, response) {
    const { userID, reportType, year, empID, role } = request.body;

    if (!(year && userID && isExistReporttype(reportType))) {
        response.status(500).json({
            error: constants.REPORT_ERROR.INVALID_ARGUMENTS,
            msg: constants.ERROR_MSG.INVALID_ARGUMENTS
        });
        return;
    }
    if (role !== 'Manager' && role !== 'Engineer') {
        response.status(500).json({
            error: constants.REPORT_ERROR.UNAUTHORIZED,
            msg: constants.ERROR_MSG.UNAUTHORIZED
        });
        return;
    }
    // if report type is productivity report get data and return
    switch (reportType) {
    case constants.REPORT_TYPES.PRODUCTIVITY:
        const id = empID ? empID : userID;
        getProductivityReport(id, year, function (error, result) {
            if (!error) {
                response.json(result);
            } else {
                response.status(500).json({
                    error: 500,
                    msg: 'Something went wrong.'
                });
            }
        });
        break;
    case constants.REPORT_TYPES.TOTAL_BILLING_DOLLARS:
        getTotalBillingDollarReport(
            request.body,
            (error, result) => {
                if (!error) {
                    getMonthWiseHoursForTotalBilledDollars(
                        result,
                        (err, data) => {
                            if (!err) {
                                response.json(data);
                            } else {
                                response.status(500).json(err);
                            }
                        }
                    );
                    // response.json(result);
                } else {
                    response.status(500).json(error);
                }
            }
        );
        break;
    case constants.REPORT_TYPES.TOTAL_HOURS:
    case constants.REPORT_TYPES.PTO:
    case constants.REPORT_TYPES.TOTAL_HOURS_PTO:
    case constants.REPORT_TYPES.BILLED_HOURS:
        getHoursReport(
            request.body,
            (error, result) => {
                if (!error) {
                    formatHourReportData(
                        result,
                        (err, data) => {
                            if (!err) {
                                response.json(data);
                            } else {
                                response.status(500).json(err);
                            }
                        }
                    );
                } else {
                    response.status(500).json({
                        error: constants.REPORT_ERROR.SCOPE_ERROR,
                        msg: constants.ERROR_MSG.UNKNOWN_ERROR
                    });
                }
            }
        );
        break;
    case constants.REPORT_TYPES.DOLLAR_PER_HOUR:
        getDollarPerHourReport(
            request.body,
            (error, result) => {
                if (!error) {
                    formatReportData(
                        result,
                        (err, data) => {
                            if (!err) {
                                response.json(data);
                            } else {
                                response.status(500).json(err);
                            }
                        }
                    );
                } else {
                    response.status(500).json({
                        error: constants.REPORT_ERROR.SCOPE_ERROR,
                        msg: constants.ERROR_MSG.UNKNOWN_ERROR
                    });
                }
            }
        );
        break;
    default:
        response.status(404).json({
            error: constants.REPORT_ERROR.INVALID_ARGUMENTS,
            msg: constants.ERROR_MSG.UNKNOWN_ERROR
        });
        break;
    }
});

module.exports = router;
