import express from 'express';

import Keyword from '../models/Keyword';
import ScopeKeyword from '../models/ScopeKeyword';
import { ensureAuthenticated } from './common';
var common = require('./common');
const router = express.Router();
import { emitScoket, socketType } from '../utils/socket';

router.put('/:id', ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const keyword = req.body;
        const result = await Keyword
            .findOneAndUpdate({ id }, keyword, { new: true })
            .exec()
            .catch(e => { throw e });
        res.json(result);
    } catch (e) {
        console.log('Error in put keyword/:id', e);
    }
});

router.put('/update-custom-keyword/:id', ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const {
            keyword,
            scopeKeywordId,
            taskId
        } = req.body;
        await Keyword
            .findOneAndUpdate({ id: keyword.id }, keyword, { new: true })
            .exec()
            .catch(e => { throw e });
        const result = await ScopeKeyword
            .findOneAndUpdate({ id: scopeKeywordId }, { enabled: false }, { new: true })
            .populate([{
                path: 'customKeyword.keyword',
            }, {
                path: 'itemTypeKeywords.keyword',
            }])
            .exec()
            .catch(e => { throw e });
        res.json({ scopeKeyword: result, taskId });
        emitScoket(req, socketType.UPDATE_SCOPE_KEYWORD, { scopeKeyword: result, taskId });
        // emitScoket(req, socketType.UPDATE_CUSTOM_SCOPE_KEYWORD, { scopeKeyword: result, taskId });
        // UPDATE_CUSTOM_SCOPE_KEYWORD: 'update_custom_scope_keyword',
    } catch (e) {
        console.log('Error in put keyword/:id', e);
    }
});


router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const newKeyword = await new Keyword({ title: req.body.title }).save().catch(e => { throw e });
        res.json(newKeyword);
        // emitScoket(req, socketType.ADD_TAG, newTag);
    } catch (e) {
        console.log('Error in KeywordPost post API', e);
        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
    }
})

router.post('/search', ensureAuthenticated, async (req, res) => {
    try {
        var query = { title: { $regex: req.body.keyword, $options: 'i' } }
        let keywords = await Keyword.find(query).exec();
        keywords = keywords.filter(item => item.title !== "All Customers")
        res.json(keywords)
    } catch (error) {
        console.log('error', error)
    }
})

export default router;
