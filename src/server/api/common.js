let jwt = require('jsonwebtoken');
let mongoose = require('mongoose');
let moment = require('moment');
let _ = require('lodash');
let nodemailer = require('nodemailer');
let OAuth = require('oauth');
let axios = require('axios');
let uuid = require('uuid');
import User from '../models/User';
import Accessibility from '../models/Accessibility';

const customerAPI = {
    ['/api/app-settings/master-data']: true,
    ['/api/roles']: true,
    ['/api/task-groups']: true,
    ['/api/tasks/getScopesAndContactsOfCustomerNew']: true,
    ['/api/office/authUrl']: true,
    ['/api/office']: true,
    ['/api/office/access-token-for-customer']: true,
    ['/api/tasks']: true,
    ['/api/users']: true,
    ['/api/users/password']: true
};
// constant
let constants = require('../utils/constants');

// models
let Permission = require('../models/Permission');
let Scope = require('../models/Scope');

let transport = nodemailer.createTransport({
    host: 'smtp.sendgrid.net',
    port: 465,
    auth: {
        user: 'apikey',
        pass: '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'
        // pass: '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'
    }
});

let authority = 'https://login.microsoftonline.com/common';
let authorizeEndpoint = '/oauth2/v2.0/authorize';
let tokenEndpoint = '/oauth2/v2.0/token';

function hasAccessTokenExpired(e) {
    let expired = false;
    if (e.status === 401 && e.statusText === 'Unauthorized') {
        expired = true;
    }
    return expired;
}

function getTokenFromRefreshToken(refreshToken, callback) {
    let OAuth2 = OAuth.OAuth2;
    let oauth2 = new OAuth2(
        process.env.OFFICE_CLIENT_ID,
        process.env.OFFICE_CLIENT_SECRET,
        authority,
        authorizeEndpoint,
        tokenEndpoint
    );

    oauth2.getOAuthAccessToken(
        refreshToken, {
            grant_type: 'refresh_token',
            redirect_uri: process.env.APP_URL,
            response_mode: 'form_post',
            nonce: uuid.v4()
        },
        function (err, accessToken, refreshToken) {
            callback(err, accessToken, refreshToken);
        }
    );
}

exports.generateToken = function (user) {
    let payload = {
        iss: process.env.DOMAIN,
        sub: user.id,
        iat: moment().unix(),
        exp: moment().add(7, 'days').unix(),
        role: {
            name: user.role.name,
            level: user.roleLevel.name
        }
    };
    return jwt.sign(payload, process.env.TOKEN_SECRET);
};

// return true if route is present in customerAPI else false
function canIAccess(route) {
    return customerAPI[route] || false;
}

exports.ensureAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) {
        const user = jwt.decode(req.get('Authorization').split(' ')[1]);
        if (user.role.name === 'Customer') {
            if (canIAccess(req.originalUrl) || canIAccess(req.baseUrl)) {
                next();
                return;
            }
            res.status(constants.STATUS_CODES.FORBIDDEN).send(constants.RESPONSE_MESSAGES.FORBIDDEN);
            return;
        }
        next();
    } else {
        res.status(constants.STATUS_CODES.INVALID_TOKEN).send(constants.RESPONSE_MESSAGES.INVALID_TOKEN);
    }
};

exports.hasPermission = function (req, res, permissionName, accessLevel, next) {
    Permission.findOne({
        name: permissionName,
        isActive: true
    }, function (err, permission) {
        if (!err) {
            if (permission) {
                let hasPermission = null;
                hasPermission = _.find(req.user.permissions, function (permission) {
                    return permission.permission.name === permissionName && req.user.isActive && permission.permission.isActive;
                });

                if (hasPermission) {
                    if (hasPermission[accessLevel]) {
                        next();
                    } else {
                        console.log('User is Unauthorized');
                        res.status(constants.STATUS_CODES.UNAUTHORIZED).send(constants.RESPONSE_MESSAGES.USER.UNAUTHORIZED);
                    }
                } else {
                    console.log('User is Unauthorized');
                    res.status(constants.STATUS_CODES.UNAUTHORIZED).send(constants.RESPONSE_MESSAGES.USER.UNAUTHORIZED);
                }
            } else {
                console.log('User is Unauthorized');
                res.status(constants.STATUS_CODES.UNAUTHORIZED).send(constants.RESPONSE_MESSAGES.USER.UNAUTHORIZED);
            }
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
};

exports.sendEmail = function (res, to, subject, html, next) {
    transport.sendMail({
        from: process.env.EMAIL_FROM,
        to: to,
        subject: subject,
        html: html
    }, function (err, info) {
        if (!err) {
            next();
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
};

exports.getBoxAccessToken = function (req, res, next) {
    let boxAccessToken = req.cookies.boxAccessToken;
    if (boxAccessToken) {
        next(boxAccessToken);
    } else {
        console.log('Invalid token');
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
};

exports.getHours = function (user, callback) {
    let query = {
        $and: [
            {
                group: { $ne: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS) }
            },
            {
                $or: [
                    { 'engineerDetails.engineer': user._id },
                    { 'drafterDetails.drafter': user._id },
                    { 'managerDetails.manager': user._id }
                ]
            },
            {
                isArchived: false
            }
        ]
    };

    Scope.aggregate([
        {
            $match: query
        }, {
            $group: {
                _id: user._id,
                totalUrgentHours: {
                    $sum: {
                        $cond: {
                            if: { $eq: ['$engineerDetails.engineer', user._id] },
                            then: {
                                $cond: {
                                    if: {
                                        $and: [
                                            {
                                                $or: [
                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.BIDS)] },
                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.TASKS)] }
                                                ]
                                            },
                                            { $eq: ['$engineerDetails.status', 'Complete'] }
                                        ]
                                    },
                                    then: 0,
                                    else: '$engineerDetails.urgentHours'
                                }
                            },
                            else: {
                                $cond: {
                                    if: {
                                        $eq:
                                            ['$drafterDetails.drafter', user._id]
                                    }, then: {
                                        $cond: {
                                            if: {
                                                $and: [
                                                    {
                                                        $or: [
                                                            { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.BIDS)] },
                                                            { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.TASKS)] }
                                                        ]
                                                    },
                                                    { $eq: ['$drafterDetails.status', 'Complete'] }
                                                ]
                                            },
                                            then: 0,
                                            else: '$drafterDetails.urgentHours'
                                        }
                                    }, else: {
                                        $cond: {
                                            if: {
                                                $eq:
                                                    ['$managerDetails.manager', user._id]
                                            }, then: {
                                                $cond: {
                                                    if: {
                                                        $and: [
                                                            {
                                                                $or: [
                                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.BIDS)] },
                                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.TASKS)] }
                                                                ]
                                                            },
                                                            { $eq: ['$drafterDetails.status', 'Complete'] }
                                                        ]
                                                    },
                                                    then: 0,
                                                    else: '$managerDetails.urgentHours'
                                                }
                                            }, else: 0
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                totalNonUrgentHours: {
                    $sum: {
                        $cond: {
                            if: { $eq: ['$engineerDetails.engineer', user._id] },
                            then: {
                                $cond: {
                                    if: {
                                        $and: [
                                            {
                                                $or: [
                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.BIDS)] },
                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.TASKS)] }
                                                ]
                                            },
                                            { $eq: ['$engineerDetails.status', 'Complete'] }
                                        ]
                                    },
                                    then: 0,
                                    else: '$engineerDetails.nonUrgentHours'
                                }
                            },
                            else: {
                                $cond: {
                                    if: {
                                        $eq:
                                            ['$drafterDetails.drafter', user._id]
                                    }, then: {
                                        $cond: {
                                            if: {
                                                $and: [
                                                    {
                                                        $or: [
                                                            { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.BIDS)] },
                                                            { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.TASKS)] }
                                                        ]
                                                    },
                                                    { $eq: ['$drafterDetails.status', 'Complete'] }
                                                ]
                                            },
                                            then: 0,
                                            else: '$drafterDetails.nonUrgentHours'
                                        }
                                    }, else: {
                                        $cond: {
                                            if: {
                                                $eq:
                                                    ['$managerDetails.manager', user._id]
                                            }, then: {
                                                $cond: {
                                                    if: {
                                                        $and: [
                                                            {
                                                                $or: [
                                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.BIDS)] },
                                                                    { $eq: ['$group', mongoose.Types.ObjectId(constants.TASK_GROUP__ID.TASKS)] }
                                                                ]
                                                            },
                                                            { $eq: ['$engineerDetails.status', 'Complete'] }
                                                        ]
                                                    },
                                                    then: 0,
                                                    else: '$managerDetails.nonUrgentHours'
                                                }
                                            }, else: 0
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    ], function (error, totalHours) {
        if (!error) {
            if (totalHours.length > 0) {
                callback(null, {
                    urgentHours: totalHours[0].totalUrgentHours,
                    nonUrgentHours: totalHours[0].totalNonUrgentHours
                });
            } else {
                callback(null, {
                    urgentHours: 0,
                    nonUrgentHours: 0
                });
            }
        } else {
            callback(error);
        }
    });
};

/**
 *
 * @param {*} accessToken get the access token from cookies
 * @param {*} query  pass company name as the query to get contacts from that company
 * @param {*} req pass the request object of the api
 * @param {*} res pass the response object of the api
 * @param {*} callback pass a callback function to get the contacts
 * @returns {*} returns the contacts array
 * @description In future, use this function to get contacts from any file in the project
 */
exports.getContacts = function (accessToken, query, req, res, callback) {
    let url = 'https://graph.microsoft.com/v1.0/me/contacts?$filter=startswith(companyName, \'' + query + '\')&$top=10000&$count=true';

    let config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };
    axios.get(url, config).then(function (response) {
        callback(null, response.data.value);
    }).catch(function (contactError) {
        if (hasAccessTokenExpired(contactError.response)) {
            let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
            getTokenFromRefreshToken(refreshToken, function (tokenError, accessToken, refreshToken) {
                if (!tokenError) {
                    res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
                    res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);

                    config = {
                        headers: {
                            Authorization: 'Bearer ' + accessToken
                        }
                    };

                    axios.get(url, config).then(function (response) {
                        callback(null, response.data.value);
                    }).catch(function (err) {
                        console.error(err);
                        callback(err);
                    });
                } else {
                    console.error(tokenError);
                    callback({
                        statusCode: constants.STATUS_CODES.INVALID_TOKEN
                    });
                }
            });
        } else {
            console.error(contactError);
            callback(contactError);
        }
    });
};

exports.getPhoto = function (accessToken, contactID, req, res, callback) {
    let url = `https://graph.microsoft.com/v1.0/me/contacts/${contactID}/photo/$value`;

    let config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        },
        responseType: 'arraybuffer'
    };

    axios.get(url, config).then(function (response) {
        callback(null, response.data);
    }).catch(function (contactError) {
        if (contactError.response && hasAccessTokenExpired(contactError.response)) {
            let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
            getTokenFromRefreshToken(refreshToken, function (tokenError, accessToken, refreshToken) {
                if (!tokenError) {
                    res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
                    res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);

                    config = {
                        headers: {
                            Authorization: 'Bearer ' + accessToken
                        }
                    };

                    axios.get(url, config).then(function (response) {
                        callback(null, response.data);
                    }).catch(function (err) {
                        callback(err);
                    });
                } else {
                    callback({
                        statusCode: constants.STATUS_CODES.INVALID_TOKEN
                    });
                }
            });
        } else {
            callback(contactError);
        }
    });
};


exports.getImageBuffer = function (url) {
    let config = {
        responseType: 'arraybuffer'
    };

    return axios.get(url, config)
        .then(response =>
            'data:' + response.headers['content-type'] + ';base64,' + new Buffer(response.data).toString('base64'));
};

exports.addUserToRequest = function (req, res, next) {
    if (req.isAuthenticated()) {
        let payload = req.isAuthenticated();
        User.findOne({
            id: payload.sub,
            isActive: true
        })
            .populate('role')
            .lean()
            .exec(function (err, user) {
                if (user) {
                    Accessibility
                        .findOne({
                            role: user.role._id,
                            level: user.roleLevel
                        })
                        .populate('permissions.permission')
                        .exec(function (err, access) {
                            if (access) {
                                user.permissions = access.permissions;
                            }
                            req.user = user;
                            next();
                        });
                } else {
                    res.status(401).send('User is Unauthorized');
                }
            });
    } else {
        res.status(401).send('User is Unauthorized');
    }
};
