import { get } from 'axios';
import express from 'express';
import find from 'lodash/find';
import forEach from 'lodash/forEach';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import PdfPrinter from 'pdfmake/src/printer';

import Agreement from '../models/Agreement';
import Task from '../models/Task';
import { generateSelection, getTokenFromRefreshToken, hasAccessTokenExpired } from '../utils/common';
import constants, { COOKIES, RESPONSE_MESSAGES, STATUS_CODES } from '../utils/constants';
import { addTaskMilestone, createAgreement } from '../utils/milestonesCreate';
import { selectedScopePopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common, { ensureAuthenticated } from './common';


const router = express.Router();

let request = require('request').defaults({ encoding: null });

function getPDFDocDefinitionObject(docObject) {
    let clientAddress = docObject.clientAddress;
    let task = docObject.task;

    let docDefinition = {
        pageMargins: [40, 60, 40, 60],
        pageSize: 'LETTER',
        content: [
            {
                image: docObject.titleBlockURL,
                width: 550,
                height: 100,
                absolutePosition: { x: 25, y: 0 }
            },
            {
                text: '\n\n\n\nJOB NO.:  ' + docObject.jobNo,
                alignment: 'center',
                bold: true
            },
            {
                text: 'CONSULTING SERVICES AGREEMENT',
                alignment: 'center',
                decoration: 'underline',
                bold: true,
                margin: [0, 0, 0, 6]
            },

            {
                text: 'SCOPE OF SERVICES',
                decoration: 'underline',
                alignment: 'left',
                bold: true,
                margin: [0, 0, 0, 6]
            },
            {
                text: [
                    {
                        text: '         D.H. Charles Engineering, Inc. (the “Consultant”), whose qualifying engineer is Jasper Calcara, P.E., Registration No. 62941, located at 4706 Hoen Ave, Santa Rosa, California 95405, will provide professional consulting engineering services to ' + docObject.clientCompany + ' (the “Client”), located at ' + clientAddress.street + ', ' + clientAddress.city + ', ' + clientAddress.state + ', ' + clientAddress.postalCode + ' for the ',
                        alignment: 'justify'
                    },
                    {
                        text: task.title + ' Project',
                        decoration: 'underline',
                        alignment: 'justify'
                    },
                    {
                        text: ' (the “Project”) in ' + task.city + ', ' + task.state + ' as described in this Scope of Services as follows:',
                        alignment: 'justify'
                    }
                ],
                margin: [0, 0, 0, 8]
            },
            {
                text: docObject.scopes,
                margin: [20, 20, 0, 0],
                alignment: 'justify'
            },
            {
                text: docObject.checkOptionsAndRender,
                margin: [0, 0, 0, 30]
            },
            {
                columns: [
                    [
                        {
                            margin: [0, 0, 0, 25],
                            columns: [
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    {
                                                        image: docObject.signatureURL,
                                                        height: 36,
                                                        width: 100
                                                    },
                                                    '_____________________',
                                                    'Consultant Signature'
                                                ]
                                            },
                                            {
                                                stack: [
                                                    {
                                                        text: docObject.date,
                                                        margin: [0, 22, 0, 0]
                                                    },
                                                    '_____________',
                                                    'Date'
                                                ]
                                            }
                                        ],
                                        alignment: 'left'
                                    }
                                ],
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    {
                                                        text: '                  ',
                                                        margin: [0, 22, 0, 0]
                                                    },
                                                    '_____________________',
                                                    'Client Signature'
                                                ]
                                            },
                                            {
                                                stack: [
                                                    {
                                                        text: '               ',
                                                        margin: [0, 22, 0, 0]
                                                    },

                                                    '_____________',
                                                    'Date'
                                                ]
                                            }
                                        ],
                                        alignment: 'right'
                                    }
                                ]
                            ]
                        },
                        {
                            margin: [0, 0, 0, 25],
                            columns: [
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    docObject.managerName,
                                                    '_____________________________________',
                                                    'Name (Printed)'
                                                ]
                                            }
                                        ],
                                        alignment: 'left'
                                    }
                                ],
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    '                     ',
                                                    '_____________________________________________',
                                                    'Name (Printed)'
                                                ]
                                            }
                                        ],
                                        alignment: 'right'
                                    }
                                ]
                            ]
                        },
                        {
                            margin: [0, 0, 0, 25],
                            columns: [
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    docObject.managerTitle,
                                                    '_____________________________________',
                                                    'Title of Consultant\'s Representative'
                                                ]
                                            }
                                        ],
                                        alignment: 'left'
                                    }
                                ],
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    '                ',
                                                    '_____________________________________________',
                                                    'Title of Client\'s Representative'
                                                ]
                                            }
                                        ],
                                        alignment: 'right'
                                    }
                                ]
                            ]
                        },
                        {
                            margin: [0, 0, 0, 25],
                            columns: [
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    'D.H. Charles Engineering, Inc.',
                                                    '_____________________________________',
                                                    'Consultant\'s Company Name'
                                                ]
                                            }
                                        ],
                                        alignment: 'left'
                                    }
                                ],
                                [
                                    {
                                        columns: [
                                            {
                                                stack: [
                                                    docObject.clientCompany,
                                                    '_____________________________________________',
                                                    'Client\'s Company Name'
                                                ]
                                            }
                                        ],
                                        alignment: 'right'
                                    }
                                ]
                            ]
                        }
                    ]
                ]
            }
        ],
        header: function (currentPage) {
            return {
                text: [
                    { text: currentPage === 1 ? '' : 'Job No. ' + docObject.jobNo, alignment: 'right' }
                ],
                margin: [40, 15, 15, 15]
            };
        },
        footer: function (currentPage, pageCount) {
            let initial;
            if (currentPage === pageCount) {
                initial = {
                    text: '',
                    width: 40,
                    height: 40,
                    absolutePosition: { x: 110, y: 0 }
                };
            } else {
                initial = {
                    image: docObject.initialsURL,
                    width: 40,
                    height: 40,
                    absolutePosition: { x: 110, y: 0 }
                };
            }
            return {
                margin: [0, 27, 40, 0],
                columns: [
                    initial,
                    {
                        text: currentPage === pageCount ? '' : 'Consultant_____________ ',
                        alignment: 'left'
                    },
                    {
                        text: currentPage.toString() + ' of ' + pageCount,
                        alignment: 'center'
                    },
                    {
                        text: currentPage === pageCount ? '' : 'Client____________',
                        alignment: 'right'
                    }
                ]
            };
        }
    };

    return docDefinition;
}

function _extractPersonalNotes(personalNotes) {
    let payload = {};
    if (personalNotes && personalNotes.length > 0) {
        personalNotes = personalNotes.split(';');
        for (let i = 0; i < personalNotes.length; i++) {
            let keyValue = personalNotes[i].split(':');
            switch (keyValue[0].trim()) {
                case 'Company':
                    payload.company = keyValue[1].trim();
                    break;

                case 'Bill Branch':
                    payload.billBranch = keyValue[1].trim();
                    break;

                case 'PO Required':
                    payload.poRequired = keyValue[1].trim();
                    break;

                case 'Include Contact':
                    payload.includeContacts = keyValue[1].trim();
                    break;

                case 'Invoice Contact':
                    payload.invoiceContact = keyValue[1].trim();
                    break;

                case 'Include Client Job Number':
                    payload.includeClientJobNo = keyValue[1].trim();
                    break;

                case 'Contract':
                    payload.contract = keyValue[1].trim();
                    break;
            }
        }
    }

    return payload;
}

function getContactDetails(id, req, res, callback) {
    let url = 'https://graph.microsoft.com/v1.0/me/contacts/' + id;

    let config = {
        headers: {
            Authorization: 'Bearer ' + req.cookies.OFFICE_ACCESS_TOKEN
        }
    };

    get(url, config).then(function (response) {
        callback(null, response.data);
    }).catch(function (contactError) {
        if (hasAccessTokenExpired(contactError.response)) {
            let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
            getTokenFromRefreshToken(refreshToken, function (refreshTokenError, accessToken, refreshToken) {
                if (!refreshTokenError) {
                    res.cookie(COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
                    res.cookie(COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);

                    config = {
                        headers: {
                            Authorization: 'Bearer ' + accessToken
                        }
                    };

                    get(url, config).then(function (response) {
                        callback(null, response.data);
                    }).catch(function (error) {
                        console.log(error);
                        callback(error);
                    });
                } else {
                    console.log(refreshTokenError);
                    callback({
                        statusCode: STATUS_CODES.INVALID_TOKEN
                    });
                }
            });
        } else {
            console.log(contactError);
            callback(contactError);
        }
    });
}

router.post('/sync-contractor-details', ensureAuthenticated, function (req, res) {
    let contactID = req.body.contactID;
    let taskID = req.body.taskID;
    let userID = req.body.userID;
    let resData = {};
    getContactDetails(
        contactID,
        req,
        res,
        function (error, contact) {
            if (!error) {
                if (contact) {
                    let extractedPersonalNotes = _extractPersonalNotes(contact.personalNotes ? contact.personalNotes : '');
                    let contractor = {
                        company: extractedPersonalNotes.company && extractedPersonalNotes.company.length > 0 ? extractedPersonalNotes.company : '',
                        billBranch: extractedPersonalNotes.billBranch && extractedPersonalNotes.billBranch.length > 0 ? extractedPersonalNotes.billBranch : '',
                        poRequired: extractedPersonalNotes.poRequired && extractedPersonalNotes.poRequired.toLowerCase() === 'yes' ? true : false,
                        includeContacts: extractedPersonalNotes.includeContacts && extractedPersonalNotes.includeContacts.toLowerCase() === 'yes' ? true : false,
                        invoiceContact: extractedPersonalNotes.invoiceContact && extractedPersonalNotes.invoiceContact ? extractedPersonalNotes.invoiceContact : '',
                        includeClientJobNo: extractedPersonalNotes.includeClientJobNo && extractedPersonalNotes.includeClientJobNo.toLowerCase() === 'yes' ? true : false,
                        contract: extractedPersonalNotes.contract && extractedPersonalNotes.contract ? extractedPersonalNotes.contract : '',
                        id: contact.id,
                        name: contact.companyName,
                        updatedBy: userID,
                        updatedAt: new Date()

                    };
                    let task = {
                        id: taskID,
                        contractor
                    };
                    Task.findOneAndUpdate({ id: taskID }, task, { new: true }, function (err, data) {
                        if (!err) {
                            resData = {
                                data,
                                status: 200
                            };
                            res.json(resData);
                        } else {
                            resData = {
                                data: err,
                                status: 404
                            };
                            console.log(err);
                            res.json(resData);
                        }
                    });
                } else {
                    resData = {
                        data: 'not synced',
                        status: 301
                    };
                    res.json(resData);
                }
            } else {
                console.log(error);
            }
        }
    );
});

router.post('/download', function (req, res) {
    let csaId = req.body.id;
    let taskId = req.body.taskId;

    Agreement.findOne({
        id: csaId
    }, function (err, csa) {
        if (!err) {
            if (csa) {
                Task.findOne({
                    id: taskId
                }).populate({
                    path: 'scopes',
                    populate: [{
                        path: 'managerDetails.manager',
                        select: 'firstName lastName _id id role picture employeeCode signature title initials titleBlock'
                    }]
                }).exec(function (taskError, task) {
                    if (!err) {
                        if (task) {
                            getContactDetails(task.contractor.id, req, res, function (err, contact) {
                                if (!err) {
                                    if (contact) {
                                        let totalFee = 0;
                                        let scopes = [];
                                        forEach(task.scopes, function (scope) {
                                            let isScopeSelected = find(csa.selectedScopes, function (selectedScopeForCSA) {
                                                return scope._id.equals(selectedScopeForCSA);
                                            });
                                            if (isScopeSelected) {
                                                totalFee += scope.price;
                                                scopes.push({ text: scope.definition + '\n\n' });
                                            }
                                        });

                                        let clientAddress = {
                                            street: '',
                                            city: '',
                                            state: '',
                                            countryOrRegion: '',
                                            postalCode: ''
                                        };

                                        if (!isEmpty(contact.businessAddress)) {
                                            clientAddress = contact.businessAddress;
                                        }

                                        let fonts = {
                                            Roboto: {
                                                normal: './public/fonts/Roboto-Regular.ttf',
                                                bold: './public/fonts/Roboto-Medium.ttf',
                                                italics: './public/fonts/Roboto-Italic.ttf',
                                                bolditalics: '../public/fonts/Roboto-MediumItalic.ttf'
                                            }
                                        };
                                        let printer = new PdfPrinter(fonts);

                                        let jobNo = task.isBidding ? 'B' + new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber : new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber;
                                        let date = moment().format('M/D/YY');
                                        let managerDetails = task.scopes[0] && task.scopes[0].managerDetails || null;
                                        let projectManager = managerDetails.manager;

                                        let managerName = projectManager.firstName + ' ' + projectManager.lastName;
                                        let managerTitle = projectManager.title;

                                        let titleBlockURL = './public/images/dhc-csa-header.JPG';
                                        let signatureURL = './public/images/signature.jpg';
                                        let initialsURL = './public/images/initial.jpg';
                                        let imagePromises = [];

                                        if (projectManager.signature) {
                                            let signaturePromise = new Promise(function (resolve) {
                                                request.get(projectManager.signature, function (signatureErr, signatureResponse, signatureBody) {
                                                    if (!signatureErr && signatureResponse.statusCode === 200) {
                                                        signatureURL = 'data:' + signatureResponse.headers['content-type'] + ';base64,' + new Buffer(signatureBody).toString('base64');
                                                        resolve();
                                                    } else {

                                                        console.log(signatureErr);
                                                        resolve();
                                                    }
                                                });
                                            });

                                            imagePromises.push(signaturePromise);
                                        }

                                        if (projectManager.titleBlock) {
                                            let titleBlockPromise = new Promise(function (resolve) {
                                                request.get(projectManager.titleBlock, function (titleBlockErr, titleBlockResponse, titleBlockBody) {
                                                    if (!titleBlockErr && titleBlockResponse.statusCode === 200) {
                                                        titleBlockURL = 'data:' + titleBlockResponse.headers['content-type'] + ';base64,' + new Buffer(titleBlockBody).toString('base64');
                                                        resolve();
                                                    } else {
                                                        console.log(titleBlockErr);
                                                        resolve();
                                                    }
                                                });
                                            });

                                            imagePromises.push(titleBlockPromise);
                                        }

                                        if (projectManager.initials) {
                                            let initialsPromise = new Promise(function (resolve) {
                                                request.get(projectManager.initials, function (initialsErr, initialsResponse, initialsBody) {
                                                    if (!initialsErr && initialsResponse.statusCode === 200) {
                                                        initialsURL = 'data:' + initialsResponse.headers['content-type'] + ';base64,' + new Buffer(initialsBody).toString('base64');
                                                        resolve();
                                                    } else {
                                                        console.log(initialsErr);
                                                        resolve();
                                                    }
                                                });
                                            });

                                            imagePromises.push(initialsPromise);
                                        }

                                        Promise.all(imagePromises).then(function () {
                                            let checkOptionsAndRender = [];
                                            if (csa.hasShoring) {
                                                checkOptionsAndRender.push({ text: csa.shoringTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({
                                                    text: csa.shoring + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify'
                                                });
                                            }

                                            if (csa.hasEquipmentSupport) {
                                                checkOptionsAndRender.push({ text: csa.equipmentSupportTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.equipmentSupport + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasStructuralShoring) {
                                                checkOptionsAndRender.push({ text: csa.structuralShoringTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.structuralShoring + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }


                                            checkOptionsAndRender.push({ text: '\n        All of Consultant’s services in any way related to the Project shall be subject to the terms of this Agreement. Consultant’s services shall be limited to those expressly set forth above, and Consultant shall have no other obligations or responsibilities for the Project except as agreed to in writing or as provided in this Agreement. Consultant’s services are intended for the Client’s sole use  and benefit and solely for the Client’s use on the Project and shall not create any third party rights. Except as agreed to in writing, Consultant’s services and work product shall not be used or relied on by any other person or entity, or for any purpose following substantial completion of the Project.\n\n', margin: [0, 15, 0, 15], alignment: 'justify' });

                                            if (csa.hasFee) {
                                                checkOptionsAndRender.push({ text: csa.feeTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: 'Consultant shall prepare the Scope of Services ' });
                                                if (csa.isCostIncluded) {
                                                    checkOptionsAndRender.push({ text: 'for a total fee of ' });
                                                    checkOptionsAndRender.push({ text: '$' + totalFee, decoration: 'underline' });
                                                } else {
                                                    checkOptionsAndRender.push({ text: 'on a ' });
                                                    checkOptionsAndRender.push({ text: 'Time & Materials', decoration: 'underline' });
                                                    checkOptionsAndRender.push({ text: ' basis' });
                                                }
                                                checkOptionsAndRender.push({ text: '. ' });
                                                checkOptionsAndRender.push({ text: csa.fee + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasIndeminity) {
                                                checkOptionsAndRender.push({ text: csa.indeminityTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.indeminity + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasMeans) {
                                                checkOptionsAndRender.push({ text: csa.meansTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.means + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasTermination) {
                                                checkOptionsAndRender.push({ text: csa.terminationTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.termination + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasCare) {
                                                checkOptionsAndRender.push({ text: csa.careTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.care + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasSchedule) {
                                                checkOptionsAndRender.push({ text: csa.scheduleTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.schedule + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasDispute) {
                                                checkOptionsAndRender.push({ text: csa.disputeTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.dispute + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasSafety) {
                                                checkOptionsAndRender.push({ text: csa.safetyTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.safety + '\n\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            if (csa.hasMiscellaneous) {
                                                checkOptionsAndRender.push({ text: csa.miscellaneousTitle + '\n', margin: [0, 0, 0, 5], decoration: 'underline', bold: true });
                                                checkOptionsAndRender.push({ text: csa.miscellaneous + '\n', margin: [0, 0, 0, 8], alignment: 'justify' });
                                            }

                                            let clientName = contact.displayName ? contact.displayName : '';
                                            let clientTitle = contact.jobTitle ? contact.jobTitle : '';
                                            let clientCompany;

                                            if (task.contractor.company) {
                                                clientCompany = task.contractor.company.trim();
                                            } else {
                                                clientCompany = contact.companyName ? contact.companyName.replace(/\S*\[\d+]+\S*/g, '') : '';
                                            }
                                            let requestDocObject = {
                                                clientAddress: clientAddress,
                                                clientCompany: clientCompany,
                                                task: task,
                                                scopes: scopes,
                                                checkOptionsAndRender: checkOptionsAndRender,
                                                jobNo: jobNo,
                                                titleBlockURL: titleBlockURL,
                                                signatureURL: signatureURL,
                                                initialsURL: initialsURL,
                                                date: date,
                                                managerTitle: managerTitle,
                                                managerName: managerName
                                            };

                                            let docDefinition = getPDFDocDefinitionObject(requestDocObject);
                                            let pdfDoc = printer.createPdfKitDocument(docDefinition);
                                            // console.log('agreement pdf', pdfDoc);

                                            res.attachment('CSA-' + jobNo + '-' + csa.number + '.pdf');
                                            pdfDoc.pipe(res);
                                            pdfDoc.end();
                                        }).catch(function (rejectionError) {
                                            console.log(rejectionError);
                                        });
                                    } else {
                                        console.log('Contact not found');
                                        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
                                    }
                                } else {
                                    console.log(err);
                                    if (err.statusCode) {
                                        res.status(STATUS_CODES.INVALID_TOKEN).send(RESPONSE_MESSAGES.INVALID_TOKEN);
                                    } else {
                                        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
                                    }
                                }
                            });
                        } else {
                            console.log('Task with id ' + taskId + ' not found.');
                            res.status(STATUS_CODES.NOT_FOUND).send(RESPONSE_MESSAGES.TASK.NOT_FOUND);
                        }
                    } else {
                        console.log(taskError);
                        res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log('CSA with id ' + csaId + ' not found.');
                res.status(STATUS_CODES.NOT_FOUND).send(RESPONSE_MESSAGES.AGREEMENT.NOT_FOUND);
            }
        } else {
            console.log(err);
            res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).send(RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const agreement = await Agreement.findOne({ id }).populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(agreement);
    } catch (e) {
        console.log('Error in get agreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { agreement, taskId } = req.body;
    console.log(agreement)
    try {
        const result = await Agreement
            .findOneAndUpdate({ id }, agreement, { new: true, select: generateSelection(agreement) })
            .populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_AGREEMENT, { taskId, agreement: result });
    } catch (e) {
        console.log('Error in put agreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, agreement } = req.body;
        const result = await Agreement
            .findOneAndUpdate({ id }, agreement, { new: true, select: generateSelection(agreement) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_AGREEMENT, { taskId, agreement: result });
    } catch (e) {
        console.log('Error in put agreement/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', ensureAuthenticated, async (req, res) => {
    const { taskId, agreement } = req.body;
    try {
        const result = await createAgreement(taskId, agreement, true).catch(e => { throw e });
        const data = await Agreement.findById(result._id).populate(selectedScopePopulation).exec().catch(e => { throw e });
        const task = await addTaskMilestone('agreements', taskId, data).catch(e => { throw e });
        res.json(task);
        emitScoket(req, socketType.ADD_AGREEMENT, task);
    } catch (e) {
        console.log('Error in post agreement/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        await Agreement.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_AGREEMENT, { id });
    } catch (e) {
        console.log('Error in delete agreement/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;

