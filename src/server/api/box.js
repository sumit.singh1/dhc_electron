var fs = require('fs');
var request = require('request');
var jwt = require('jsonwebtoken');
var uuid = require('uuid');
var axios = require('axios');
var querystring = require('querystring');
var formidable = require('formidable');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//others
var common = require('./common');

function getToken(userType, boxId, callback) {
    var claims = {
        box_sub_type: userType,
        iss: process.env.BOX_CLIENT_ID,
        sub: boxId,
        aud: "https://api.box.com/oauth2/token",
        jti: uuid.v4(),
        exp: Math.floor((new Date().getTime() + new Date().getTimezoneOffset()) / 1000) + 60
    };
    var options = {
        algorithm: 'RS256',
        header: {
            kid: process.env.BOX_PUBLIC_KEY_ID
        }
    };
    var keyParams = {
        key: process.env.BOX_PRIVATE_KEY,
        passphrase: process.env.BOX_PRIVATE_KEY_PASS
    };

    var assertion = jwt.sign(claims, keyParams, options);

    var data = querystring.stringify({
        grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
        client_id: process.env.BOX_CLIENT_ID,
        client_secret: process.env.BOX_CLIENT_SECRET,
        assertion: assertion
    });

    var url = 'https://api.box.com/oauth2/token';

    axios.post(url, data).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function createUser(name, accessToken, callback) {
    var data = {
        name: name,
        is_platform_access_only: true
    };

    var url = 'https://api.box.com/2.0/users';

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.post(url, data, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function createSharedLink(fileId, accessToken, callback) {
    var data = {
        shared_link: {
            access: 'open',
            can_download: true,
            can_preview: true
        }
    };

    var url = 'https://api.box.com/2.0/files/' + fileId;

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.put(url, data, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        callback(err);
    });
}

function downloadFile(fileId, accessToken, callback) {
    var url = 'https://api.box.com/2.0/files/' + fileId + '/content';

    request.get({
        url: url,
        headers: {
            Authorization: 'Bearer ' + accessToken
        },
        followRedirect: false
    }, function(err, response, body) {
        if (!err) {
            if (response.statusCode.toString().charAt(0) === '2' || response.statusCode.toString().charAt(0) === '3') {
                callback(null, response.headers.location);
            } else {
                console.log(response.statusCode, body);
                callback(body);
            }
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function uploadFile(data, accessToken, callback) {
    var url = 'https://upload.box.com/api/2.0/files/content';
    var headers = {
        Authorization: 'Bearer ' + accessToken
    };

    request.post({
        url: url,
        headers: headers,
        formData: data
    }, function(err, response, body) {
        if (!err) {
            if (response.statusCode.toString().charAt(0) === '2') {
                callback(null, JSON.parse(body));
            } else {
                console.log(response.statusCode, body);
                callback(err);
            }
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function createFolder(name, parentId, accessToken, callback) {
    var data = {
        name: name,
        parent: {
            id: parentId
        }
    };

    var url = 'https://api.box.com/2.0/folders';

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.post(url, data, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function getFiles(folderId, accessToken, callback) {
    var url = 'https://api.box.com/2.0/folders/' + folderId + '/items?fields=shared_link,name';

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.get(url, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function createGroup(groupName, accessToken, callback) {
    var data = {
        name: groupName,
        invitability_level: 'admins_and_members',
        member_viewability_level: 'admins_and_members'
    };

    var url = 'https://api.box.com/2.0/groups';

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.post(url, data, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function createCollaboration(folderId, groupId, accessToken, callback) {
    var data = {
        item: {
            id: folderId,
            type: 'folder'
        },
        accessible_by: {
            id: groupId,
            type: 'group'
        },
        role: 'editor'
    };

    var url = 'https://api.box.com/2.0/collaborations';

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.post(url, data, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function createMembership(userId, groupId, accessToken, callback) {
    var data = {
        user: {
            id: userId
        },
        group: {
            id: groupId
        }
    };

    var url = 'https://api.box.com/2.0/group_memberships';

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.post(url, data, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}

function deleteMembership(membershipId, accessToken, callback) {
    var url = 'https://api.box.com/2.0/group_memberships/' + membershipId;

    var config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.delete(url, config).then(function(response) {
        callback(null, response.data);
    }).catch(function(err) {
        console.log(err);
        callback(err);
    });
}


router.post('/getToken', function(req, res) {
    var userType = req.body.userType;
    var boxId = req.body.boxId;

    getToken(userType, boxId, function(err, tokenData) {
        if (!err) {
            res.json(tokenData);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    })
});

router.post('/uploadProfilePicture', common.ensureAuthenticated, function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if (!err) {
            var parentId = fields.parentId;
            var fileName = fields.fileName;
            var file = files.file;

            var userType = 'enterprise';
            var boxUserId = process.env.BOX_ENTERPRISE_ID;

            getToken(userType, boxUserId, function(err, token) {
                if (!err) {
                    var data = {
                        attributes: JSON.stringify({
                            name: fileName,
                            parent: {
                                id: parentId
                            }
                        }),
                        file: fs.createReadStream(file.path)
                    };

                    uploadFile(data, token.access_token, function(err, file) {
                        if (!err) {
                            res.json(file);
                        } else {
                            console.log(err);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });

                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/uploadFile', common.ensureAuthenticated, function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        if (!err) {
            var user = JSON.parse(fields.user);
            var parentId = fields.parentId;
            var fileName = fields.fileName;
            var file = files.file;

            var userType = 'enterprise';
            var boxUserId = process.env.BOX_ENTERPRISE_ID;
            if (user.boxUserId) {
                userType = 'user';
                boxUserId = user.boxUserId;
            }

            getToken(userType, boxUserId, function(err, token) {
                if (!err) {
                    var data = {
                        attributes: JSON.stringify({
                            name: fileName,
                            parent: {
                                id: parentId
                            }
                        }),
                        file: fs.createReadStream(file.path)
                    };

                    uploadFile(data, token.access_token, function(err, file) {
                        if (!err) {
                            res.json(file);
                        } else {
                            console.log(err);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });

                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/createSharedLink', common.ensureAuthenticated, function(req, res) {
    var fileId = req.body.fileId;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            createSharedLink(fileId, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/downloadFile', common.ensureAuthenticated, function(req, res) {
    var fileId = req.body.fileId;
    var user = req.body.user;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;
    if (user.boxUserId) {
        userType = 'user';
        boxUserId = user.boxUserId;
    }

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            downloadFile(fileId, token.access_token, function(err, fileUrl) {
                if (!err) {
                    res.json(fileUrl);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});


router.post('/createUser', common.ensureAuthenticated, function(req, res) {
    var name = req.body.name;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            createUser(name, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/createFolder', common.ensureAuthenticated, function(req, res) {
    var name = req.body.name;
    var parentId = req.body.parentId;
    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            createFolder(name, parentId, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/getFiles', common.ensureAuthenticated, function(req, res) {
    var user = req.body.user;
    var folderId = req.body.folderId;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;
    if (user.boxFolderId) {
        userType = 'user';
        boxUserId = user.boxUserId;
    }


    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            getFiles(folderId, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/createGroup', common.ensureAuthenticated, function(req, res) {
    var groupName = req.body.groupName;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            createGroup(groupName, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/createCollaboration', common.ensureAuthenticated, function(req, res) {
    var groupId = req.body.groupId;
    var folderId = req.body.folderId;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            createCollaboration(folderId, groupId, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/createMembership', common.ensureAuthenticated, function(req, res) {
    var groupId = req.body.groupId;
    var userId = req.body.userId;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            createMembership(userId, groupId, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.delete('/deleteMembership/:membershipId', common.ensureAuthenticated, function(req, res) {
    var membershipId = req.params.membershipId;

    var userType = 'enterprise';
    var boxUserId = process.env.BOX_ENTERPRISE_ID;

    getToken(userType, boxUserId, function(err, token) {
        if (!err) {
            deleteMembership(membershipId, token.access_token, function(err, data) {
                if (!err) {
                    res.json(data);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            });
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

module.exports = router;
