let OAuth = require('oauth');
let uuid = require('uuid');
let axios = require('axios');
let express = require('express');
let router = express.Router();


// constants
let constants = require('../utils/constants');

// others
let common = require('./common');

import User from '../models/User';

let authority = 'https://login.microsoftonline.com/common';
let authorizeEndpoint = '/oauth2/v2.0/authorize';
let tokenEndpoint = '/oauth2/v2.0/token';
let scope = 'Calendars.ReadWrite.Shared Contacts.ReadWrite.Shared offline_access';

function getTokenFromCode(code, callback) {
    let OAuth2 = OAuth.OAuth2;
    let oauth2 = new OAuth2(
        process.env.OFFICE_CLIENT_ID,
        process.env.OFFICE_CLIENT_SECRET,
        authority,
        authorizeEndpoint,
        tokenEndpoint
    );

    oauth2.getOAuthAccessToken(
        code, {
            grant_type: 'authorization_code',
            redirect_uri: process.env.APP_URL,
            response_mode: 'form_post',
            nonce: uuid.v4()
        },
        function (err, accessToken, refreshToken) {
            callback(err, accessToken, refreshToken);
        }
    );
}

function getTokenFromRefreshToken(refreshToken, callback) {
    let OAuth2 = OAuth.OAuth2;
    let oauth2 = new OAuth2(
        process.env.OFFICE_CLIENT_ID,
        process.env.OFFICE_CLIENT_SECRET,
        authority,
        authorizeEndpoint,
        tokenEndpoint
    );

    oauth2.getOAuthAccessToken(
        refreshToken, {
            grant_type: 'refresh_token',
            redirect_uri: process.env.APP_URL,
            response_mode: 'form_post',
            nonce: uuid.v4()
        },
        function (err, accessToken, refreshToken) {
            callback(err, accessToken, refreshToken);
        }
    );
}

function hasAccessTokenExpired(e) {
    let expired = false;
    if (e.status === 401 && e.statusText === 'Unauthorized') {
        expired = true;
    }
    return expired;
}

function getAuthUrl() {
    return authority + authorizeEndpoint +
        '?client_id=' + process.env.OFFICE_CLIENT_ID +
        '&response_type=code' +
        '&scope=' + scope +
        '&redirect_uri=' + process.env.APP_URL +
        '&response_mode=query' +
        '&nonce=' + uuid.v4();
}

function getContacts(accessToken, query, req, res, callback) {
    let url = 'https://graph.microsoft.com/v1.0/me/contacts?$filter=startswith(companyName, \'' + query + '\')&$top=10000&$count=true';

    let config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };
    axios.get(url, config).then(function (response) {
        callback(null, response.data.value);
    }).catch(function (err, a) {
        if (hasAccessTokenExpired(err.response)) {
            let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
            getTokenFromRefreshToken(refreshToken, function (err, accessToken, refreshToken) {
                if (!err) {
                    res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
                    res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);

                    config = {
                        headers: {
                            Authorization: 'Bearer ' + accessToken
                        }
                    };

                    axios.get(url, config).then(function (response) {
                        callback(null, response.data.value);
                    }).catch(function (err) {
                        console.error(err);
                        callback(err);
                    });
                } else {
                    console.error(err);
                    callback({
                        statusCode: constants.STATUS_CODES.INVALID_TOKEN
                    });
                }
            });
        } else {
            console.error(err);
            callback(err);
        }
    });
}

function getContractors(accessToken, query, req, res, callback) {
    let url = 'https://graph.microsoft.com/v1.0/me/contacts?$filter=startswith(companyName, \'' + query + '\')&$top=10000&$count=true';

    let config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };
    axios.get(url, config).then(function (response) {
        callback(null, response.data.value);
    }).catch(function (err, a) {
        if (hasAccessTokenExpired(err.response)) {
            let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
            getTokenFromRefreshToken(refreshToken, function (err, accessToken, refreshToken) {
                if (!err) {
                    res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
                    res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);

                    config = {
                        headers: {
                            Authorization: 'Bearer ' + accessToken
                        }
                    };

                    axios.get(url, config).then(function (response) {
                        callback(null, response.data.value);
                    }).catch(function (err) {
                        console.log(err);
                        callback(err);
                    });
                } else {
                    console.log(err);
                    callback({
                        statusCode: constants.STATUS_CODES.INVALID_TOKEN
                    });
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

function getEvents({ accessToken, req, res }, callback) {
    let { startDate, endDate } = req.params;
    let url = 'https://graph.microsoft.com/v1.0/me/calendarView?$top=10000&startDateTime=' + startDate + '&endDateTime=' + endDate;

    let config = {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    };

    axios.get(url, config).then(function (response) {
        callback(null, response.data.value);
    }).catch(function (err) {
        if (hasAccessTokenExpired(err.response)) {
            let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
            getTokenFromRefreshToken(refreshToken, function (error, newAccessToken, newRefreshToken) {
                if (!error) {
                    res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, newAccessToken);
                    res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, newRefreshToken);
                    config = {
                        headers: {
                            Authorization: 'Bearer ' + newAccessToken
                        }
                    };

                    axios.get(url, config).then(function (response) {
                        callback(null, response.data.value);
                    }).catch(function (e) {
                        console.log(e);
                        callback(e);
                    });
                } else {
                    console.log(error);
                    callback({
                        statusCode: constants.STATUS_CODES.INVALID_TOKEN
                    });
                }
            });
        } else {
            console.log(err);
            callback(err);
        }
    });
}

router.get('/authUrl', common.ensureAuthenticated, function (req, res) {
    res.json(getAuthUrl());
});

router.get('/login/:code', common.ensureAuthenticated, function (req, res) {
    if (typeof req.params.code !== 'undefined') {
        getTokenFromCode(req.params.code, function (err, accessToken, refreshToken) {
            if (!err) {
                res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
                res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);
                User.findOneAndUpdate({firstName: 'Jasper'}, {$set:{refreshToken}}, {new: true}, () => {
                    res.json();
                });
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('Invalid Office Login');
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/contacts/:query', common.ensureAuthenticated, function (req, res) {
    let query = req.params.query;
    query = query.replace('\'', '\'\'');
    query = encodeURIComponent(query);
    let accessToken = req.cookies.OFFICE_ACCESS_TOKEN;
    if (accessToken) {
        getContacts(accessToken, query, req, res, function (err, contacts) {
            if (!err) {
                res.json(contacts);
            } else if (err.statusCode) {
                res.status(constants.STATUS_CODES.INVALID_TOKEN).send(constants.RESPONSE_MESSAGES.INVALID_TOKEN);
            } else {
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('No access token');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.OFFICE_365.TOKEN_NOT_FOUND);
    }
});

router.get('/contractors/:query', common.ensureAuthenticated, function (req, res) {
    let accessToken = req.cookies.OFFICE_ACCESS_TOKEN;
    let query = req.params.query;
    query = query.replace('\'', '\'\'');
    query = encodeURIComponent(query);
    if (accessToken) {
        getContractors(accessToken, query, req, res, function (err, contacts) {
            if (!err) {
                res.json(contacts);
            } else if (err.statusCode) {
                res.status(constants.STATUS_CODES.INVALID_TOKEN).send(constants.RESPONSE_MESSAGES.INVALID_TOKEN);
            } else {
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('No access token');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.OFFICE_365.TOKEN_NOT_FOUND);
    }
});

router.get('/events/:startDate/:endDate', common.ensureAuthenticated, function (req, res) {
    let accessToken = req.cookies.OFFICE_ACCESS_TOKEN;
    if (accessToken) {
        let payload = {
            accessToken,
            req,
            res
        };
        getEvents(payload, function (err, events) {
            if (!err) {
                res.json(events);
            } else if (err.statusCode) {
                res.status(constants.STATUS_CODES.INVALID_TOKEN).send(constants.RESPONSE_MESSAGES.INVALID_TOKEN);
            } else {
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('No access token');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.OFFICE_365.TOKEN_NOT_FOUND);
    }
});

// get access token for customer user using refresh token
router.get('/access-token-for-customer', async (req, res) => {
    // console.log('^^^^^^^', req.cookies)
    // let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN || process.env.REFRESH_TOKEN;
    try {
        const {refreshToken} = await User.findOne({ firstName: 'Jasper' }).select('refreshToken').exec().catch(e => { throw e; });
        getTokenFromRefreshToken(refreshToken, function (err, accessToken, newRefreshToken) {
            if (err) {
                res.status(constants.STATUS_CODES.INVALID_TOKEN).json({});
                console.log('Error in access-token-for-customer API : ', err);
                return;
            }
            res.cookie(constants.COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
            res.cookie(constants.COOKIES.OFFICE_REFRESH_TOKEN, newRefreshToken);
            res.status(200).json('Success');
        });
    } catch (error) {
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.OFFICE_365.TOKEN_NOT_FOUND);
    }
});

module.exports = router;
