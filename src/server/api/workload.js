var _ = require('lodash');
var express = require('express');
var router = express.Router();

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//models
var Workload = require('../models/Workload');

//common
var common = require('./common');

var error = constants.RESPONSES;

function getWorkload(query, population, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    Workload.findOne(query)
        .populate(population)
        .exec(function (err, workload) {
            if (!err) {
                callback(null, workload);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

function getWorkloads(query, population, sort, limit, callback) {
    if (_.isEmpty(query)) {
        query = {

        };
    }
    if (_.isEmpty(population)) {
        population = '';
    }
    if (_.isEmpty(sort)) {
        sort = '';
    }

    Workload.find(query)
        .populate(population)
        .sort(sort)
        .limit(limit)
        .lean()
        .exec(function (err, workloads) {
            if (!err) {
                callback(null, workloads);
            } else {
                console.log(err);
                callback(err);
            }
        });
}

router.post('/', common.ensureAuthenticated, function (req, res) {
    var payload = req.body;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_PERSONAL_UPDATE.NAME, constants.ACCESS_LEVELS.WRITE, function () {
        var workload = new Workload({
            name: payload.name,
            color: payload.color
        });

        workload.save(function (err, savedWorkload) {
            if (!err) {
                res.json(savedWorkload);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.put('/:workloadId', common.ensureAuthenticated, function (req, res) {
    var workloadId = req.params.workloadId;
    var params = req.body;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_PERSONAL_UPDATE.NAME, constants.ACCESS_LEVELS.WRITE, function () {
        Workload.findOneAndUpdate({
            id: workloadId
        }, params, {
                new: true
            }, function (err, updatedWorkload) {
                if (!err) {
                    res.json(updatedWorkload);
                } else {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                }
            })
    });
});

router.get('/search/:query', common.ensureAuthenticated, function (req, res) {
    var query = {
        name: {
            '$regex': '.*' + req.params.query + '.*',
            '$options': 'i'
        }
    };

    getWorkloads(query, null, null, null, function (err, workloads) {
        if (!err) {
            res.json(workloads);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function (req, res) {

    getWorkloads(null, null, null, null, function (err, workloads) {
        if (!err) {
            res.json(workloads);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

module.exports = router;
