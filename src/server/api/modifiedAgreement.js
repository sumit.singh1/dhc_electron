import express from 'express';

import ModifiedAgreement from '../models/ModifiedAgreement';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { addTaskMilestone, createModifiedAgreement } from '../utils/milestonesCreate';
import { selectedScopePopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common, { ensureAuthenticated } from './common';

const router = express.Router();


router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const modifiedAgreement = await ModifiedAgreement
            .findOne({ id }).populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(modifiedAgreement);
    } catch (e) {
        console.log('Error in get modifiedAgreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { modifiedAgreement, taskId } = req.body;
    try {
        const result = await ModifiedAgreement
            .findOneAndUpdate({ id }, modifiedAgreement, { new: true, select: generateSelection(modifiedAgreement) })
            .populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_MODIFIED_AGREEMENT, { taskId, modifiedAgreement: result });
    } catch (e) {
        console.log('Error in put modifiedAgreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, modifiedAgreement } = req.body;
        const result = await ModifiedAgreement
            .findOneAndUpdate({ id }, modifiedAgreement, { new: true, select: generateSelection(modifiedAgreement) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_MODIFIED_AGREEMENT, { taskId, modifiedAgreement: result });
    } catch (e) {
        console.log('Error in put modifiedAgreement/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', ensureAuthenticated, async (req, res) => {
    const { taskId, modifiedAgreement } = req.body;
    try {
        const result = await createModifiedAgreement(taskId, modifiedAgreement).catch(e => { throw e });
        const data = await ModifiedAgreement.findById(result._id).populate(selectedScopePopulation).exec().catch(e => { throw e });
        const task = await addTaskMilestone('modifiedAgreements', taskId, data).catch(e => { throw e });
        res.json(task);
        emitScoket(req, socketType.ADD_MODIFIED_AGREEMENT, task);
    } catch (e) {
        console.log('Error in post modifiedAgreement/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        await ModifiedAgreement.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_MODIFIED_AGREEMENT, { id });
    } catch (e) {
        console.log('Error in delete modifiedAgreement/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
