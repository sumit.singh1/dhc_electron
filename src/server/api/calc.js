import express from 'express';

import Calc from '../models/Calc';

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { generateSelection } from '../utils/common';
import { createCalc } from '../utils/milestonesCreate';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const calc = await createCalc();
        res.json(calc);
        emitScoket(req, socketType.ADD_CALC, calc);
    } catch (e) {
        console.log('Error in calc/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, calc } = req.body;
        const result = await Calc
            .findOneAndUpdate({ id }, calc, { new: true, select: generateSelection(calc) })
            .exec().catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_CALC, { taskId, calc: result });
    } catch (e) {
        console.log('Error in calc/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, calc } = req.body;
        const result = await Calc
            .findOneAndUpdate({ id }, calc, { new: true, select: generateSelection(calc) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_CALC, { taskId, calc: result });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const calc = await Calc.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_CALC, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
