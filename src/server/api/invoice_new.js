import express from 'express';
import async from 'async';
import _ from 'lodash';
import mongoose from 'mongoose';

import Invoice from '../models/Invoice';
import Scope from '../models/Scope';

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { generateSelection } from '../utils/common';
import { createInvoice, addTaskMilestone } from '../utils/milestonesCreate';
import { invoiceTablePopulation, invoiceSelectedScopePopulation, selectedScopePopulation } from '../utils/populations'

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    const { taskId, invoice } = req.body;
    try {
        const result = await createInvoice(taskId, invoice).catch(e => { throw e });
        const data = await Invoice.findById(result._id).populate(invoiceTablePopulation).exec().catch(e => { throw e });
        const task = await addTaskMilestone('invoices', taskId, data).catch(e => { throw e });
        res.json(task);
        emitScoket(req, socketType.ADD_INVOICE, task);
    } catch (e) {
        console.log('Error in invoice/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const invoice = await Invoice.findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) }).exec().catch(e => { throw e });
        res.json(invoice);
        emitScoket(req, socketType.ARCHIVE_INVOICE, invoice);
    } catch (e) {
        console.log('Error in invoice/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        if (body.selectedScopes) {
            const promises = [];
            body.selectedScopes.forEach(selectedScope => {
                if (!selectedScope.isPartial && selectedScope.scope.price !== selectedScope.oldPrice) {
                    promises.push(Scope.findOneAndUpdate({ _id: selectedScope.scope._id }, { price: selectedScope.oldPrice }, { new: true }));
                }
            })
            await Promise.all(promises).catch(e => { throw e });
        }
        const invoice = await Invoice.findOneAndUpdate({ id }, body, { new: true }).populate(invoiceTablePopulation).exec().catch(e => { throw e });
        res.json(invoice);
        emitScoket(req, socketType.UPDATE_INVOICE, invoice);
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const invoice = await Invoice.findOneAndRemove({ id }).exec();
        res.json({ id });
        emitScoket(req, socketType.DELETE_INVOICE, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/', async (req, res) => {
    const sent = req.query.sent;
    const paid = req.query.paid;
    const limit = req.query.limit;
    const skip = req.query.skip;

    const invoiceQuery = {
        isVisibleOnAdminPage: true,
        isArchived: false
    };

    if (sent) {
        invoiceQuery.sent = sent;
    }

    if (paid) {
        invoiceQuery.paid = paid;
    }

    try {
        const result = await Invoice
            .find(invoiceQuery)
            .populate(invoiceTablePopulation)
            .limit(Number(limit))
            .skip(Number(skip))
            .lean()
            .exec()
            .catch(e => { throw e });
        res.json(result);

    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message);
    }
});

router.get('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const invoice = await Invoice.findOne({ id }).exec().catch(e => { throw e });
        res.json(invoice);
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/getHoldInvoices', common.ensureAuthenticated, function (req, res) {
    var userID = req.body.userID;
    var invoicePopulation = [{
        path: 'selectedScopes.scope',
        populate: [{
            path: 'task',
            populate: [{
                path: 'agreements',
                match: {
                    isArchived: false
                },
                select: '_id id templates isArchived number'
            },
            {
                path: 'clientAgreements',
                match: {
                    isArchived: false
                },
                select: '_id id templates isArchived number'
            },
            {
                path: 'masterAgreements',
                match: {
                    isArchived: false
                },
                select: '_id id templates isArchived number'
            },
            {
                path: 'modifiedAgreements',
                match: {
                    isArchived: false
                },
                select: '_id id templates isArchived number'
            },
            {
                path: 'purchaseOrders',
                match: {
                    isArchived: false
                },
                select: '_id id templates isArchived isUpdated name number'
            },
            {
                path: 'invoices',
                match: {
                    isArchived: false
                },
                select: '_id id templates isArchived paid hold poNumber isUpdated'
            },
            {
                path: 'teamMembers',
                select: 'firstName lastName _id id role picture employeeCode email'
            }
            ]
        },
        {
            path: 'engineerDetails.engineer',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }, {
            path: 'drafterDetails.drafter',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }, {
            path: 'managerDetails.manager',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }, {
            path: 'milestone.templates.metaData'
        }, {
            path: 'itemType'
        }, {
            path: 'group'
        }, {
            path: 'hourTrackers',
            populate: [{
                path: 'employee',
                populate: [{
                    path: 'role'
                }],
                select: 'firstName lastName _id id role picture employeeCode email'
            }]
        }, {
            path: 'drawings',
            match: {
                isArchived: false
            }
        }, {
            path: 'custDrawings',
            match: {
                isArchived: false
            }
        }, {
            path: 'calcs',
            match: {
                isArchived: false
            }
        }, {
            path: 'letters',
            match: {
                isArchived: false
            }
        }, {
            path: 'tabData',
            match: {
                isArchived: false
            }
        }
        ]
    }];

    var scopePopulation = [{
        path: 'task',
        populate: [{
            path: 'agreements',
            match: {
                isArchived: false
            },
            select: '_id id templates isArchived number'
        },
        {
            path: 'clientAgreements',
            match: {
                isArchived: false
            },
            select: '_id id templates isArchived number'
        },
        {
            path: 'masterAgreements',
            match: {
                isArchived: false
            },
            select: '_id id templates isArchived number'
        },
        {
            path: 'modifiedAgreements',
            match: {
                isArchived: false
            },
            select: '_id id templates isArchived number'
        },
        {
            path: 'purchaseOrders',
            match: {
                isArchived: false
            },
            select: '_id id templates isArchived isUpdated name number'
        },
        {
            path: 'invoices',
            match: {
                isArchived: false
            },
            select: '_id id templates isArchived paid hold poNumber isUpdated selectedScopes '
        },
        {
            path: 'teamMembers',
            select: 'firstName lastName _id id role picture employeeCode'
        }
        ]
    },
    {

        path: 'hourTrackers',
        populate: [{
            path: 'employee',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }]

    },
    {
        path: 'calcs',
        match: {
            isArchived: false
        }
    },
    {
        path: 'engineerDetails.engineer',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode email'
    }, {
        path: 'drafterDetails.drafter',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode email'
    }, {
        path: 'managerDetails.manager',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode email'
    }, {
        path: 'milestone.templates.metaData'
    }, {
        path: 'itemType'
    }, {
        path: 'group'
    }, {
        path: 'hourTrackers',
        populate: [{
            path: 'employee',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode'
        }]
    }, {
        path: 'drawings',
        match: {
            isArchived: false
        }
    }, {
        path: 'custDrawings',
        match: {
            isArchived: false
        }
    }, {
        path: 'letters',
        match: {
            isArchived: false
        }
    }, {
        path: 'tabData',
        match: {
            isArchived: false
        }
    }, {
        path: 'selectedInInvoices.selectedScopes.scope',
        select: 'price'
    }

    ];
    var holdInvoiceQuery = {
        $and: [{
            hold: 'Y'
        },
        {
            isArchived: false
        }
        ]
    };

    var newQuery = [{
        $match: {
            group: constants.TASK_GROUP__ID.COMPLETED_PROJECTS,
            isArchived: false,
            $or: [{
                'engineerDetails.engineer': userID
            },
            {
                'managerDetails.manager': userID
            },
            {
                'drafterDetails.drafter': userID
            }
            ]
        }
    },
    {
        $lookup: {
            'from': 'invoices',
            'localField': '_id',
            'foreignField': 'selectedScopes.scope',
            'as': 'selectedInInvoices'
        }
    },
    {
        $project: {
            price: 1,
            engineerDetails: 1,
            drafterDetails: 1,
            managerDetails: 1,
            'selectedInInvoices.selectedScopes': 1,
            'selectedInInvoices._id': 1
        }
    }
    ];


    async.parallel({
        holdInvoiceScopes: function (callBack) {
            Invoice.find(holdInvoiceQuery).populate(invoicePopulation).lean().exec(function (invoiceError, invoices) {
                if (!invoiceError) {
                    var scopes = [];
                    if (invoices.length > 0) {
                        invoices.forEach(function (invoice) {
                            invoice.selectedScopes.forEach(function (selectedScope) {
                                if (!selectedScope.scope.isArchived) {
                                    if (String(selectedScope.scope.engineerDetails.engineer._id) === String(userID) ||
                                        String(selectedScope.scope.managerDetails.manager._id) === String(userID) ||
                                        (selectedScope.scope.engineerDetails.drafter &&
                                            String(selectedScope.scope.engineerDetails.drafter._id) === String(userID))
                                    ) {
                                        scopes.push(selectedScope.scope);
                                    }
                                }
                            });
                        });
                    }
                    scopes = _.uniq(scopes);
                    // var groupedScopes = _.groupBy(scopes, function (currentScope) {
                    //     return currentScope.group._id;
                    // });
                    callBack(null, scopes);
                } else {
                    callBack(invoiceError);
                }
            });
        },
        completedUnInvoiced: function (callBack) {
            Scope.aggregate(
                [{
                    $match: {
                        isArchived: false,
                        group: {
                            $eq: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS)
                        },
                        $or: [{
                            'engineerDetails.engineer': {
                                $eq: mongoose.Types.ObjectId(userID)
                            }
                        },
                        {
                            'drafterDetails.drafter': {
                                $eq: mongoose.Types.ObjectId(userID)
                            }
                        },
                        {
                            'managerDetails.manager': {
                                $eq: mongoose.Types.ObjectId(userID)
                            }
                        }
                        ]
                    }
                },
                {
                    $lookup: {
                        'from': 'invoices',
                        'localField': '_id',
                        'foreignField': 'selectedScopes.scope',
                        'as': 'selectedInInvoices'
                    }
                },
                {
                    $project: {
                        updatedAt: 1,
                        createdAt: 1,
                        id: 1,
                        note: 1,
                        dueDate: 1,
                        price: 1,
                        group: 1,
                        status: 1,
                        itemType: 1,
                        number: 1,
                        task: 1,
                        parent: 1,
                        isArchived: 1,
                        hourTrackers: 1,
                        calcs: 1,
                        drawings: 1,
                        tabData: 1,
                        letters: 1,
                        agreements: 1,
                        custDrawings: 1,
                        definition: 1,
                        managerDetails: 1,
                        drafterDetails: 1,
                        engineerDetails: 1,
                        'selectedInInvoices.selectedScopes': 1,
                        'selectedInInvoices.isArchived': 1,
                        'selectedInInvoices._id': 1
                    }
                }
                ]).exec(function (aggregateError, result) {
                    if (!aggregateError) {
                        Scope.populate(result, scopePopulation, function (populateError, populatedScopes) {
                            if (!populateError) {

                                // console.log('Pop Scope', populatedScopes);
                                var copyOfAllScops = populatedScopes;
                                var pupulateScope = [];
                                // copyOfAllScops = _.unionBy(copyOfAllScops, '_id');
                                if (populatedScopes.length > 0) {
                                    _.map(populatedScopes, scope => {

                                        var totalBilled = 0;
                                        var childScopesOfCurrentScope = [];

                                        // childScopesOfCurrentScope = copyOfAllScops.filter(_scope => {
                                        //     return String(scope._id) === String(_scope.parent);
                                        // });
                                        copyOfAllScops.map(_scope => {
                                            if (String(_scope.parent) === String(scope._id)) {
                                                childScopesOfCurrentScope.push(_scope);
                                            }
                                        });

                                        scope.childScops = childScopesOfCurrentScope;

                                        _.forEach(scope.selectedInInvoices, function (currentInvoice) {
                                            // console.log(currentInvoice.isArchived)
                                            if (!currentInvoice.isArchived) {
                                                _.forEach(currentInvoice.selectedScopes, function (currentSelectedScope) {
                                                    if (String(currentSelectedScope.scope._id) === String(scope._id)) {
                                                        if (currentSelectedScope.isPartial) {
                                                            totalBilled += Number(currentSelectedScope.amount);
                                                        } else {
                                                            totalBilled += Number(currentSelectedScope.scope.price !== currentSelectedScope.oldPrice ? currentSelectedScope.oldPrice : currentSelectedScope.scope.price);
                                                        }
                                                    }
                                                });
                                            }
                                        });

                                        _.forEach(scope.childScops, function (currentChildScope) {
                                            _.forEach(currentChildScope.selectedInInvoices, function (currentInvoice) {
                                                if (!currentInvoice.isArchived) {
                                                    _.forEach(currentInvoice.selectedScopes, function (currentSelectedScope) {
                                                        if (String(currentSelectedScope.scope._id) === String(currentChildScope._id)) {
                                                            if (currentSelectedScope.isPartial) {
                                                                totalBilled += Number(currentSelectedScope.amount);
                                                            } else {
                                                                totalBilled += Number(currentSelectedScope.scope.price !== currentSelectedScope.oldPrice ? currentSelectedScope.oldPrice : currentSelectedScope.scope.price);
                                                                // totalBilled += (currentSelectedScope.scope.price);
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                        // console.log('Scope Price',scope.price, scope.parseFloat(parseFloat, ': ', scope.note.toFixed(2)))
                                        // console.log('Total Billed',totalBilled)
                                        if (scope.price > totalBilled) {
                                            pupulateScope.push(scope);
                                        }
                                    });
                                }

                                callBack(null, pupulateScope);
                            } else {
                                console.log('Pop Error', populateError);
                                callBack(populateError);
                            }
                        });
                        // callBack(null, result);
                    } else {
                        console.log('Aggregate error', aggregateError);
                        callBack(aggregateError);
                    }
                });
        }
    }, function (parallelError, allScopes) {
        if (!parallelError) {
            res.json(allScopes);
        } else {
            console.error(parallelError);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});


export default router;
