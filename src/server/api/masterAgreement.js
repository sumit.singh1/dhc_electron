import express from 'express';

import MasterAgreement from '../models/MasterAgreement';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { addTaskMilestone, createMasterAgreement } from '../utils/milestonesCreate';
import { selectedScopePopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common, { ensureAuthenticated } from './common';

const router = express.Router();


router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const masterAgreement = await MasterAgreement.findOne({ id })
            .populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(masterAgreement);
    } catch (e) {
        console.log('Error in get masterAgreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { masterAgreement, taskId } = req.body;
    try {
        const result = await MasterAgreement
            .findOneAndUpdate({ id }, masterAgreement, { new: true })
            .populate(selectedScopePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_MASTER_AGREEMENT, { taskId, masterAgreement: result });
    } catch (e) {
        console.log('Error in put masterAgreement/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { taskId, masterAgreement } = req.body;
    try {
        const result = await MasterAgreement
            .findOneAndUpdate({ id }, masterAgreement, { new: true, select: generateSelection(masterAgreement) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_MASTER_AGREEMENT, { taskId, masterAgreement: result });
    } catch (e) {
        console.log('Error in put masterAgreement/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', ensureAuthenticated, async (req, res) => {
    const { taskId, masterAgreement } = req.body;
    try {
        masterAgreement.templates = [
            {
                title: 'Scope:',
                isDone: true,
                isAttachment: false
            }, {
                title: 'Signed',
                isDone: false,
                isAttachment: false
            }
        ];
        const result = await createMasterAgreement(taskId, masterAgreement).catch(e => { throw e });;
        const data = await MasterAgreement.findById(result._id).populate(selectedScopePopulation).exec().catch(e => { throw e });
        const task = await addTaskMilestone('masterAgreements', taskId, data).catch(e => { throw e });
        res.json(task);
        emitScoket(req, socketType.ADD_MASTER_AGREEMENT, task);
    } catch (e) {
        console.log('Error in post masterAgreement/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        await MasterAgreement.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_MASTER_AGREEMENT, { id });
    } catch (e) {
        console.log('Error in delete masterAgreement/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
