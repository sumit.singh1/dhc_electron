import express from 'express';
import common from './common';

import CustomerImages from '../models/CustomerImage';
import constants from '../utils/constants';

const router = express.Router();

function getCustomerImage(query = {}) {
    return CustomerImages.findOne(query).exec();
}

// API to get logo and poster image for a particular customer/company
router.post(
    '/get-customer-image',
    common.ensureAuthenticated,
    async (request, response) => {
        let { name } = request.body;
        if (!name) {
            return response.status(constants.STATUS_CODES.NOT_FOUND).send(constants.ERROR_MSG.INVALID_ARGUMENTS);
        }
        try {
            response.json(await getCustomerImage({ name }));
        } catch (e) {
            console.log('Error in /get-customer-image API: ', e);
            response.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.ERROR_MSG.INTERNAL_SERVER_ERROR);
        }
    }
);

// API to save logo and poster image for a particular customer/company
router.put(
    '/save-customer-image',
    common.ensureAuthenticated,
    async (request, response) => {
        const { name, logo, poster } = request.body;

        if (!name || !(logo || poster)) {
            return response.status(constants.STATUS_CODES.NOT_FOUND).send(constants.ERROR_MSG.INVALID_ARGUMENTS);
        }

        const customer = await getCustomerImage({ name }).catch(e => console.log('error', e));

        try {

            // customer image exist. Modify them.
            if (customer) {
                const data = {
                    name,
                    logo: logo || '',
                    poster: poster || ''
                };
                return response.json(await CustomerImages.findOneAndUpdate({ name }, data, { new: true }).exec());
            }

            // if customer image does not exist create new
            let customerImage = new CustomerImages();
            customerImage.name = name;
            customerImage.logo = logo || '';
            customerImage.poster = poster || '';
            return response.json(await customerImage.save());

        } catch (e) {
            console.log('Error in /save-customer-image API: ', e);
            return response.status(503).send('Something went wrong while saving customer image information.');
        }
    }
);

export default router;
