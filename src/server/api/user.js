import _ from 'lodash';
import shortid from 'shortid';
import crypto from 'crypto';
import moment from 'moment';
import jwt from 'jsonwebtoken';
import aws from 'aws-sdk';
import express from 'express';

//  logger
import logger from '../utils/logger';

//  constants
import constants from '../utils/constants';

//  models
import User from '../models/User';
import HourTracker from '../models/HourTracker';
import Accessibility from '../models/Accessibility';
import { userRoleLocationPopulation, hourTrackerPopulation } from '../utils/populations.js';
//  others
import common, { addUserToRequest } from './common';

let router = express.Router();
aws.config.update({
    accessKeyId: 'AKIAIZ4V34GTKKDWQXLQ',
    secretAccessKey: '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'
});

function getUser(query = {}, population = '', callback) {
    User.findOne(query).populate(population).exec(function (err, user) {
        if (!err) {
            callback(null, user);
        } else {
            console.log(err);
            callback(err);
        }
    });
}


function getUsers(query = {}, population = '', sort = '', limit = '', callback) {
    User.find(query).populate(population).sort(sort).limit(limit).exec(function (err, users) {
        if (!err) {
            callback(null, users);
        } else {
            console.log(err);
            callback(err);
        }
    });
}

async function getTodaysHours(userId) {
    let startDate = new Date(moment().subtract(1, 'days').endOf('day')).toISOString();
    let endDate = new Date(moment().add(1, 'days').startOf('day')).toISOString();
    const query = {
        employee: userId,
        date: { $gte: startDate, $lte: endDate }
    }

    try {
        const hours = await HourTracker.find(query).populate(hourTrackerPopulation).lean().exec();
        const totalHours = hours.reduce((sum, hour) => sum += hour.hoursSpent, 0);
        return totalHours;
    } catch (error) {
        console.error('Error while getting sum of logged in hours:\n', error);
        return error;
    }
}

router.get('/user-total-hours/:userId', async (req, res) => {
    try {
        const todaysHours = await getTodaysHours(req.params.userId);
        res.json(todaysHours);
    } catch (todaysHoursError) {
        console.error('Unable to fetch today`s hour trackers:', todaysHoursError);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', addUserToRequest, function (req, res) {
    let { employeeCode,
        firstName,
        lastName,
        email,
        isActive,
        role,
        roleLevel,
        picture,
        titleBlock,
        initials,
        signature } = req.body;

    let query = {
        email: email
    };
    let population = [
        {
            path: 'role'
        }, {
            path: 'roleLevel'
        }
    ];

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_USER.NAME, constants.ACCESS_LEVELS.WRITE, function () {
        getUser(query, population, function (err, userExist) {
            if (err) {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                return;
            }

            if (userExist) {
                console.log('User with email ' + email + ' already present.');
                res.status(constants.STATUS_CODES.BAD_REQUEST).send(constants.RESPONSE_MESSAGES.USER.DUPLICATE);
                return;
            }
            let password = shortid.generate();
            const user = new User({
                employeeCode,
                firstName,
                lastName,
                email,
                isActive,
                role,
                roleLevel,
                picture,
                initials,
                signature,
                titleBlock,
                password
            });

            user.save(function (err, savedUser) {
                if (err) {
                    console.log(err);
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    return;
                }
                let html = '<div>Hello, ' + firstName + '<br><br>' + 'Your account has been created successfully on our platform with following details.<br>' + 'Your Email ' + email + ' </div><div>Your Password: <em> ' + password + '</em> </div> <br> Please click <a href="' + process.env.APP_URL + '"> here</a> to sign in to the application';
                common.sendEmail(res, email, 'Sign up successful', html, function () {
                    User.findOne({
                        id: savedUser.id
                    }).populate([
                        {
                            path: 'role'
                        }, {
                            path: 'roleLevel'
                        }
                    ]).exec(function (err, user) {
                        if (!err) {
                            res.json(user);
                        } else {
                            console.log(err);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });
                });
            });
        });
    });
});

router.put('/password', common.ensureAuthenticated, function (req, res) {
    let payload = req.body;
    let query = {
        id: payload.userId,
        isActive: true
    };
    let population = [
        {
            path: 'role'
        }, {
            path: 'roleLevel'
        }, {
            path: 'locationInfo.location'
        }
    ];
    getUser(query, population, function (err, user) {
        if (!err) {
            if (user) {
                user.comparePassword(payload.newPassword, function (newPasswordError, isPasswordMatch) {
                    if (!newPasswordError) {
                        if (isPasswordMatch) {
                            console.log('Old password and new password should not be same');
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.USER.SAME_PASSWORD);
                        } else {
                            user.comparePassword(payload.oldPassword, function (oldPasswordError, isMatch) {
                                if (!oldPasswordError) {
                                    if (isMatch) {
                                        user.password = payload.newPassword;
                                        if (payload.hasOwnProperty('isVerified')) {
                                            user.isVerified = payload.isVerified;
                                        }
                                        user.save(function (userError, updatedUser) {
                                            if (!userError) {
                                                res.json(updatedUser);
                                            } else {
                                                console.log(userError);
                                                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                            }
                                        });
                                    } else {
                                        console.log('Incorrect old password');
                                        res.status(constants.STATUS_CODES.BAD_REQUEST).send(constants.RESPONSE_MESSAGES.USER.INCORRECT_PASSWORD);
                                    }
                                } else {
                                    console.log(oldPasswordError);
                                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                }
                            });
                        }
                    } else {
                        console.log(newPasswordError);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log('User with id ' + payload.userId + ' not found.');
                res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.USER.NOT_FOUND);
            }
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/login', function (req, res) {
    let payload = req.body;
    let query = {
        email: payload.email,
        isActive: true
    };
    let population = [
        {
            path: 'role'
        }, {
            path: 'roleLevel'
        }, {
            path: 'locationInfo.location'
        }
    ];
    let io = req.app.get('socketio');
    let socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    getUser(query, population, function (err, user) {
        if (!err) {
            if (user) {
                user.comparePassword(payload.password, async (err, isMatch) => {
                    if (!err) {
                        if (isMatch) {
                            try {
                                user.todaysHours = await getTodaysHours(user._id);
                            } catch (hoursError) {
                                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                return;
                            }
                            Accessibility.findOne({
                                role: user.role._id,
                                level: user.roleLevel
                                    ? user.roleLevel._id
                                    : null
                            }).populate('permissions.permission').exec(function (err, access) {
                                if (!err) {
                                    user.permissions = [];
                                    if (access) {
                                        user.permissions = access.permissions;
                                    }
                                    // console.log('YYYY',  common.generateToken(user))
                                    common.getHours(user, function (err, hours) {
                                        if (!err) {
                                            user.urgentHours = hours.urgentHours;
                                            user.nonUrgentHours = hours.nonUrgentHours;
                                            res.json({
                                                token: common.generateToken(user),
                                                user: user,
                                            });
                                        } else {
                                            console.log(err);
                                            user.urgentHours = 0;
                                            user.nonUrgentHours = 0;
                                            res.json({
                                                token: common.generateToken(user),
                                                user: user,
                                            });
                                        }
                                    });
                                } else {
                                    console.log(err);
                                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                }
                            });
                        } else {
                            console.log('Incorrect password');
                            res.status(constants.STATUS_CODES.BAD_REQUEST).send(constants.RESPONSE_MESSAGES.USER.INVALID_PASSWORD);
                        }
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log('User with email ' + payload.email + ' not found.');
                res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.USER.NOT_FOUND);
            }
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/forgot', function (req, res) {
    let payload = req.body;
    let query = {
        email: payload.email,
        isActive: true
    };

    getUser(query, '', function (err, user) {
        if (!err) {
            if (user) {
                crypto.randomBytes(16, function (err, buf) {
                    if (!err) {
                        let token = buf.toString('hex');
                        user.passwordResetToken = token;
                        user.passwordResetExpires = Date.now() + 3600000; // expire in 1 hour
                        user.save(function (err, updatedUser) {
                            if (!err) {
                                let html = '<div>You are receiving this email because you (or someone else) have requested t' +
                                    'he reset of the password for your account.</div><div>Please click on the followi' +
                                    'ng link, or paste this into your browser to complete the process:</div><div>http' +
                                    '://' + req.headers.host + '/reset/' + token + '</div><div>If you did not request this, please ignore this email and your passwo' + 'rd will remain unchanged.</div>';
                                common.sendEmail(res, payload.email, 'Reset your password', html, function () {
                                    res.json(updatedUser);
                                });
                            } else {
                                console.log(err);
                                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                            }
                        });
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log('User with email ' + payload.email + ' not found.');
                res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.USER.NOT_FOUND);
            }
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/reset/:token', function (req, res) {
    let payload = req.body;
    let query = {
        passwordResetToken: req.params.token,
        passwordResetExpires: {
            $gt: moment()
        }
    };

    getUser(query, '', function (err, user) {
        if (!err) {
            if (user) {
                user.password = payload.password;
                user.passwordResetToken = undefined;
                user.passwordResetExpires = undefined;
                user.save(function (err, updatedUser) {
                    if (!err) {
                        res.json(updatedUser);
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log('Password reset token is invalid or has expired.');
                res.status(constants.STATUS_CODES.BAD_REQUEST).send(constants.RESPONSE_MESSAGES.USER.RESET_PASSWORD_EXPIRED);
            }
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/', common.ensureAuthenticated, function (req, res) {
    let sort = {
        firstName: -1
    };
    // return all users except Customers
    let query = {
        $and: [
            {
                role: { $ne: constants.CUSTOMER.ROLE }
            },
            {
                roleLevel: { $ne: constants.CUSTOMER.ROLE_LEVEL }
            }
        ]
    };

    getUsers(query, userRoleLocationPopulation, sort, null, function (err, users) {
        if (!err) {
            let sortedUsers = _.sortBy(users, ['firstName', 'employeeCode']);
            res.json(sortedUsers);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.get('/:userId', common.ensureAuthenticated, function (req, res) {
    let query = {
        id: req.params.userId
    };

    let population = [
        {
            path: 'role'
        }, {
            path: 'locationInfo.location'
        }, {
            path: 'roleLevel'
        }
    ];

    getUser(query, population, function (err, user) {
        if (!err) {
            res.json(user);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/getLoggedInUser', function (req, res) {
    let isAuthenticated = jwt.verify(req.body.authToken, process.env.TOKEN_SECRET);
    if (isAuthenticated) {
        let query = {
            id: isAuthenticated.sub,
            isActive: true
        };

        let population = [
            {
                path: 'role'
            }, {
                path: 'locationInfo.location'
            }, {
                path: 'roleLevel'
            }
        ];

        getUser(query, population, function (err, user) {
            if (!err) {
                if (user) {
                    user = user.toJSON();
                    Accessibility.findOne({
                        role: user.role._id,
                        level: user.roleLevel
                            ? user.roleLevel._id
                            : null
                    }).populate('permissions.permission').exec(async (err, access) => {
                        if (!err) {
                            user.permissions = [];
                            if (access) {
                                user.permissions = access.permissions;
                            }
                            try {
                                user.todaysHours = await getTodaysHours(user._id);
                            } catch (hoursError) {
                                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                return;
                            }
                            common.getHours(user, function (err, hours) {
                                if (!err) {
                                    user.urgentHours = hours.urgentHours;
                                    user.nonUrgentHours = hours.nonUrgentHours;
                                    res.json(user);
                                } else {
                                    console.log(err);
                                    user.urgentHours = 0;
                                    user.nonUrgentHours = 0;
                                    res.json(user);
                                }
                            });
                        } else {
                            console.log(err);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });
                } else {
                    console.log('User with id ' + req.params.userId + ' is not present.');
                    res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.USER.NOT_FOUND);
                }
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('Incorrect authentication token');
        res.status(constants.STATUS_CODES.UNAUTHORIZED).send(constants.RESPONSE_MESSAGES.USER.UNAUTHORIZED);
    }
});

router.get('/:id/totalHours', async (req, res) => {
    const _id = req.params.id;
    let user = null;
    try {
        user = await User.findById({ _id }).exec();
    } catch (e) {
        return res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }

    if (!user) {
        return res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.NO_USER_FOUND);
    }

    common.getHours(user, function (err, hours) {
        if (!err) {
            res.json(hours);
            let io = req.app.get('socketio');
            let socket = _.find(io.sockets.sockets, function (currentSocket) {
                return currentSocket.id === req.headers.socketid;
            });
            if (socket) {
                socket.broadcast.emit(constants.SOCKET_EVENTS.UPDATE_USER_TOTAL_HOURS, { _id, ...hours });
            }

        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);

        }
    });
});

// Check if email is already exists or not
router.post('/is-exists', common.ensureAuthenticated, (req, res) => {
    const { email } = req.body;
    getUser({ email }, '', (error, result) => {
        if (result) {
            res.status(200).send('Email address already exist.');
            return;
        }
        if (error) {
            res.status(403).send('Something went wrong.');
            return;
        }
        res.status(200).send('');

    });
});

router.post('/update-cutomer', common.ensureAuthenticated, (req, res) => {
    const {
        firstName,
        lastName,
        email,
        companyName,
        companyId,
        isActive,
        _id
    } = req.body;

    const data = {
        firstName,
        lastName,
        email,
        companyName,
        companyId,
        isActive
    };
    User.findOneAndUpdate(
        { _id: _id },
        data,
        { new: true })
        .lean()
        .exec((err, updatedUser) => {
            if (err) {
                console.log('Error in add-customer API : ', err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                return;
            }
            res.status(constants.STATUS_CODES.SUCCESS).send('');
        });
});

router.post('/add-customer', common.ensureAuthenticated, (req, res) => {
    const {
        firstName,
        lastName,
        email,
        companyName,
        companyId,
        isActive
    } = req.body;

    const password = shortid.generate();
    const role = constants.CUSTOMER.ROLE;
    const roleLevel = constants.CUSTOMER.ROLE_LEVEL;
    getUser({ email }, '', (error, isExist) => {
        if (error) {
            console.log('Error in add-customer API : ', error);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            return;
        }

        if (isExist) {
            console.log('User with email ' + email + ' already present.');
            res.status(200).send(constants.RESPONSE_MESSAGES.USER.DUPLICATE);
            return;
        }

        const user = new User({
            firstName,
            lastName,
            email,
            companyName,
            companyId,
            password,
            role,
            roleLevel,
            isActive
        });

        user.save((err, result) => {
            if (err) {
                console.log('Error in add-customer save API : ', error);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send('Error while adding new customer.');
                return;
            }
            let html = '<div>Hello, ' + firstName + '<br><br>' + 'Your account has been created successfully on our platform with following details.<br>' + 'Your Email ' + email + ' </div><div>Your Password: <em> ' + password + '</em> </div> <br> Please click <a href="' + process.env.APP_URL + '"> here</a> to sign in to the application';
            common.sendEmail(res, email, 'Sign up successful', html, () => {
                res.status(constants.STATUS_CODES.SUCCESS).send('');
            });

        });
    });
});


router.post('/get-all-customer', common.ensureAuthenticated, (req, res) => {
    let sort = {
        firstName: -1
    };
    let query = {
        $and: [
            {
                role: { $eq: constants.CUSTOMER.ROLE }
            },
            {
                roleLevel: { $eq: constants.CUSTOMER.ROLE_LEVEL }
            }
        ]
    };

    getUsers(query, userRoleLocationPopulation, sort, null, function (err, users) {
        if (!err) {
            let sortedUsers = _.sortBy(users, ['firstName', 'employeeCode']);
            let payload = {
                data: sortedUsers,
                status: constants.STATUS_CODES.SUCCESS,
                msg: 'Success'
            };
            res.status(constants.STATUS_CODES.SUCCESS).json(payload);
        } else {
            console.log('Error in get-all-customer API : ', err);
            let payload = {
                data: [],
                status: constants.STATUS_CODES.ERROR,
                msg: constants.ERROR_MSG.UNKNOWN_ERROR
            };
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });

});

router.put('/:userId', common.ensureAuthenticated, function (req, res) {
    let userId = req.params.userId;
    let params = req.body;
    let query = {
        id: userId
    };
    let io = req.app.get('socketio');
    let socket = _.find(io.sockets.sockets, function (currentSocket) {
        return currentSocket.id === req.headers.socketid;
    });
    let population = [
        {
            path: 'role'
        }, {
            path: 'roleLevel'
        }, {
            path: 'locationInfo.location'
        }, {
            path: 'workload'
        }
    ];

    User.findOneAndUpdate(query, params, {
        new: true
    }).populate(population).lean().exec(function (err, user) {
        if (!err) {
            let length = Object.keys(params).length;
            if (!(length === 1 && params.hasOwnProperty('scopeHighlights'))) {
                // params = _.merge(params, user);
                params.role = user.role;
                params.locationInfo = user.locationInfo;
                params.id = userId;
                params._id = user._id;
                if (socket) {
                    socket.broadcast.emit(constants.SOCKET_EVENTS.USER_UPDATE, { user: params });
                }
            }
            res.json(params);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.post('/signedUrl', common.ensureAuthenticated, function (req, res) {
    let s3 = new aws.S3();
    let params = {
        Bucket: req.body.bucketName,
        Key: req.body.fileName,
        Expires: 60,
        ContentType: req.body.fileType
    };

    s3.getSignedUrl('putObject', params, function (err, signedUrl) {
        if (!err) {
            res.json(signedUrl);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

module.exports = router;
