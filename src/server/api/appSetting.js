let remove = require('lodash/remove');
let forEach = require('lodash/forEach');
let escapeRegExp = require('lodash/escapeRegExp');
let find = require('lodash/find');

let shortid = require('shortid');
let async = require('async');
let Promise = require('bluebird');

let express = require('express');
let router = express.Router();

//logger
let logger = require('../utils/logger');
import { escapeRegExpresson } from '../utils/Validation'
import { userRoleLocationPopulation } from '../utils/populations.js';

//constants
let constants = require('../utils/constants');

//models
let AppSetting = require('../models/AppSetting');
let Location = require('../models/Location');
let Workload = require('../models/Workload');
let Role = require('../models/Role');
let User = require('../models/User');
let Task = require('../models/Task');
let ItemType = require('../models/ItemType');
let Tag = require('../models/Tag');

import moment from 'moment';

//common
let common = require('./common');

router.put('/', common.ensureAuthenticated, function (req, res) {
    let payload = req.body;

    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_APP.NAME, constants.ACCESS_LEVELS.WRITE, function () {
        AppSetting.findOneAndUpdate({}, payload, {
            new: true
        }, function (err, updatedAppSetting) {
            if (!err) {
                res.json(updatedAppSetting);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.get('/', common.ensureAuthenticated, function (req, res) {
    AppSetting.findOne(function (err, appSetting) {
        if (!err) {
            if (appSetting) {
                res.json(appSetting);
            } else {
                console.log('No settings found');
                res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.APP_SETTINGS.NOT_FOUND);
            }
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

router.put('/attachment', common.ensureAuthenticated, function (req, res) {
    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_APP.NAME, constants.ACCESS_LEVELS.WRITE, function () {
        AppSetting.findOne(function (err, appSetting) {
            if (!err) {
                if (appSetting) {
                    appSetting.attachments.push({ id: shortid.generate() + shortid.generate(), name: req.body.name });

                    appSetting.markModified('attachments');

                    appSetting.save(function (err, updatedAppSetting) {
                        if (!err) {
                            res.json(updatedAppSetting);
                        } else {
                            console.log(err);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });
                } else {
                    console.log('No settings found');
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.APP_SETTINGS.NOT_FOUND);
                }
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.delete('/attachment/:id', common.ensureAuthenticated, function (req, res) {
    common.hasPermission(req, res, constants.PERMISSIONS.MANAGE_APP.NAME, function () {
        AppSetting.findOne(function (err, appSetting) {
            if (!err) {
                if (appSetting) {

                    remove(appSetting.attachments, function (attachment) {
                        return attachment.id === req.params.id;
                    });

                    appSetting.markModified('attachments');

                    appSetting.save(function (err, updatedAppSetting) {
                        if (!err) {
                            res.json(updatedAppSetting);
                        } else {
                            console.log(err);
                            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                        }
                    });
                } else {
                    console.log('No settings found');
                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.APP_SETTINGS.NOT_FOUND);
                }
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    });
});

router.get('/master-data', common.ensureAuthenticated, function (req, res) {

    let rolePopulation = [
        {
            path: 'workload'
        }
    ];

    async.parallel({
        appSettings: function (callback) {
            AppSetting.findOne(function (err, appSetting) {
                if (!err) {
                    if (appSetting) {
                        callback(null, appSetting);
                    } else {
                        console.log('No settings found');
                        callback(null, {});
                    }
                } else {
                    console.log(err);
                    callback(null, {});
                }
            });
        },
        locations: function (callback) {
            Location.find(function (err, location) {
                if (!err) {
                    callback(null, location);
                } else {
                    console.log(err);
                    callback(null, {});
                }
            });
        },
        workloads: function (callback) {
            Workload.find(function (err, workload) {
                if (!err) {
                    callback(null, workload);
                } else {
                    console.log(err);
                    callback(null, {});
                }
            });
        },
        roles: function (callback) {
            Role.find({}).populate(rolePopulation).exec(function (err, role) {
                if (!err) {
                    callback(null, role);
                } else {
                    console.log(err);
                    callback(null, {});
                }
            });
        },
        users: function (callback) {
            let userFieldsSelection = {
                availability: 1,
                createdAt: 1,
                email: 1,
                employeeCode: 1,
                firstName: 1,
                id: 1,
                isActive: 1,
                isVerified: 1,
                lastName: 1,
                locationInfo: 1,
                nonUrgentHours: 1,
                payDayFormDate: 1,
                skypeId: 1,
                title: 1,
                updatedAt: 1,
                urgentHours: 1,
                picture: 1,
                _id: 1
            };
            var query = {
                $and: [
                    {
                        role: { $ne: constants.CUSTOMER.ROLE }
                    },
                    {
                        roleLevel: { $ne: constants.CUSTOMER.ROLE_LEVEL }
                    },
                    { isVerified: true },
                    { isActive: true }
                ]
            };

            User.find(query).populate(userRoleLocationPopulation).select(userFieldsSelection).lean().exec(function (err, users) {
                if (!err) {
                    let promises = [];

                    forEach(users, function (user) {
                        let promise = new Promise(function (resolve) {
                            common.getHours(user, function (err, hours) {
                                if (!err) {
                                    user.urgentHours = hours.urgentHours;
                                    user.nonUrgentHours = hours.nonUrgentHours;
                                    resolve();
                                } else {
                                    console.log(err);
                                    user.urgentHours = 0;
                                    user.nonUrgentHours = 0;
                                    resolve();
                                }
                            });
                        });
                        promises.push(promise);
                    });

                    Promise.all(promises).then(function () {
                        callback(null, users);
                    });
                } else {
                    console.log(err);
                    callback(null, {});
                }
            });
        },
        itemTypes: function (callback) {
            ItemType.find({}).lean().exec(function (err, itemTypes) {
                if (!err) {
                    callback(null, itemTypes);
                } else {
                    console.log(err);
                    callback(null, {});
                }
            });
        },
        tags: function (callback) {
            Tag.find({}, function (err, tags) {
                if (!err) {
                    callback(null, tags);
                } else {
                    console.log(err);
                    callback(null, []);
                }
            });
        }
    }, function (err, results) {
        if (!err) {
            res.json(results);
        } else {
            console.log(err);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    })
});


router.post('/search-options', common.ensureAuthenticated, async function (req, res) {

    let query = escapeRegExpresson(req.body.query);
    const userQuery = [{
        $match: {
            $and: [
                { isVerified: true },
                { isActive: true },
                {
                    $or: [
                        { firstName: { $regex: new RegExp(query, 'i') } },
                        { lastName: { $regex: new RegExp(query, 'i') } }
                    ]
                }
            ]
        }
    }];

    const regExpression = new RegExp(query, 'i');

    const startDate = moment().subtract('1', 'year').startOf('year');
    const endDate = moment().endOf('year');

    console.log('$$$$$', startDate, endDate)

    const taskQuery = [
        {
            $lookup: {
                from: 'tags',
                localField: 'tags',
                foreignField: '_id',
                as: 'tags'
            }
        }, {
            $addFields: {
                taskNumber: { $concat: [{ $substr: [{ $year: '$createdAt' }, 0, -1] }, '-', '$taskNumber'] },
                bidNumber: { $concat: ['B', { $substr: [{ $year: '$createdAt' }, 0, -1] }, '-', '$taskNumber'] }
            }
        },
        {
            $match: {
                $or: [
                    {
                        $and: [
                            {
                                $or: [
                                    { title: { $regex: regExpression } },
                                    { taskNumber: { $regex: regExpression } },
                                    { "tags.name": { $regex: regExpression } },
                                    { "contractor.name": { $regex: regExpression } },
                                ]
                            }
                        ]
                    },
                    {
                        $and: [
                            { isBidding: true },
                            { bidNumber: { $regex: regExpression } },
                        ]
                    },
                    {
                        createdAt: { $gte: startDate, $lte: endDate }
                    }
                ]
            }
        }
    ];

    await Promise.all([
        User.aggregate(userQuery).exec(),
        Task.aggregate(taskQuery).exec()]
    )
        .then(response => res.json({ users: response[0], tasks: response[1] }))
        .catch((e) => {
            console.log('OOO', e)
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR)
        })
});

module.exports = router;
