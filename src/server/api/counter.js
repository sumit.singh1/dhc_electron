import express from 'express';
import constants from '../utils/constants';
import common from './common';
import Counter from '../models/Counter';

const router = express.Router();

router.post('/update-counter', common.ensureAuthenticated, async (req, res) => {

    const taskID = req.body.taskID;
    const agreement = req.body.contract;
    const object = {
        [agreement]: 1
    };

    try {
        const doc = await  Counter.findOneAndUpdate({ taskID }, { $inc: object }, { new: true, select: object });
        res.json(doc[agreement]);
    } catch (e) {
        console.log('Error in update-counter : ', e.message);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
