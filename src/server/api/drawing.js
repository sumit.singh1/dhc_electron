import express from 'express';

import Drawing from '../models/Drawing';

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { generateSelection } from '../utils/common';
import { createDrawing } from '../utils/milestonesCreate';

const router = express.Router();

router.post('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const drawing = await createDrawing();
        res.json(drawing);
        emitScoket(req, socketType.ADD_DRAWING, drawing);
    } catch (e) {
        console.log('Error in drawing/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, drawing } = req.body;
        const result = await Drawing
            .findOneAndUpdate({ id }, drawing, { new: true, select: generateSelection(drawing) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_DRAWING, { taskId, drawing: result });
    } catch (e) {
        console.log('Error in drawing/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, drawing } = req.body;
        const result = await Drawing
            .findOneAndUpdate({ id }, drawing, { new: true, select: generateSelection(drawing) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_DRAWING, { taskId, drawing: result });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const drawing = await Drawing.findByIdAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_DRAWING, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
