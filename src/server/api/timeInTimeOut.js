import express from 'express';
import moment from 'moment';

import common from './common';
import constants from '../utils/constants';
import TimeInTimeOut from '../models/TimeInTimeOut';
import User from '../models/User';
const router = express.Router();

function getRecords (query  = {}) {
    return TimeInTimeOut.find(query).exec();
}


function saveTimeInOut({ date, timeIn, timeOut, lunchIn, lunchOut, user}) {
    let obj = {
        date,
        timeIn,
        timeOut,
        lunchIn,
        lunchOut,
        user
    };
    return new TimeInTimeOut(obj).save();
}

function getAllAdminDrafterUsers () {
    return User.find({
        $or : [
            {role: '58becfb062b976001126f776'},
            {role: '58bda81be8ac450011ce8964'}
        ]
    }).exec()
}

function getUserDataObject(usersD) {
    var users = [];
    usersD.map((userD) => {
        var i = 0;
        var user = [];
        while(i<16) {
            user.push({
                date: moment().subtract(i, 'days').valueOf(),
                user: userD._id,
                timeIn: moment().subtract(i, 'days').hours(7).minute(0).valueOf(),
                timeOut: moment().subtract(i, 'days').hours(16).minute(0).valueOf(),
                lunchIn: moment().subtract(i, 'days').hours(11).minute(30).valueOf(),
                lunchOut: moment().subtract(i, 'days').hours(12).minute(30).valueOf(),
            });
            i++;
        }
        users.push(user);
    });
    return users;
}
function updateUsers (object) {
    return object.map(data => {
        return data.map(userData => {
            const result = new TimeInTimeOut(userData).save();
            console.log(result);
            return result;
        })
    });
}

function updateRecord(body){
    return TimeInTimeOut.findOneAndUpdate({_id: body._id}, body, {new: true}).exec();
}

router.post('/update-users', async (req, res) => {

    const users = await getAllAdminDrafterUsers();
    const result = getUserDataObject(users);
    const updateResult = await updateUsers(result);
   res.json(updateResult);
});
 
router.post('/update/time', async(req, res)  => {
    const params = req.body;
    console.log(params);
    try {
        const result = await updateRecord(params);
        res.json({data:result});
    } catch(e){
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message || constants.RESPONSE_MESSAGES.REFRESH_PAGE); 
    }
});

router.get('/', common.ensureAuthenticated, async (req, res) => {
    try {
        const records = await getRecords().catch(e => {
            throw new Error(constants.RESPONSE_MESSAGES.CIENT_CONNECTION_ERROR)
        });
        res.json(records);
    } catch(e) {
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message || constants.RESPONSE_MESSAGES.REFRESH_PAGE);
    }
    
});

router.post('/user/record', common.ensureAuthenticated, async(req, res) => {
    const {user, date} = req.body;
    const query = {
        $and: [
            { user: user },
            {
                date: {
                    $gte: date,
                    $lte: date
                }
            }
        ]        
    };
    try {
        const records = await getRecords(query).catch(e => {
            throw new Error(constants.RESPONSE_MESSAGES.CIENT_CONNECTION_ERROR)
        });
        res.json(records);
    } catch(e) {
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message || constants.RESPONSE_MESSAGES.REFRESH_PAGE);
    }
});

router.post('/save', common.ensureAuthenticated, async (req, res) => {
    const { date, timeIn, timeOut, lunchIn, lunchOut, user} = req.body;
    try{
        const result = await saveTimeInOut(req.body);
        res.json({data:result});
    } catch(e){
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message || constants.RESPONSE_MESSAGES.REFRESH_PAGE);
    }

});

export default router;
