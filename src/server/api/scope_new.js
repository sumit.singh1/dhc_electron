import express from 'express';

import Scope from '../models/Scope';
import Counter from '../models/Counter';
import Task from '../models/Task';
let mongoose = require('mongoose');

import common from './common';
import constants from '../utils/constants';
import { emitScoket, socketType } from '../utils/socket';
import { defaultSelection, scopeByGroupSelection } from '../utils/selection';
import { generateSelection } from '../utils/common';
import { scopePopulationNew, scopePopulation, genetatePopulation } from '../utils/populations';
import { createCalc, createDrawing, createCustDrawing, createTabData, createLetter, createScope } from '../utils/milestonesCreate';


import User from '../models/User';
import { getCounter, incrementScopeCounter, incrementRevScopeCounter } from '../utils/counterUtils';

const router = express.Router();

// router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
//     const id = req.params.id;
//     const body = req.body;
//     try {
//         const scope = await Scope
//             .findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) })
//             .exec();
//         res.json(scope);
//         emitScoket(req, socketType.ARCHIVE_SCOPE, scope);
//     } catch (e) {
//         console.log('Error in put scope/archive/:id API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.put('/:id', common.ensureAuthenticated, async (req, res) => {
//     const id = req.params.id;
//     const body = req.body;
//     try {
//         const scope = await Scope.findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) })
//             .populate(genetatePopulation(body))
//             .exec();
//         res.json(scope);
//         emitScoket(req, socketType.UPDATE_SCOPE, scope);
//     } catch (e) {
//         console.log('Error in put scope/:id API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.put('/:id/customer_drawing', common.ensureAuthenticated, async (req, res) => {
//     try {
//         const _id = req.params.id;
//         const { number, taskId } = req.body;
//         const customerDrawing = await createCustDrawing(number || 0);
//         if (!customerDrawing) {
//             throw 'error in creating customer drawing setup'
//         }
//         const body = {
//             custDrawings: [customerDrawing._id]
//         };
//         const scope = await Scope.findOneAndUpdate({ _id }, body, { new: true, select: defaultSelection }).exec();
//         scope.custDrawings = [customerDrawing];
//         res.json(scope);
//         emitScoket(req, socketType.UPDATE_SCOPE_MILESTONE, { scope, taskId });
//     } catch (e) {
//         console.log('Error in put scope/:id API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.put('/:id/letter', common.ensureAuthenticated, async (req, res) => {
//     try {
//         const _id = req.params.id;
//         const { number, taskId } = req.body;
//         const letter = await createLetter(number || 0);
//         if (!letter) {
//             throw 'error in creating customer drawing setup'
//         }
//         const body = {
//             letters: [letter._id]
//         };
//         const scope = await Scope.findOneAndUpdate({ _id }, body, { new: true, select: defaultSelection }).exec();
//         scope.letters = [letter];
//         res.json(scope);
//         emitScoket(req, socketType.UPDATE_SCOPE_MILESTONE, { scope, taskId });
//     } catch (e) {
//         console.log('Error in put scope/:id API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.put('/:id/tab_data', common.ensureAuthenticated, async (req, res) => {
//     try {
//         const _id = req.params.id;
//         const { number, taskId } = req.body;
//         const tabData = await createTabData(number || 0);
//         if (!tabData) {
//             throw 'error in creating customer drawing setup'
//         }
//         const body = {
//             tabData: [tabData._id]
//         };
//         const scope = await Scope.findOneAndUpdate({ _id }, body, { new: true, select: defaultSelection }).exec();
//         scope.tabData = [tabData];
//         res.json(scope);
//         emitScoket(req, socketType.UPDATE_SCOPE_MILESTONE, { scope, taskId });
//     } catch (e) {
//         console.log('Error in put scope/:id API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.post('/group/:id', common.ensureAuthenticated, async (req, res) => {
//     const groupId = req.params.id;
//     const { userId } = req.body;
//     const query = {
//         $and: [
//             {
//                 $or: [
//                     { 'engineerDetails.engineer': userId },
//                     { 'drafterDetails.drafter': userId },
//                     { 'managerDetails.manager': userId }
//                 ]
//             },
//             { group: groupId },
//             { isArchived: false }
//         ]
//     };

//     const sort = {
//         createdAt: -1
//     };

//     try {
//         const scopes = await Scope.find(query).populate(scopePopulationNew).select(scopeByGroupSelection).sort(sort).exec();
//         res.json(scopes);
//     } catch (e) {
//         console.log('Error in post scope/group/:id API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.post('/', common.ensureAuthenticated, async (req, res) => {
//     const { scope, taskID } = req.body;
//     try {
//         const counter = await getCounter(taskID);
//         const updatedCounter = await incrementScopeCounter(counter, 1);
//         if (!updatedCounter) {
//             throw Error('counter not updated');
//         }
//         const newScope = await createScope(scope);
//         await Task.findOneAndUpdate({ id: taskID }, { $push: { scopes: newScope._id } }).exec();
//         const result = await Scope.findOne({ id: newScope.id }).populate(scopePopulation).lean().exec();
//         res.json(result);
//     } catch (e) {
//         console.log('Error in post scope/ API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });


// router.put('/:id/hourtracker/add', common.ensureAuthenticated, async (req, res) => {
//     const id = req.params.id
//     const hourTracker = req.body;
//     try {
//         const scope = await Scope.findOneAndUpdate({ id }, { $push: { hourTrackers: hourTracker._id } }, { new: true, select: defaultSelection }).lean().exec();
//         scope.hourtracker = hourTracker;
//         res.json(scope);
//         emitScoket(req, socketType.ADD_HOUR_TRACKER_TO_SCOPE, scope);
//     } catch (e) {
//         console.log('Error in post scope/:id/hourtracker/add API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.put('/:id/hourtracker/remove', common.ensureAuthenticated, async (req, res) => {
//     const id = req.params.id
//     const { hourtrackerId } = req.body;
//     try {
//         const scope = await Scope
//             .findOneAndUpdate(
//                 { id },
//                 { $pull: { hourTrackers: mongoose.Types.ObjectId(hourtrackerId) } },
//                 { select: defaultSelection }
//             )
//             .lean()
//             .exec()
//             .catch(e => { throw e });
//         scope.hourTracker = hourtrackerId;
//         res.json(scope);
//         emitScoket(req, socketType.REMOVE_HOUR_TRACKER_FROM_SCOPE, scope);
//     } catch (e) {
//         console.log('Error in post scope/:id/hourtracker/remove API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.put('/:id/highlight', common.ensureAuthenticated, async (req, res) => {
//     const id = req.params.id
//     const highlight = req.body;
//     try {
//         let scope = null;
//         if (!highlight.hasOwnProperty('_id')) {
//             // create new highlight
//             scope = await Scope
//                 .findOneAndUpdate(
//                     { id },
//                     { $push: { highlights: highlight } },
//                     { new: true }
//                 )
//                 .lean()
//                 .exec();
//         } else {
//             // update highlight
//             scope = await Scope
//                 .findOneAndUpdate(
//                     { id, "highlights._id": highlight._id },
//                     { $set: { "highlights.$": highlight } },
//                     { new: true }
//                 )
//                 .lean()
//                 .exec();
//         }

//         if (!scope) {
//             throw error('not able to update scope');
//         }
//         res.json(scope.highlights);
//     } catch (e) {
//         console.log('Error in put scope/:id/highlight API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

// router.post('/add-rev-scope', async (req, res) => {
//     let scope = req.body;
//     try {
//         const { nextRev } = await incrementRevScopeCounter(scope._id);

//         const revScopes = await Scope.find({ parent: scope._id }).exec();
//         let group = scope.group._id;
//         if (scope.group._id === '5907701d219b330011d09bca') {
//             group = '58bed83e62b976001126f7a6';
//         }
//         if (revScopes.length > 0) {
//             let lastRevScope = revScopes.filter(item => !item.isArchived).sort((first, second) => {
//                 return first.number > second.number ? -1 : first.number < second.number ? 1 : 0;
//             })[0];
//             group = lastRevScope.group;
//             if (lastRevScope.group._id === '5907701d219b330011d09bca') {
//                 group = '58bed83e62b976001126f7a6';
//             }
//         }

//         const revScope = {
//             group,
//             parent: scope._id,
//             task: scope.task,
//             itemType: scope.itemType,
//             dueDate: new Date(),
//             price: 0,
//             note: scope.note,
//             managerDetails: {
//                 ...scope.managerDetails,
//                 nonUrgentHours: 0,
//                 urgentHours: 0
//             },
//             engineerDetails: {
//                 ...scope.engineerDetails,
//                 nonUrgentHours: 0,
//                 urgentHours: 0
//             },
//             drafterDetails: {
//                 ...scope.drafterDetails,
//                 nonUrgentHours: 0,
//                 urgentHours: 0
//             },
//             number: `${scope.number}-${nextRev}`
//         };
//         const newScope = await createScope(revScope, nextRev);
//         await Task.findOneAndUpdate({ _id: scope.task }, { $push: { scopes: newScope._id } }).exec();
//         const result = await Scope.findOne({ id: newScope.id }).populate(scopePopulation).lean().exec();
//         res.json(result);
//     } catch (e) {
//         console.log('Error in put scope/:id/add-rev-sope API', e);
//         res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
//     }
// });

export default router;
