var axios = require('axios');
var express = require('express');
var router = express.Router();

//models
var User = require('../models/User');

//logger
var logger = require('../utils/logger');

//constants
var constants = require('../utils/constants');

//others
var common = require('./common');

var error = constants.RESPONSES;

var authUrl = 'https://slack.com/oauth/authorize';
var accessTokenUrl = 'https://slack.com/api/oauth.access';
var scope = 'client';

function getAuthUrl() {
    return authUrl + '?client_id=' + process.env.SLACK_CLIENT_ID + '&scope=' + scope + '&redirect_uri=' + process.env.SLACK_REDIRECT_URI + '&team=' + process.env.SLACK_TEAM_ID;
}

function getAccessToken(code, callback) {
    var url = accessTokenUrl + '?client_id=' + process.env.SLACK_CLIENT_ID + '&client_secret=' + process.env.SLACK_CLIENT_SECRET + '&code=' + code + '&redirect_uri=' + process.env.SLACK_REDIRECT_URI;

    axios
        .get(url)
        .then(function(response) {
            if (response.data.ok) {
                callback(null, response.data);
            } else {
                callback(response.data);
            }
        })
        .catch(function(err) {
            console.log(err);
            callback(err);
        });
}

function createChannel(name, accessToken, callback) {
    var url = 'https://slack.com/api/channels.create?token=' + accessToken + '&name=' + name;

    axios
        .get(url)
        .then(function(response) {
            if (response.data.ok) {
                callback(null, response.data.channel);
            } else {
                callback(response.data);
            }
        })
        .catch(function(err) {
            console.log(err);
            callback(err);
        });
}

function inviteToTeam(email, accessToken, callback) {
    var url = 'https://slack.com/api/users.admin.invite?token=' + accessToken + '&email=' + email;

    axios.get(url)
        .then(function(response) {
            if (response.data.ok) {
                callback(null, response.data);
            } else {
                callback(response.data);
            }
        })
        .catch(function(err) {
            console.log(err);
            callback(err);
        });
}

function inviteToChannel(channelId, userId, accessToken, callback) {
    var url = 'https://slack.com/api/channels.invite?token=' + accessToken + '&channel=' + channelId + '&user=' + userId;

    axios.get(url)
        .then(function(response) {
            if (response.data.ok) {
                callback(null, response.data);
            } else {
                callback(response.data);
            }
        })
        .catch(function(err) {
            console.log(err);
            callback(err);
        });
}

function kickFromChannel(channelId, userId, accessToken, callback) {
    var url = 'https://slack.com/api/channels.kick?token=' + accessToken + '&channel=' + channelId + '&user=' + userId;

    axios
        .get(url)
        .then(function(response) {
            if (response.data.ok) {
                callback(null, response.data);
            } else {
                callback(response.data);
            }
        })
        .catch(function(err) {
            console.log(err);
            callback(err);
        });
}

router.get('/authUrl', common.ensureAuthenticated, function(req, res) {
    res.json(getAuthUrl());
});

router.get('/login/:code/:userId', common.ensureAuthenticated, function(req, res) {
    if (typeof req.params.code !== 'undefined') {
        getAccessToken(req.params.code, function(err, data) {
            if (!err) {
                User.findOne({
                    id: req.params.userId
                }, function(err, user) {
                    if (!err) {
                        if (!user.slackId) {
                            user.slackId = data.user_id;
                            user.save(function(err, updatedUser) {
                                if (!err) {
                                    res.cookie(constants.COOKIES.SLACK_ACCESS_TOKEN, data.access_token);
                                    res.json(updatedUser);
                                } else {
                                    console.log(err);
                                    res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                                }
                            });
                        } else {
                            res.cookie(constants.COOKIES.SLACK_ACCESS_TOKEN, data.access_token);
                            res.json(user);
                        }
                    } else {
                        console.log(err);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('Invalid Slack Login');
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/channel', common.ensureAuthenticated, function(req, res) {
    var name = req.body.name;
    var accessToken = req.cookies.SLACK_ACCESS_TOKEN;

    if (accessToken) {
        createChannel(name, accessToken, function(err, channel) {
            if (!err) {
                res.json(channel);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('Slack token is not present');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.SLACK.TOKEN_NOT_FOUND);
    }
});

router.get('/invite/team/:email', common.ensureAuthenticated, function(req, res) {
    var email = req.params.email;
    var accessToken = req.cookies.SLACK_ACCESS_TOKEN;

    if (accessToken) {
        inviteToTeam(email, accessToken, function(err, done) {
            if (!err) {
                res.json(done);
            } else {
                console.log(err);
                var html = '<div>DHC hs invited you to join their Slack team. Click on the link below<br/>ht' +
                    'tps://dhc-app.slack.com</div>';
                common.sendEmail(res, email, 'Invitation to join DHC on slack.', html, function() {
                    res.json({
                        ok: true
                    });
                });
            }
        });
    } else {
        console.log('Slack token is not present');
        var html = '<div>DHC hs invited you to join their Slack team. Click on the link below<br/>ht' +
            'tps://dhc-app.slack.com</div>';
        common.sendEmail(res, email, 'Invitation to join DHC on slack.', html, function() {
            res.json({
                ok: true
            });
        });
    }
});

router.get('/invite/channel/:channelId/:userId', common.ensureAuthenticated, function(req, res) {
    var channelId = req.params.channelId;
    var userId = req.params.userId;
    var accessToken = req.cookies.SLACK_ACCESS_TOKEN;

    if (accessToken) {
        inviteToChannel(channelId, userId, accessToken, function(err, channel) {
            if (!err) {
                res.json(channel);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('Slack token is not present');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.SLACK.TOKEN_NOT_FOUND);
    }
});

router.get('/kick/channel/:channelId/:userId', common.ensureAuthenticated, function(req, res) {
    var channelId = req.params.channelId;
    var userId = req.params.userId;
    var accessToken = req.cookies.SLACK_ACCESS_TOKEN;

    if (accessToken) {
        kickFromChannel(channelId, userId, accessToken, function(err, channel) {
            if (!err) {
                res.json(channel);
            } else {
                console.log(err);
                res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
            }
        });
    } else {
        console.log('Slack token is not present');
        res.status(constants.STATUS_CODES.NOT_FOUND).send(constants.RESPONSE_MESSAGES.SLACK.TOKEN_NOT_FOUND);
    }
});

module.exports = router;
