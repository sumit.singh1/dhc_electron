import express from 'express';

import Task from '../models/Task';
import { generateSelection, padWithZeroes } from '../utils/common';
import constants from '../utils/constants';
import { createCounter, incrementBidNumberCounter, incrementScopeCounter, incrementTaskGroupNumberCounter, incrementTaskNumberCounter } from '../utils/counterUtils';
import { addTaskMilestone, createAgreement, createClientAgreement, createInvoice, createMasterAgreement, createModifiedAgreement, createPurchaseOrder, createScope, createTaskGroupScope } from '../utils/milestonesCreate';
import { getTaskPopulation } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import { ensureAuthenticated } from './common';

const router = express.Router();

router.get('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await Task.findOne({ id }).populate(getTaskPopulation).exec();
        res.json(task);
    } catch (e) {
        console.log('Error in get task/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/', ensureAuthenticated, async (req, res) => {
    const contractor = req.query.contractor;
    const title = req.query.title;
    try {
        const query = {
            $and: [
                {
                    title: {
                        $regex: '^' + title + '$',
                        $options: 'i'
                    }
                },
                {
                    'contractor.name': contractor
                }
            ]
        };
        const tasks = await Task.find(query).exec();
        res.json(tasks);
    } catch (e) {
        console.log('Error in get task/?title=""&contractor="" API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const task = await Task.findOneAndUpdate({ id }, body, { new: true, select: generateSelection(body) }).exec();
        res.json(task);
    } catch (e) {
        console.log('Error in put task/:id API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', async (req, res) => {
    const { task, scopes } = req.body;
    task.teamMembers = []; // remove this after removing team member field from task model
    try {
        let taskNumber = null;
        if (task.isBidding) {
            taskNumber = await incrementBidNumberCounter();
        } else if (task.isFromTaskGroup) {
            taskNumber = await incrementTaskGroupNumberCounter();
        } else {
            taskNumber = await incrementTaskNumberCounter();
        }
        if (!taskNumber) {
            throw 'unable to create a task number';
        }
        task.taskNumber = padWithZeroes(taskNumber, 4);
        const newTask = await (new Task(task)).save();
        const counter = await createCounter(newTask.id);
        const updatedCounter = await incrementScopeCounter(counter, scopes.length);
        if (!updatedCounter) {
            throw 'counter not updated';
        }
        scopes.forEach((scope, index) => scope.number = String.fromCharCode(65 + index));
        let newScopes = [];
        if(newTask.isFromTaskGroup) {
            newScopes = await Promise.all(scopes.map(scope => createTaskGroupScope({ ...scope, task: newTask._id })));
        } else {
            newScopes = await Promise.all(scopes.map(scope => createScope({ ...scope, task: newTask._id })));
        }

        if(newScopes.length === 0) {
            throw Error('Error in creating scopes')
        }

        // add logic of creating milestones
        const promises = [];
        const milestoneTypes = [];

        let selectedScopes = newScopes.map(item => item._id);

        let data = {
            scopes: selectedScopes
        };

        if (!task.isFromTaskGroup) {

            if (task.contractor.poRequired) {
                promises.push(createPurchaseOrder(newTask.id));
                milestoneTypes.push('purchaseOrders');
            }

            switch (task.contractor.contract.toLowerCase()) {
                case 'csa':
                    promises.push(createAgreement(newTask.id, { selectedScopes }, false));
                    milestoneTypes.push('agreements');
                    break;
                case 'ma':
                    promises.push(createMasterAgreement(newTask.id, { selectedScopes }));
                    milestoneTypes.push('masterAgreements');
                    break;
                case 'mcsa':
                    promises.push(createModifiedAgreement(newTask.id, { selectedScopes }));
                    milestoneTypes.push('modifiedAgreements');
                    break;
                case 'ca':
                    promises.push(createClientAgreement(newTask.id, { selectedScopes }));
                    milestoneTypes.push('clientAgreements');
                    break;
            }

            promises.push(createInvoice(newTask.id, {
                selectedScopes: newScopes.map(item => ({ scope: item._id, oldPrice: item.price, description: item.definition }))
            }));

            milestoneTypes.push('invoices');

            const milestones = await Promise.all(promises).catch(e => {
                throw e;
            });

            milestoneTypes.forEach((item, index) => data[item] = milestones[index]);
        }

        const result = await Task.findOneAndUpdate({ id: newTask.id }, data, { new: true }).populate(getTaskPopulation).exec();
        res.json(result);
        emitScoket(req, socketType.ADD_TASK, result);
    } catch (e) {
        console.log('Error in post task/ API : ', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/purchase_order', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await addTaskMilestone('purchaseOrders', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_PURCHASE_ORDER, task);
    } catch (e) {
        console.log('Error in put task/:id/purchase_order API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = addTaskMilestone('agreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/master_agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = addTaskMilestone('masterAgreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_MASTER_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/master_agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/modified_agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = addTaskMilestone('modifiedAgreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_MODIFIED_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/modified_agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id/client_agreement', ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const task = await addTaskMilestone('clientAgreements', id, req.body);
        res.json(task);
        emitScoket(req, socketType.ADD_TASK_CLIENT_AGREEMENT, task);
    } catch (e) {
        console.log('Error in put task/:id/client_agreement API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

export default router;
