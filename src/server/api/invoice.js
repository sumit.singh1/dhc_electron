import async from 'async';
import xl from 'excel4node';
import express from 'express';
import _ from 'lodash';
import moment from 'moment';
import mongoose from 'mongoose';

import Invoice from '../models/Invoice';
import User from '../models/User';
import Scope from '../models/Scope';
import Task from '../models/Task';
import { generateSelection } from '../utils/common';
import constants from '../utils/constants';
import { addTaskMilestone, createInvoice } from '../utils/milestonesCreate';
import { invoiceTablePopulation, scopePopulationNew } from '../utils/populations';
import { emitScoket, socketType } from '../utils/socket';
import common from './common';

const router = express.Router();

async function getScopesWithHoldInvoices(userID = null) {
    const query = [
        {
            $match: {
                isArchived: false,
                hold: 'Y'
            }
        },
        {
            $lookup: {
                from: 'scopes',
                localField: 'selectedScopes.scope',
                foreignField: '_id',
                as: 'selectedScope'
            }
        },
        {
            $unwind: '$selectedScope'
        },
        {
            $group: {
                _id: '$selectedScope._id', scope: { $addToSet: "$selectedScope" }
            }
        },
        {
            $group: { _id: null, scopes: { $addToSet: { $arrayElemAt: ["$scope", 0] } } }
        }
    ]
    if (userID) {
        query.push(
            {
                $unwind: "$scopes"
            },
            {
                $match: {
                    $or: [
                        {
                            "scopes.managerDetails.manager": mongoose.Types.ObjectId(userID)
                        },
                        {
                            "scopes.engineerDetails.engineer": mongoose.Types.ObjectId(userID)
                        },
                        {
                            "scopes.drafterDetails.drafter": mongoose.Types.ObjectId(userID)
                        }
                    ]
                }
            },
            {
                $group: {
                    _id: "null", scopes: { $addToSet: "$scopes" }
                }
            }
        )
    }
    const holdInvoicesScopes = await Invoice.aggregate(query).exec().catch(e => { throw e });
    return holdInvoicesScopes.length && holdInvoicesScopes[0].scopes ? holdInvoicesScopes[0].scopes : [];
}

async function getCompletedUnInvoicedScopes(userID = null) {
    let query = [
        {
            $match: {
                isArchived: false,
                group: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS)
            }
        },
        {
            $lookup: {
                from: 'invoices',
                localField: '_id',
                foreignField: 'selectedScopes.scope',
                as: 'selectedInInvoice'
            }
        },
        {
            $unwind: "$selectedInInvoice"
        },
        {
            $addFields: {
                filteredInvoiceScopes: {
                    $filter: {
                        input: "$selectedInInvoice.selectedScopes",
                        as: "invoiceScope",
                        cond: {
                            $eq: ["$$invoiceScope.scope", "$_id"]
                        }
                    }
                }
            }
        },
        {
            $addFields: {
                'filteredInvoiceScope': {
                    $arrayElemAt: [
                        '$filteredInvoiceScopes', 0
                    ]
                }

            }
        },
        {
            $group: {
                _id: "$_id",
                price: { $addToSet: "$price" },
                rootScope: { $addToSet: "$$ROOT" },
                total: {
                    $sum: {
                        $cond: {
                            if: { $eq: ['$filteredInvoiceScope.isPartial', true] },
                            then: '$filteredInvoiceScope.amount',
                            else: {
                                $cond: {
                                    if: { $ne: ['$filteredInvoiceScope.oldPrice', '$price'] },
                                    then: '$filteredInvoiceScope.oldPrice',
                                    else: '$price'
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            $addFields: {
                price: { $arrayElemAt: ['$price', 0] }
            }
        },
        {
            $addFields: {
                isPriceGreater: { $gt: ["$price", "$total"] }
            }
        },
        {
            $match: {
                isPriceGreater: true
            }
        },
        {
            $group: {
                _id: null, scopes: { $addToSet: { $arrayElemAt: ["$rootScope", 0] } }
            }
        }
    ];

    if (userID) {
        query[0].$match.$or = [
            { 'engineerDetails.engineer': mongoose.Types.ObjectId(userID) },
            { 'managerDetails.manager': mongoose.Types.ObjectId(userID) },
            { 'drafterDetails.drafter': mongoose.Types.ObjectId(userID) }
        ]
    }
    const completeUnInvoicedScopes = await Scope.aggregate(query).exec().catch(e => { throw e });
    return completeUnInvoicedScopes.length && completeUnInvoicedScopes[0].scopes ? completeUnInvoicedScopes[0].scopes : [];;
}

router.get('/unInvoiced/:userID', async (req, res) => {
    try {
        const userID = req.params.userID;
        const { role } = await User.findOne({ _id: userID }).populate({ path: 'role' }).exec();
        const user = role.id === constants.ROLE_ID.ADMIN ? null : userID;

        async.series({
            holdInvoiceScopes: async function (callBack) {
                callBack(null, await getScopesWithHoldInvoices(user));
            },
            completedUnInvoiced: async function (callBack) {
                callBack(null, await getCompletedUnInvoicedScopes(user));
            }
        }, (unInvoicedError, unInvoicedScopes) => {
            if (!unInvoicedError) {
                Scope.populate([...unInvoicedScopes.holdInvoiceScopes, ...unInvoicedScopes.completedUnInvoiced], scopePopulationNew, (populationError, populatedScopes) => {
                    if (!populationError) {
                        console.log('Total Number of Scopes:', populatedScopes.length);
                        res.json(populatedScopes);
                    } else {
                        console.error('Error in get invoice/unInvoiced:userID API\n', populationError);
                        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
                    }
                });
            } else {

            }
        });
    } catch (e) {
        console.error('Error in get invoice/unInvoiced:userID API\n', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/', common.ensureAuthenticated, async (req, res) => {
    const { taskId, invoice } = req.body;
    try {
        const result = await createInvoice(taskId, invoice).catch(e => { throw e });
        if (invoice.selectedScopes) {
            const promises = [];
            invoice.selectedScopes.forEach(selectedScope => {
                if (!selectedScope.isPartial && selectedScope.scope.price !== selectedScope.oldPrice) {
                    promises.push(Scope.findOneAndUpdate({ _id: selectedScope.scope._id }, { price: selectedScope.oldPrice }, { new: true }));
                }
            })
            await Promise.all(promises).catch(e => { throw e });
        }
        const data = await Invoice.findById(result._id).populate(invoiceTablePopulation).exec();
        const task = await addTaskMilestone('invoices', taskId, data)
        res.json(task);
        emitScoket(req, socketType.ADD_INVOICE, task);
    } catch (e) {
        console.log('Error in invoice/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/archive/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    const { taskId, invoice } = req.body;
    try {
        const result = await Invoice
            .findOneAndUpdate({ id }, invoice, { new: true, select: generateSelection(invoice) })
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_INVOICE, { taskId, invoice: result });
    } catch (e) {
        console.log('Error in invoice/archive/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.put('/:id', common.ensureAuthenticated, async (req, res) => {
    try {
        const id = req.params.id;
        const { taskId, invoice } = req.body;
        if (invoice.selectedScopes) {
            const promises = [];
            invoice.selectedScopes.forEach(selectedScope => {
                const priceUpdated = invoice.priceUpdatedScopesInInvoice && invoice.priceUpdatedScopesInInvoice.find(id => id === selectedScope.scope.id);

                if (priceUpdated && !selectedScope.isPartial && selectedScope.scope.price !== selectedScope.oldPrice) {
                    promises.push(Scope.findOneAndUpdate({ _id: selectedScope.scope._id }, { price: selectedScope.oldPrice }, { new: true }));
                }
            })
            await Promise.all(promises).catch(e => { throw e });
        }
        const result = await Invoice
            .findOneAndUpdate({ id }, invoice, { new: true })
            .populate(invoiceTablePopulation)
            .exec()
            .catch(e => { throw e });
        res.json(result);
        emitScoket(req, socketType.UPDATE_INVOICE, { taskId, invoice: result });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.delete('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const invoice = await Invoice.findOneAndRemove({ id }).exec().catch(e => { throw e });
        res.json({ id });
        emitScoket(req, socketType.DELETE_INVOICE, { id });
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.get('/', async (req, res) => {
    const sent = req.query.sent;
    const paid = req.query.paid;
    const limit = req.query.limit;
    const skip = req.query.skip;

    const invoiceQuery = {
        isVisibleOnAdminPage: true,
        isArchived: false
    };

    if (sent) {
        invoiceQuery.sent = sent;
    }

    if (paid) {
        invoiceQuery.paid = paid;
    }

    try {
        const result = await Invoice
            .find(invoiceQuery)
            .populate(invoiceTablePopulation)
            .limit(Number(limit))
            .skip(Number(skip))
            .lean()
            .exec()
            .catch(e => { throw e });
        res.json(result);

    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(e.message);
    }
});

router.get('/:id', common.ensureAuthenticated, async (req, res) => {
    const id = req.params.id;
    try {
        const invoice = await Invoice.findOne({ id }).exec().catch(e => { throw e });
        res.json(invoice);
    } catch (e) {
        console.log('Error in put scope/:id API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    }
});

router.post('/download', async (req, res) => {

    try {
        const {
            invoiceId,
            taskId,
            poRequired,
            includeContacts,
            contact,
            invoiceContact,
            billBranchFromOutlook,
            companyNameFromOutlookNotes,
            clientJobNo
        } = req.body;

        var customerNameForXL = billBranchFromOutlook.trim().length >= 1 ? billBranchFromOutlook : companyNameFromOutlookNotes;
        var population = [{
            path: 'scopes',
            populate: [{
                path: 'itemType'
            }]
        }, {
            path: 'teamMembers'
        }];
        var populationForScopes = [{
            path: 'lastModifiedBy'
        },
        {
            path: 'selectedScopes.scope',
            populate: [{
                path: 'itemType'
            },
            {
                path: 'engineerDetails.engineer'
            }
            ]
        }
        ];
        let invoice = await Invoice.findOne({ id: invoiceId }).populate(populationForScopes).exec().catch(e => { throw e });
        let task = await Task.findOne({ id: taskId }).populate(population).exec().catch(e => { throw e });
        if (!task || !invoice) {
            throw Error('task or invoice not found');
        }

        var wb = new xl.Workbook({
            defaultFont: {
                size: 10,
                name: 'Arial'
            }
        });
        var ws = wb.addWorksheet('Sheet 1', {
            margins: {
                left: 0.44,
                right: 0.44
            },
            pageSetup: {
                paperSize: 'A4_PAPER',
                scale: 80
            },
            printOptions: {
                centerHorizontal: true
            }
        });
        var alignCenter = wb.createStyle({
            alignment: {
                horizontal: 'center'
            }
        });

        var projectManager = _.find(task.teamMembers, function (teamMember) {
            return teamMember._id.toString() === task.createdBy.toString();
        });
        var cityState = task.city.toUpperCase() + ', ' + task.state.toUpperCase();
        var poNumber = poRequired === 'yes' ? invoice.poNumber ? invoice.poNumber : '' : 'N/A';
        var company = invoice.company ? invoice.company.toUpperCase() : '-';
        var hold = invoice.hold || '-';
        var contractorName = task.contractor.company ? task.contractor.company.trim() : task.contractor.name.trim();
        var jobNumber = task.isBidding ? 'B' + new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber + '-' + invoice.number :
            new Date(task.createdAt).getFullYear().toString().substr(2, 2) + '-' + task.taskNumber + '-' + invoice.number;
        var customerContactName;
        var description = '';
        var price = 0;
        var memo = task.city + ', ' + task.state + ' - ' + task.title;
        customerNameForXL = customerNameForXL.trim().length === 0 ? task.contractor.name.trim() : customerNameForXL.trim();
        var rowNumber = 1;
        ws.cell(rowNumber, 1).string('CUSTOMER').style(alignCenter);
        ws.cell(rowNumber, 2).string('CONTACT').style(alignCenter);
        ws.cell(rowNumber, 3).string('CLIENT JOB NO').style(alignCenter);
        ws.cell(rowNumber, 4).string('DATE').style(alignCenter);
        ws.cell(rowNumber, 5).string('TERMS').style(alignCenter);
        ws.cell(rowNumber, 6).string('INVOICE#').style(alignCenter);
        ws.cell(rowNumber, 7).string('QUANTITY').style(alignCenter);
        ws.cell(rowNumber, 8).string('P.O. NO.').style(alignCenter);
        ws.cell(rowNumber, 9).string('REP').style(alignCenter);
        ws.cell(rowNumber, 10).string('MEMO').style(alignCenter);
        ws.cell(rowNumber, 11).string('PROJECT').style(alignCenter);
        ws.cell(rowNumber, 12).string('CITY').style(alignCenter);
        ws.cell(rowNumber, 13).string('COMPANY').style(alignCenter);
        ws.cell(rowNumber, 14).string('DESCRIPTION').style(alignCenter);
        ws.cell(rowNumber, 15).string('ITEM TYPE').style(alignCenter);
        ws.cell(rowNumber, 16).string('AMOUNT').style(alignCenter);
        ws.cell(rowNumber, 17).string('HOLD').style(alignCenter);

        invoice.selectedScopes = invoice.selectedScopes.sort((scope1, scope2) => scope1.number > scope2.number);

        invoice.selectedScopes.forEach(selectedScope => {
            if (!selectedScope.scope.isArchived) {
                rowNumber += 1;
                customerContactName = selectedScope.scope.customerContact && selectedScope.scope.customerContact.name ? selectedScope.scope.customerContact.name : '';
                customerContactName = includeContacts === 'yes' ? contact : customerContactName;
                description = selectedScope.description;
                price = selectedScope.isPartial ? selectedScope.amount : selectedScope.scope.price;
                ws.cell(rowNumber, 1).string(customerNameForXL.toUpperCase()).style(alignCenter);
                ws.column(1).setWidth(customerNameForXL.length > 40 ? customerNameForXL.length : 40);
                ws.cell(rowNumber, 2).string(customerContactName).style(alignCenter);
                ws.column(2).setWidth(customerContactName.length > 25 ? customerContactName.length : 25);
                ws.cell(rowNumber, 3).string(clientJobNo).style(alignCenter);
                ws.column(3).setWidth(clientJobNo.length > 25 ? clientJobNo.length : 25);
                ws.cell(rowNumber, 4).string(moment(selectedScope.scope.dueDate).format('M/D/YYYY')).style(alignCenter);
                ws.cell(rowNumber, 5).string('Net 30').style(alignCenter);
                ws.cell(rowNumber, 6).string(jobNumber).style(alignCenter);
                ws.cell(rowNumber, 7).string('').style(alignCenter);
                ws.cell(rowNumber, 8).string(poNumber).style(alignCenter);
                ws.cell(rowNumber, 9).string(selectedScope.scope.engineerDetails.engineer.employeeCode).style(alignCenter);
                ws.cell(rowNumber, 10).string(memo).style(alignCenter);
                ws.column(10).setWidth(memo.length > 40 ? memo.length : 40);
                ws.cell(rowNumber, 11).string(task.title.toUpperCase()).style(alignCenter);
                ws.column(11).setWidth(task.title.length > 40 ? task.title.length : 40);
                ws.cell(rowNumber, 12).string(cityState).style(alignCenter);
                ws.column(12).setWidth(task.city.length > 25 ? task.city.length : 25);
                ws.cell(rowNumber, 13).string(company).style(alignCenter);
                ws.column(13).setWidth(company.length > 25 ? contractorName.length : 25);
                ws.cell(rowNumber, 14).string(description).style(alignCenter);
                ws.column(14).setWidth(description.length > 40 ? description.length : 40);
                ws.cell(rowNumber, 15).string(selectedScope.scope.itemType.name).style(alignCenter);
                ws.column(15).setWidth(selectedScope.scope.itemType.name.length > 40 ? selectedScope.scope.itemType.name.length : 40);
                ws.cell(rowNumber, 16).number(price).style(alignCenter);
                ws.cell(rowNumber, 17).string(hold).style(alignCenter);
            }
        });

        res.attachment('Invoice ' + jobNumber + '.xlsx');

        wb.write('Invoice ' + jobNumber + '.xlsx', res);

    } catch (e) {
        console.log('Error in post invoice/download API', e);
        res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
    };
});


router.post('/getHoldInvoices', common.ensureAuthenticated, async (req, res) => {
    const userID = req.body.userID;
    const userPopulation = [
        {
            path: 'role'
        }
    ];

    // const { role } = await User.findOne({ _id: userID }).populate(userPopulation).exec();
    // console.log(')))))', role, userID);

    // const isAdmin = role.id === 'HJFINQKHx';
    // const isAdmin = role.id === 'HJFINQKHx';

    const invoicePopulation = [{
        path: 'selectedScopes.scope',
        match: {
            isArchived: false
        },
        populate: [{
            path: 'task',
            populate: scopePopulationNew
        }]
    }];

    const holdInvoiceQuery = {
        $and: [{
            hold: 'Y'
        },
        {
            isArchived: false
        }
        ]
    };

    const completedInvoiceQuery = [{
        $match: {
            isArchived: false,
            group: {
                $eq: mongoose.Types.ObjectId(constants.TASK_GROUP__ID.COMPLETED_PROJECTS)
            }
        }
    },
    {
        $lookup: {
            'from': 'invoices',
            'localField': '_id',
            'foreignField': 'selectedScopes.scope',
            'as': 'selectedInInvoices'
        }
    },
    {
        $project: {
            updatedAt: 1,
            createdAt: 1,
            id: 1,
            note: 1,
            dueDate: 1,
            price: 1,
            group: 1,
            status: 1,
            itemType: 1,
            number: 1,
            task: 1,
            parent: 1,
            isArchived: 1,
            hourTrackers: 1,
            calcs: 1,
            drawings: 1,
            tabData: 1,
            letters: 1,
            agreements: 1,
            custDrawings: 1,
            definition: 1,
            managerDetails: 1,
            drafterDetails: 1,
            engineerDetails: 1,
            'selectedInInvoices.selectedScopes': 1,
            'selectedInInvoices.isArchived': 1,
            'selectedInInvoices._id': 1
        }
    }
    ];

    // if (!isAdmin) {
    completedInvoiceQuery[0].$match.$or = [{
        'engineerDetails.engineer': {
            $eq: mongoose.Types.ObjectId(userID)
        }
    },
    {
        'drafterDetails.drafter': {
            $eq: mongoose.Types.ObjectId(userID)
        }
    },
    {
        'managerDetails.manager': {
            $eq: mongoose.Types.ObjectId(userID)
        }
    }]
    // }

    async.parallel({
        holdInvoiceScopes: function (callBack) {
            Invoice.find(holdInvoiceQuery).populate(invoicePopulation).lean().exec(function (invoiceError, invoices) {
                if (!invoiceError) {
                    var scopes = [];
                    invoices.forEach(function (invoice) {
                        invoice.selectedScopes.forEach(function (selectedScope) {
                            if (String(selectedScope.scope.engineerDetails.engineer._id) === String(userID) ||
                                String(selectedScope.scope.managerDetails.manager._id) === String(userID) ||
                                (selectedScope.scope.engineerDetails.drafter &&
                                    String(selectedScope.scope.engineerDetails.drafter._id) === String(userID))
                                // || isAdmin
                            ) {
                                scopes.push(selectedScope.scope);
                            }
                        });
                    });
                    scopes = _.uniq(scopes);
                    callBack(null, scopes);
                } else {
                    callBack(invoiceError);
                }
            });
        },
        completedUnInvoiced: function (callBack) {
            Scope.aggregate(completedInvoiceQuery).exec(function (aggregateError, result) {
                if (!aggregateError) {
                    Scope.populate(result, scopePopulationNew, function (populateError, populatedScopes) {
                        if (!populateError) {
                            var copyOfAllScops = populatedScopes;
                            var pupulateScope = [];
                            if (populatedScopes.length > 0) {
                                populatedScopes.forEach(scope => {
                                    var totalBilled = 0;

                                    const childScopesOfCurrentScope = populatedScopes.filter(_scope => String(_scope.parent) === String(scope._id));

                                    [...scope.selectedInInvoices, ...childScopesOfCurrentScope].forEach(currentInvoice => {
                                        if (!currentInvoice.isArchived) {
                                            _.forEach(currentInvoice.selectedScopes, function (currentSelectedScope) {
                                                if (String(currentSelectedScope.scope._id) === String(scope._id)) {
                                                    if (currentSelectedScope.isPartial) {
                                                        totalBilled += Number(currentSelectedScope.amount);
                                                    } else {
                                                        totalBilled += Number(currentSelectedScope.scope.price !== currentSelectedScope.oldPrice ? currentSelectedScope.oldPrice : currentSelectedScope.scope.price);
                                                    }
                                                }
                                            });
                                        }
                                    });

                                    if (scope.price > totalBilled) {
                                        pupulateScope.push(scope);
                                    }
                                });
                            }

                            callBack(null, pupulateScope);
                        } else {
                            console.log('Pop Error', populateError);
                            callBack(populateError);
                        }
                    });
                    // callBack(null, result);
                } else {
                    console.log('Aggregate error', aggregateError);
                    callBack(aggregateError);
                }
            });
        }
    }, function (parallelError, allScopes) {
        if (!parallelError) {
            res.json(allScopes);
        } else {
            console.error(parallelError);
            res.status(constants.STATUS_CODES.INTERNAL_SERVER_ERROR).send(constants.RESPONSE_MESSAGES.ERROR);
        }
    });
});

export default router;
