export const defaultSelection = {
    id: 1
};

export const archiveSelection = {
    id: 1,
    isArchived: 1
};

export const scopeByGroupSelection = {
    id: 1,
    dueDate: 1,
    note: 1,
    isArchived: 1,
    status: 1,
    drafterDetails: 1,
    engineerDetails: 1,
    managerDetails: 1,
    group: 1,
    number: 1,
    task: 1,
    price: 1,
    hourtrackers: 1,
    highlights: 1
};
