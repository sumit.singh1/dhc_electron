import { OAuth2 } from 'oauth';
import { v4 } from 'uuid';
import { get } from 'axios';
import moment from 'moment'

import User from '../models/User';


export function generateSelection(body) {
    let select = {
        id: 1
    };
    for (let key in body) {
        select[key] = 1;
    }
    return select;
}

export function getTaskNumber({ createdAt, taskNumber, isBidding, isFromTaskGroup }) {
    if (isBidding) {
        return 'B' + new Date(createdAt).getFullYear().toString().substr(2, 2) + '-' + taskNumber;
    } else if (isFromTaskGroup) {
        return 'T' + new Date(createdAt).getFullYear().toString().substr(2, 2) + '-' + taskNumber;
    }

    return new Date(createdAt).getFullYear().toString().substr(2, 2) + '-' + taskNumber;
}

export function getTokenFromRefreshToken(refreshToken, callback) {
    const oauth2 = new OAuth2(
        process.env.OFFICE_CLIENT_ID,
        process.env.OFFICE_CLIENT_SECRET,
        'https://login.microsoftonline.com/common',
        '/oauth2/v2.0/authorize',
        '/oauth2/v2.0/token'
    );

    oauth2.getOAuthAccessToken(
        refreshToken,
        {
            grant_type: 'refresh_token',
            redirect_uri: process.env.APP_URL,
            response_mode: 'form_post',
            nonce: v4()
        },
        function (err, accessToken, token) {
            User.findOneAndUpdate({firstName: 'Jasper'}, {$set:{refreshToken: token}}, {new: true}, () => {
                callback(err, accessToken, token);
            });
        }
    );
}

export function hasAccessTokenExpired({ status, statusText }) {
    return status === 401 && statusText === 'Unauthorized';
}

export async function getOfficeContactDetails(id, req, res, callback) {
    const url = `https://graph.microsoft.com/v1.0/me/contacts/${id}`;
    let config = {
        headers: {
            Authorization: 'Bearer ' + req.cookies.OFFICE_ACCESS_TOKEN
        }
    };
    try {
        const { data } = await get(url, config);
        callback(null, data);
    } catch (e) {
        console.log(e)
        if (!hasAccessTokenExpired(e.response)) {
            return callback(e);
        }
        let refreshToken = req.cookies.OFFICE_REFRESH_TOKEN;
        getTokenFromRefreshToken(refreshToken, (refreshTokenError, accessToken, refreshToken) => {
            if (refreshTokenError) {
                return callback({ statusCode: STATUS_CODES.INVALID_TOKEN });
            }
            res.cookie(COOKIES.OFFICE_ACCESS_TOKEN, accessToken);
            res.cookie(COOKIES.OFFICE_REFRESH_TOKEN, refreshToken);

            config.headers.Authorization = 'Bearer ' + accessToken;

            get(url, config).then(({ data }) => {
                callback(null, data);
            }).catch(error => {
                callback(error);
            });
        })
    }
}

export function padWithZeroes(number, length) {
    let castedNumber = '' + number;
    while (castedNumber.length < length) {
        castedNumber = '0' + castedNumber;
    }
    return castedNumber;
}

export function formattedDate(date, timeZoneOffset, format) {
    let newDate = new Date(date);
    // timeZoneOffset = parseInt(timeZoneOffset)
    // return moment(date).zone(timeZoneOffset).format(format);
    // return moment(date).zone(timeZoneOffset).toDate();

    // newDate.setTime(newDate.getTime() + newDate.getTimezoneOffset(timeZoneOffset) * 60 * 1000);
    // console.log("formattedDate: " +  date, "timeZoneOffset: " + timeZoneOffset, "new Date" + newDate);
    // return newDate;
    if (date === '-' || date === null || !date) return '-'
    timeZoneOffset = parseInt(timeZoneOffset)
    return moment(date).isValid() && moment(date).zone(parseInt(timeZoneOffset)).format(format) || '-'
};