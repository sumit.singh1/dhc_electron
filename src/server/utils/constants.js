module.exports = {
    PERMISSIONS: {
        MANAGE_USER: {
            NAME: 'Manage User'
        },
        MANAGE_APP: {
            NAME: 'Manage App'
        },
        MANAGE_ACCESSIBILITY: {
            NAME: 'Manage Accessibility'
        },
        MANAGE_GROUP_STATUS: {
            NAME: 'Manage Group Status'
        },
        MANAGE_PERSONAL_UPDATE: {
            NAME: 'Manage Personal Update'
        },
        MANAGE_TASK: {
            NAME: 'Manage Task'
        }
    },
    STATUS_CODES: {
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500,
        INVALID_TOKEN: 498,
        CONFLICT: 409,
        ENTITY_ALREADY_EXIST: 422,
        SUCCESS: 200,
        ERROR: 301
    },
    REPORT_TYPES: {
        PRODUCTIVITY: 'productivity_report',
        BILLED_HOURS: 'billing_hours_report',
        TOTAL_HOURS: 'total_hours_report',
        TOTAL_HOURS_PTO: 'total_and_pto_report',
        DOLLAR_PER_BILLED_HOURS: 'dollar_per_billing_hour_report',
        DOLLAR_PER_TOTAL_HOURS: 'dollar_per_total_hour_report',
        DOLLAR_PER_TOTAL_HOURS_PTO: 'dollar_per_total_and_pto_report',
        PTO: 'pto_report',
        SALARY: 'salary_report',
        BONUS: 'bonus_report',
        SALARY_BONUS: 'salary_and_bonus_report',
        TOTAL_BILLING_DOLLARS: 'total_billing_dollars',
        DOLLAR_PER_HOUR: 'dollar_per_hour'
    },
    REPORT_ERROR: {
        INVALID_ARGUMENTS: 9001,
        USER_ERROR: 9002,
        SCOPE_ERROR: 9003,
        USER_NOT_FOUND: 9004,
        USER_ROLE_NOT_FOUND: 9005,
        UNAUTHORIZED: 9006
    },
    ERROR_MSG: {
        INVALID_ARGUMENTS: 'Invalid or missing mandatory input arguments.',
        UNKNOWN_ERROR: 'Something went wrong.',
        INTERNAL_SERVER_ERROR: 'Internal server error.',
        USER_NOT_FOUND: 'User not found.',
        USER_ROLE_NOT_FOUND: 'User role is not found.',
        UNAUTHORIZED: 'Access denied.'
    },
    RESPONSE_MESSAGES: {
        INTERNAL_SERVER_ERROR: 'Internal Server Error',
        INVALID_TOKEN: 'Invalid tokn',
        FORBIDDEN: 'Forbidden: Access is denied',
        ERROR: 'The server encountered an error processing the request. Please try again, Sorry for the trouble.',
        APP_SETTINGS: {
            NOT_FOUND: 'App setting not found'
        },
        USER: {
            UNAUTHORIZED: 'User is unauthorized',
            DUPLICATE: 'User already present',
            INCORRECT_PASSWORD: 'Old password is incorrect',
            NOT_FOUND: 'User is not present',
            INVALID_PASSWORD: 'Wrong password',
            RESET_PASSWORD_EXPIRED: 'Password reset token is invalid or has expired',
            SAME_PASSWORD: 'Old password and new password should not be same',
            SCOPES: 'Can not fetch scopes of user',
            GROUP_SCOPES: 'Can not fetch group scopes',
            INVOICE: 'Can not fetch invoices',
            SUCCESS: 'User added successfully.'

        },
        OFFICE_365: {
            TOKEN_NOT_FOUND: 'Please log in to office 365'
        },
        SLACK: {
            TOKEN_NOT_FOUND: 'Please log in to slack'
        },
        TASK: {
            ALREADY_EXIST: 'Task name already exist',
            NOT_FOUND: 'Task not found',
            UPDATE_ERROR: 'Task can not be updated'
        },
        SCOPE: {
            NOT_FOUND: 'Scope not found',
            CANT_POPULATE: 'Scope can not be populated',
            CANT_SAVE: 'Scope can not be saved',
            MAX_LIMIT: 'Exceeded max number of scope limit in one project. Please consider adding new Project/Task instead.'
        },
        DRAWING: {
            CANT_SAVE: 'Drawing can not be saved'
        },
        CALC: {
            CANT_SAVE: 'Calc can not be saved'
        },
        PURCHASE_ORDER: {
            NOT_FOUND: 'No purchase order found'
        },
        AGREEMENT: {
            NOT_FOUND: 'No purchase order found'
        },
        ATTACHMENT_UPLOAD: {
            REQUEST_TIMEOUT: 'Request timeout. Please try uploading file with high speed network or upload a file having small size.'
        },
        REFRESH_PAGE: 'Something went wrong!! Please refresh the page.',
        DATE_UPDATE_ERROR: 'Unable to update date. Please try again after sometime.',
        CIENT_CONNECTION_ERROR: 'Unable to connect to FTP server at this point. Please try again after sometime.',
        CREATE_PDF: 'Unable to create pdf at this time. Please try after sometime.',
        NO_DATA_FOUND: 'Unable to create pdf. Please fill timesheet and try again.',
        NO_USER_FOUND: 'Unable to find user. Please try after sometime.'

    },
    ROLES: {
        ADMIN: 'Administrator'
    },
    ACCESS_LEVELS: {
        READ: 'read',
        WRITE: 'write'
    },
    COOKIES: {
        AUTH_TOKEN: 'AUTH_TOKEN',
        OFFICE_ACCESS_TOKEN: 'OFFICE_ACCESS_TOKEN',
        OFFICE_REFRESH_TOKEN: 'OFFICE_REFRESH_TOKEN',
        SLACK_ACCESS_TOKEN: 'SLACK_ACCESS_TOKEN'
    },
    SOCKET_EVENTS: {
        SCOPE_UPDATE: 'scope update',
        REV_SCOPE_CREATE: 'rev scope create',
        TASK_UPDATE: 'task update',
        USER_UPDATE: 'user update',
        MANAGER_UPDATE: 'manager update',
        TASK_CREATE: 'task create',
        ROLE_UPDATE: 'role update',
        HOURTRACKER_UPDATE: 'hourtracker update',
        MILESTONE_UPDATE: 'milestone update',
        MILESTONE_CREATE: 'milestone create',
        SCOPE_TEMPLATE_UPDATE: 'scope template update',
        ITEM_TYPE_CREATE: 'itemtype create',
        ITEM_TYPE_UPDATE: 'itemtype update',
        USER_SINGIN: 'user singin',
        BID_SCOPE_UPDATE: 'bid scope update',
        TASK_COMMENT_ADD: 'task comment create',
        TASK_COMMENT_UPDATE: 'task comment update',
        TASK_COMMENT_DELETE: 'task comment delete',
        UPDATE_USER_TOTAL_HOURS: 'update other users total urgent and non-urgent hours'
    },

    DOCUMENT_TYPES: {
        PURCHASE_ORDER: 'PO',
        CUSTOMER_SERVICE_AGREEMENT: 'CSA',
        MODIFIED_CUSTOMER_SERVICE_AGREEMENT: 'MCSA',
        MASTER_AGREEMENT: 'MA',
        CLIENT_AGREEMENT: 'CA',
        INVOICE: 'Invoice',
        DRAWING: 'Drawing',
        CALC: 'Calc',
        SCOPE_INVOICE: 'Scope Invoice',
        CUST_DRAWING: 'Cust Drawing',
        LETTER: 'Letter',
        TAB_DATA: 'Tab Data'
    },
    TASK_GROUP_ID: {
        ACTIVE_PROJECTS: 'BkeEu11Jb',
        ON_HOLD: 'H1shXWJyW',
        TASKS: 'B1tKksrJb',
        BIDS: 'rkndUO_kW',
        COMPLETED_PROJECTS: 'HJVcLOOyb'
    },
    TASK_GROUP__ID: {
        ACTIVE_PROJECTS: '58bed83e62b976001126f7a6',
        ON_HOLD: '58bed84862b976001126f7a7',
        TASKS: '58bed84f62b976001126f7a8',
        BIDS: '590241226a30630011b82056',
        COMPLETED_PROJECTS: '5907701d219b330011d09bca'
    },
    DRAWING_TEMPLATES: [
        {
            title: 'In drafting',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Draft complete',
            isDone: false,
            isAttachment: false
        }, {
            title: 'To customer for review',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Customer comments',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Approved by customer',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Submitted',
            isDone: false,
            isAttachment: false
        }
    ],
    CALC_TEMPLATES: [
        {
            title: 'In progress',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Completed',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Stamped',
            isDone: false,
            isAttachment: false
        }, {
            title: 'Submitted',
            isDone: false,
            isAttachment: false
        }
    ],
    TABLE_HEADERS: [
        {
            title: 'Team',
            sort: ''
        },
        {
            title: 'Contractor',
            sort: 'contractor'
        },
        {
            title: 'Job #',
            sort: 'invoiceNumber'
        },
        {
            title: 'Scope',
            sort: ''
        },
        {
            title: 'Project',
            sort: 'projectName'
        },
        {
            title: 'City',
            sort: 'city'
        },
        {
            title: 'State',
            sort: 'state'
        },
        {
            title: 'Scope Note',
            sort: ''
        },
        {
            title: 'Status',
            sort: 'engineerDetails.status'
        },
        {
            title: 'Due Date',
            sort: 'dueDate'
        },
        {
            title: 'Cost',
            sort: ''
        },
        {
            title: 'Urgent Hours',
            sort: 'engineerDetails.urgentHours'
        },
        {
            title: 'Non-Urgent Hours',
            sort: ''
        },
        {
            title: 'CSA',
            sort: ''
        },
        {
            title: 'PO',
            sort: ''
        },
        {
            title: 'INV',
            sort: ''
        },
        /* {
            title: 'Final Engineering Hours',
            sort: ''
        },*/
        {
            title: 'Calc',
            sort: ''
        }
    ],
    ROLE_ID: {
        ENGINEER: 'BkZIrmetl',
        DRAFTER: 'SJiAcXM8x',
        MANAGER: 'H1xYR4VYx',
        CUSTOMER: 'C0C0aQYUV9FcOQvv1XO',
        ADMIN: 'HJFINQKHx'
    },
    CUSTOMER: {
        ROLE: '5b9125eb499fd200144e6ad4',
        ROLE_LEVEL: '5b9125fe499fd200144e6ad5'
    }
};
