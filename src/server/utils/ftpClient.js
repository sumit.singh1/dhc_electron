import Client from 'promise-ftp';

export function getClient() {
    let client = new Client();
    const config = {
        host: process.env.FTP_HOST_NAME,
        user: process.env.FTP_USER,
        password: process.env.FTP_PASSWORD,
        port: 21,
        secure: true,
        secureOptions: {
            rejectUnauthorized: false
        }
    };
    return client.connect(config).then(() => client);
}

export function uploadFile(client, source, destination) {
    return client.put(source, destination);
}

export async function downloadFile(client, source) {
    source = process.env.FTP_BASE_FOLDER + source;
    client.get(source)
        .then(stream =>
            new Promise((resolve, reject) => {
                stream.once('close', resolve);
                stream.once('error', reject);
                stream.pipe(fs.createWriteStream('foo.local-copy.txt'));
            })
        );
}

export function deleteFile(client, path) {
    return client.delete(process.env.FTP_BASE_FOLDER + path);
}

export async function getOrCreateFolder(client, path, folderName) {
    const newPath = `${path}/${folderName}`;
    const list = await client.list(path).catch(e => { throw e });
    const exist = list.findIndex(entity => entity.type === 'd' && entity.name === `${folderName}`);
    if (exist > -1) return null;
    return client.mkdir(newPath);
}

export async function rename(client, oldPath, newPath) {
    return client.rename(process.env.FTP_BASE_FOLDER + oldPath, process.env.FTP_BASE_FOLDER + newPath);
}

export function destroyClient(client) {
    client.end();
}