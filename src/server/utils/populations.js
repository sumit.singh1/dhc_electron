// ALL population varibles are added here

export function genetatePopulation(body) {
    const population = [];
    for (let key in body) {
        switch (key) {
            case 'group':
                population.push(groupPath);
                break;
            case 'managerDetails':
                population.push(managerPopulation);
                break;
            case 'engineerDetails':
                population.push(engineerPopulation);
                break;
            case 'drafterDetails':
                population.push(drafterPopulation);
                break;
            case 'itemType':
                population.push({ path: 'itemType' })
                population.push(scopeKeywordPopulation);
                break;
            default: break;
        }
    }
    return population;
}

// ***************** Group *********** //
const groupPath = {
    path: 'group'
};

// ***************** Group *********** //
const hourTrackerPath = {
    path: 'hourTrackers'
};

// *******   HourTracker Emp ********* //
const hourTrackerEmployee = {
    path: 'hourTrackers',
    populate: [
        {
            path: 'employee',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode'
        }
    ]
};

// ************ drawings  ************* //
const drawings = {
    path: 'drawings',
    match: {
        isArchived: false
    }
};

// ************ custDrawings  ************* //
const custDrawings = {
    path: 'custDrawings',
    match: {
        isArchived: false
    }
};

// ************ scopePlanning  ************* //
export const scopePlannings = {
    path: 'scopePlannings',
    match: {
        isArchived: false
    }
};


// ************ calcs  ************* //
const calcs = {
    path: 'calcs',
    match: {
        isArchived: false
    }
};

// ************ letters  ************* //
const letters = {
    path: 'letters',
    match: {
        isArchived: false
    }
};

// ************ tabData  ************* //
const tabData = {
    path: 'tabData',
    match: {
        isArchived: false
    }
};

// *********** Selected Scope ************* //

const scopeKeywordPopulation = {
    path: 'scopeKeywords',
    populate: [{
        path: 'customKeyword.keyword'
    }, {
        path: 'itemTypeKeywords.keyword'
    }]
};
// *********** Selected Scope ************* //

const selectedScopes = {
    path: 'selectedScopes',
    select: '_id id number note isArchived'
};

// ************ Aggrement ************ //
const aggrementPopulation = {
    path: 'agreements',
    match: {
        isArchived: false
    },
    populate: [selectedScopes],
    select: '_id id number note isArchived selectedScopes templates'
};

// ************ Modified Aggrement *********** //
const modifiedAgreementPopulation = {
    path: 'modifiedAgreements',
    match: {
        isArchived: false
    },
    populate: [selectedScopes],
    select: '_id id number note isArchived selectedScopes templates'
};

// ********* Master Agreement *************** //
const masterAgreementPopulation = {
    path: 'masterAgreements',
    match: {
        isArchived: false
    },
    populate: [selectedScopes],
    select: '_id id number note isArchived selectedScopes templates'
};

// ********* Client Agreement *************//
const clientAgreementPopulation = {
    path: 'clientAgreements',
    match: {
        isArchived: false
    },
    populate: [selectedScopes],
    select: '_id id number note isArchived selectedScopes templates'
};

// *********** Purchase Order ************* //
const purchaseOrderPopulation = {
    path: 'purchaseOrders',
    match: {
        isArchived: false
    },
    populate: [selectedScopes],
    select: '_id id number note isArchived selectedScopes templates'
};

// *************** Invoice *************** //
const invoicePopulation = {
    path: 'invoices',
    match: {
        isArchived: false
    },
    populate: [selectedScopes],
    select: '_id id number note isArchived selectedScopes templates hold'
};

// ************** Team Memebers ***************** //
const teamMembersPopulation = {
    path: 'teamMembers',
    select: 'firstName lastName _id email id role picture employeeCode'
};

// ************ Task ***************** //
const taskPopulation = {
    path: 'task',
    populate: [
        aggrementPopulation,
        modifiedAgreementPopulation,
        masterAgreementPopulation,
        clientAgreementPopulation,
        purchaseOrderPopulation,
        invoicePopulation,
        teamMembersPopulation
    ]
};

// ************** engineerPopulation      *****************//
const engineerPopulation = {
    path: 'engineerDetails.engineer',
    populate: [{
        path: 'role'
    }],
    select: 'firstName lastName _id email id role picture employeeCode scopeHighlights'
};

// ************** drafterPopulation      *****************//
const drafterPopulation = {
    path: 'drafterDetails.drafter',
    populate: [{
        path: 'role'
    }],
    select: 'firstName lastName _id email id role picture employeeCode'
};

// ************** managerPopulation      *****************//
const managerPopulation = {
    path: 'managerDetails.manager',
    populate: [{
        path: 'role'
    }],
    select: 'firstName lastName _id email id role picture employeeCode'
};

// ******** Emploee ********* //
const employeePopulation = {
    path: 'employee',
    select: 'id _id firstName lastName email role picture employeeCode signature'
};

const employeeRolePopulation = {
    path: 'employee',
    populate: [{
        path: 'role'
    }],
    select: 'id _id firstName lastName email role picture'
};

// ********** USER ***********//
export const userRoleLocationPopulation = [
    {
        path: 'role'
    }, {
        path: 'locationInfo.location'
    }, {
        path: 'roleLevel'
    }
];


// ****************   HOURTRACKER *******************//
export const hourTrackerPopulation = [employeeRolePopulation];

export const hourTrackerEmployeePopulation = [employeePopulation];

export const hourTrackerScopePopulation = [
    groupPath,
    hourTrackerPath
];

export const hourTrackerScopeGroupPopulation = [groupPath];


// ****************** SCOPE ****************  //

export const scopePopulation = [
    taskPopulation,
    groupPath,
    engineerPopulation,
    drafterPopulation,
    managerPopulation,
    hourTrackerEmployee,
    {
        path: 'milestone.templates.metaData'
    },
    {
        path: 'itemType'
    },
    drawings,
    custDrawings,
    scopePlannings,
    calcs,
    letters,
    tabData,
    scopeKeywordPopulation
];

export const scopePopulationNew = [
    taskPopulation,
    groupPath,
    engineerPopulation,
    drafterPopulation,
    managerPopulation,
    hourTrackerEmployee,
    calcs,
    { path: 'itemType' },
    scopeKeywordPopulation,

];

export const customeScopePopulation = [
    engineerPopulation,
    drafterPopulation,
    managerPopulation,
    // hourTrackerEmployee,
    {
        path: 'task',
        select: 'id createdBy isBidding isFromTaskGroup createdAt taskNumber contractor title'
    },
    {
        path: 'group'
    }
];

export const scopeOfTaskIDPopulation = {
    engineerPopulation,
    drafterPopulation,
    managerPopulation,
    hourTrackerEmployee
};

export const scopeTaskIDPopulation = {
    engineerPopulation,
    drafterPopulation,
    managerPopulation,
    hourTrackerEmployee,
    drawings,
    custDrawings,
    scopePlannings,
    calcs,
    letters,
    tabData
};

// &&&&&&&&&&&&&&&&&&&&&&  TASK get task API &&&&&&&&&&&&&&&&&&&&&&&&&& //
export const getTaskPopulation = [
    {
        path: 'agreements',
        populate: [{
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }]
    },
    {
        path: 'modifiedAgreements',
        populate: [{
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }]
    },
    {
        path: 'masterAgreements',
        populate: [{
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }]
    },
    {
        path: 'clientAgreements',
        populate: [{
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }]
    },
    {
        path: 'purchaseOrders',
        populate: [{
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }]
    },
    {
        path: 'invoices',
        populate: [
            {
                path: 'selectedScopes.scope',
                select: '_id id number note price isArchived definition parent'
            }
        ]
    },
    {
        path: 'teamMembers',
        populate: [{
            path: 'role',
            select: 'id _id name'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    },
    {
        path: 'comments.postedBy',
        select: 'firstName lastName _id id role picture employeeCode'
    },
    {
        path: 'scopes',
        populate: [{
            path: 'group'
        }, {
            path: 'engineerDetails.engineer',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }, {
            path: 'drafterDetails.drafter',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }, {
            path: 'managerDetails.manager',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode email'
        }, {
            path: 'milestone.templates.metaData'
        }, {
            path: 'itemType'
        },
            hourTrackerEmployee,
        {
            path: 'drawings'
        }, {
            path: 'custDrawings'
        }, {
            path: 'scopePlannings'
        }, {
            path: 'calcs'
        }, {
            path: 'letters'
        }, {
            path: 'tabData'
        },
            scopeKeywordPopulation
        ]
    }
];


export const updateTaskPopulation = [{
    path: 'teamMembers',
    populate: [{
        path: 'role'
    }],
    select: 'firstName lastName _id id role picture employeeCode'
}, {
    path: 'scopes',
    populate: [{
        path: 'group'
    }, {
        path: 'task',
        populate: [
            {
                path: 'purchaseOrders',
                match: {
                    isArchived: false
                }
            },
            // {
            //     path: 'agreements',
            //     match: {
            //         isArchived: false
            //     }
            // },
            {
                path: 'invoices',
                match: {
                    isArchived: false
                }
            }, {
                path: 'teamMembers',
                populate: [{
                    path: 'role'
                }],
                select: 'firstName lastName _id id role picture employeeCode'
            }
        ]
    }, {
        path: 'engineerDetails.engineer',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    }, {
        path: 'drafterDetails.drafter',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    }, {
        path: 'managerDetails.manager',
        populate: [{
            path: 'role'
        }],
        select: 'firstName lastName _id id role picture employeeCode'
    }, {
        path: 'milestone.templates.metaData'
    }, {
        path: 'itemType'
    }, {
        path: 'hourTrackers',
        populate: [{
            path: 'employee',
            populate: [{
                path: 'role'
            }],
            select: 'firstName lastName _id id role picture employeeCode'
        }]
    }, {
        path: 'drawings'
    }, {
        path: 'custDrawings'
    }, {
        path: 'scopePlannings'
    }, {
        path: 'calcs'
    }, {
        path: 'letters'
    }, {
        path: 'tabData'
    }]
}, {
    path: 'comments.postedBy'
}, {
    path: 'purchaseOrders',
    populate: [
        {
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }
    ]
}, {
    path: 'agreements',
    populate: [
        {
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }
    ]
}, {
    path: 'modifiedAgreements',
    populate: [
        {
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }
    ]
}, {
    path: 'masterAgreements',
    populate: [
        {
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }
    ]
}, {
    path: 'clientAgreements',
    populate: [
        {
            path: 'selectedScopes',
            select: '_id id number note isArchived'
        }
    ]
}, {
    path: 'invoices',
    populate: [
        {
            path: 'lastModifiedBy',
            select: 'firstName lastName _id id role picture employeeCode'
        },
        {
            path: 'selectedScopes.scope',
            select: '_id id number note price isArchived definition'
        }
    ]
}];

export const invoiceTablePopulation = [{
    path: 'selectedScopes.scope',
    populate: [{
        path: 'task',
        match: {
            isBidding: false
        },
        // populate: [{
        //     path: 'teamMembers',
        //     select: 'firstName lastName _id id role picture employeeCode'
        // }],
        select: 'id _id taskNumber title createdBy createdAt contractor city state'
    }
        // {
        //     path: 'group'
        // }

    ],
    select: 'id _id price task note number definition isArchived dueDate managerDetails parent'
    // options: { limit: 1 }
}
    // {
    //     path: 'lastModifiedBy',
    //     populate: [{
    //         path: 'role'
    //     }],
    //     select: 'firstName lastName _id id role picture employeeCode'
    // }
];

export const selectedScopePopulation = {
    path: 'selectedScopes',
    select: 'id isArchived note number _id'
};

export const invoiceSelectedScopePopulation = {
    path: 'selectedScopes.scope',
    select: 'id isArchived note number _id defination price'
};

export const itemTypePopulation = {
    path: 'keywords',
    select: 'id title _id'
};
