import Counter from '../models/Counter';
import RevScopeCounter from '../models/RevScopeCounter';
import TaskCounter from '../models/TaskCounter';

export function getCounter(taskID) {
    return Counter.findOne({ taskID: taskID }).exec();
}

export function createCounter(taskID) {
    return (new Counter({ taskID })).save();
}

export function incrementRevScopeCounter(scopeID) {
    return RevScopeCounter.findOneAndUpdate({ scopeID }, { $inc: { nextRev: 1 } }, { new: true, upsert: true, select: { nextRev: 1 } }).exec();
}

export function incrementScopeCounter(counter, inc) {
    if (counter.scopeNumber === 25) {
        throw 'scope limit exceed';
    }
    if (counter.scopeNumber === 7 || counter.scopeNumber === 13) {
        counter.scopeNumber += inc + 1;
    } else {
        counter.scopeNumber += inc;
    }
    return Counter.findOneAndUpdate({ _id: counter._id }, counter).exec();
}

export function incrementPOCounter(taskID) {
    return Counter.findOneAndUpdate({ taskID }, { $inc: { po: 1 } }, { new: true, upsert: true, select: { po: 1 } }).exec();
}

export function incrementInvoiceCounter(taskID) {
    return Counter.findOneAndUpdate({ taskID }, { $inc: { invoice: 1 } }, { new: true, upsert: true, select: { invoice: 1 } }).exec();
}

export function incrementAgreementCounter(taskID) {
    return Counter.findOneAndUpdate({ taskID }, { $inc: { agreement: 1 } }, { new: true, upsert: true, select: { agreement: 1 } }).exec();
}

export function incrementModifiedAgreementCounter(taskID) {
    return Counter.findOneAndUpdate({ taskID }, { $inc: { modifiedAgreement: 1 } }, { new: true, upsert: true, select: { modifiedAgreement: 1 } }).exec();
}

export function incrementMasterAgreementCounter(taskID) {
    return Counter.findOneAndUpdate({ taskID }, { $inc: { masterAgreement: 1 } }, { new: true, upsert: true, select: { masterAgreement: 1 } }).exec();
}

export function incrementClientAgreementCounter(taskID) {
    return Counter.findOneAndUpdate({ taskID }, { $inc: { clientAgreement: 1 } }, { new: true, upsert: true, select: { clientAgreement: 1 } }).exec();
}

export async function incrementBidNumberCounter() {
    const { nextBiddingTask } = await TaskCounter.findByIdAndUpdate('task', { $inc: { nextBiddingTask: 1 } }, { new: true, upsert: true, select: { nextBiddingTask: 1 } }).exec();
    return nextBiddingTask
}

export async function incrementTaskGroupNumberCounter() {
    const { nextTaskGroupTask } = await TaskCounter.findByIdAndUpdate('task', { $inc: { nextTaskGroupTask: 1 } }, { new: true, upsert: true, select: { nextTaskGroupTask: 1 } }).exec();
    return nextTaskGroupTask
}

export async function incrementTaskNumberCounter() {
    const data = await TaskCounter.findByIdAndUpdate('task', { $inc: { nextTask: 1 } }, { new: true, upsert: true, select: { nextTask: 1 } }).exec();
    return data.nextTask
}
