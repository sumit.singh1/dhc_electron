const { body } = require('express-validator/check');

export const isNumeric = (value) => {
    return !isNaN(parseFloat(value)) && isFinite(value);
};

export const escapeRegExpresson = (str) => {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
};

export const validate = (method) => {
    switch (method) {
    case 'filtered' : {
        return [
            body('fromYear', 'from year doesn`t exists').exists(),
            body('toYear', 'to year doesn`t exists').exists(),
            body('contractor', 'Invalid contractor').optional().isArray(),
            body('itemType', 'invalid item type id').optional().isArray(),
            body('scopeKeywords', 'invalid scope keywords').optional().isArray(),
            body('group', 'group ids doesn`nt exists').exists().isArray(),
            body('cities', 'invalid cities').optional().isArray(),
            body('states', 'invalid states').optional().isArray(),
            body('engineers', 'invalid engineers').optional().isArray(),
            body('drafters', 'invalid drafters').optional().isArray(),
            body('managers', 'invalid managers').optional().isArray()
        ];
    }

    default: {
        return [];
    }
    }
};


export const validationHandler = next => result => {
    if (result.isEmpty()) {
        return;
    }
    if (!next) {
        throw new Error(
            result.array().map(i => `'${i.param}': ${i.msg}`).join(' ')
        );
    }
    return next(
      new Error(
            result.array().map(i => `'${i.param}': ${i.msg}`).join('')
        )
    )
};
