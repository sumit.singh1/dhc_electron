import find from 'lodash/find';

export function emitScoket(req, type, result) {
    const io = req.app.get('socketio');
    const socket = find(io.sockets.sockets, currentSocket => currentSocket.id === req.headers.socketid);
    if (socket) {
        socket.broadcast.emit(type, result);
    }
}

export const socketType = {
    ARCHIVE_SCOPE: 'archive_scope',
    UPDATE_SCOPE: 'update_scope',
    UPDATE_SCOPE_HIGHLIGHTS: 'update_scope_highlights',
    ADD_SCOPE_MILESTONE: 'add_scope_milestone',
    ADD_SCOPE: 'add_scope',
    ADD_HOUR_TRACKER_TO_SCOPE: 'add_hourtracker_to_scope',
    REMOVE_HOUR_TRACKER_FROM_SCOPE: 'remove_hourtracker_from_scope',

    ARCHIVE_DRAWING: 'archive_drawing',
    UPDATE_DRAWING: 'update_drawing',
    ADD_DRAWING: 'add_drawing',
    DELETE_DRAWING: 'delete_drawing',

    ARCHIVE_CUST_DRAWING: 'archive_cust_drawing',
    UPDATE_CUST_DRAWING: 'update_cust_drawing',
    ADD_CUST_DRAWING: 'add_cust_drawing',
    DELETE_CUST_DRAWING: 'delete_cust_drawing',

    ARCHIVE_CALC: 'archive_calc',
    UPDATE_CALC: 'update_calc',
    ADD_CALC: 'add_calc',
    DELETE_CALC: 'delete_calc',

    UPDATE_CUSTOM_SCOPE_KEYWORD: 'update_custom_scope_keyword',
    UPDATE_SCOPE_KEYWORD: 'update_scope_keyword',
    ADD_SCOPE_KEYWORD: 'add_scope_keyword',
    DELETE_SCOPE_KEYWORD: 'delete_scope_keyword',

    ARCHIVE_SCOPE_PLANNING: 'archive_scope_planning',
    UPDATE_SCOPE_PLANNING: 'update_scope_planning',
    ADD_SCOPE_PLANNING: 'add_scope_planning',
    DELETE_SCOPE_PLANNING: 'delete_scope_planning',


    ARCHIVE_INVOICE: 'archive_invoice',
    UPDATE_INVOICE: 'update_invoice',
    ADD_INVOICE: 'add_invoice',
    DELETE_INVOICE: 'delete_invoice',

    ARCHIVE_TAB_DATA: 'archive_tab_data',
    UPDATE_TAB_DATA: 'update_tab_data',
    ADD_TAB_DATA: 'add_tab_data',
    DELETE_TAB_DATA: 'delete_tab_data',

    ARCHIVE_LETTER: 'archive_letter',
    UPDATE_LETTER: 'update_letter',
    ADD_LETTER: 'add_letter',
    DELETE_LETTER: 'delete_letter',

    ARCHIVE_PURCHASE_ORDER: 'archive_purchase_order',
    UPDATE_PURCHASE_ORDER: 'update_purchase_order',
    ADD_PURCHASE_ORDER: 'add_purchase_order',
    DELETE_PURCHASE_ORDER: 'delete_purchase_order',

    ADD_TASK: 'add_task',
    ADD_TASK_SCOPE: 'add_task_scope',
    ADD_TASK_AGREEMENT: 'add_task_agreement',
    ADD_TASK_PURCHASE_ORDER: 'add_task_purchase_order',
    ADD_TASK_MODIFIED_AGREEMENT: 'add_task_modified_agreement',
    ADD_TASK_MASTER_AGREEMENT: 'add_task_master_agreement',
    ADD_TASK_CLIENT_AGREEMENT: 'add_task_client_agreement',
    ADD_TASK_COMMENT: 'add_task_comment',
    UPDATE_TASK_COMMENT: 'update_task_comment',
    DELETE_TASK_COMMENT: 'delete_task_comment',
    UPDATE_TASK: 'update_task',
    UPDATE_TASK_TAGS: 'update_task_tags',

    ADD_TAG: 'add_tag',

    UPDATE_HOUR_TRACKER: 'update_hour_tracker',
    ADD_HOUR_TRACKER: 'add_hour_tracker',
    DELETE_HOUR_TRACKER: 'delete_hour_tracker',

    ARCHIVE_AGREEMENT: 'archive_agreement',
    UPDATE_AGREEMENT: 'update_agreement',
    ADD_AGREEMENT: 'add_agreement',
    DELETE_AGREEMENT: 'delete_agreement',

    ARCHIVE_MODIFIED_AGREEMENT: 'archive_modified_agreement',
    UPDATE_MODIFIED_AGREEMENT: 'update_modified_agreement',
    ADD_MODIFIED_AGREEMENT: 'add_modified_agreement',
    DELETE_MODIFIED_AGREEMENT: 'delete_modified_agreement',

    ARCHIVE_MASTER_AGREEMENT: 'archive_master_agreement',
    UPDATE_MASTER_AGREEMENT: 'update_master_agreement',
    ADD_MASTER_AGREEMENT: 'add_master_agreement',
    DELETE_MASTER_AGREEMENT: 'delete_master_agreement',

    ARCHIVE_CLIENT_AGREEMENT: 'archive_client_agreement',
    UPDATE_CLIENT_AGREEMENT: 'update_client_agreement',
    ADD_CLIENT_AGREEMENT: 'add_client_agreement',
    DELETE_CLIENT_AGREEMENT: 'delete_client_agreement'
};
