import Agreement from '../models/Agreement';
import Calc from '../models/Calc';
import ClientAgreement from '../models/ClientAgreement';
import CustDrawing from '../models/CustDrawing';
import Drawing from '../models/Drawing';
import Invoice from '../models/Invoice';
import Letter from '../models/Letter';
import MasterAgreement from '../models/MasterAgreement';
import ModifiedAgreement from '../models/ModifiedAgreement';
import PurchaseOrder from '../models/PurchaseOrder';
import Scope from '../models/Scope';
import TabData from '../models/TabData';
import ItemType from '../models/ItemType';
import ScopeKeyword from '../models/ScopeKeyword';
import Task from '../models/Task';
import Keyword from '../models/Keyword'
import ScopePlanning from '../models/ScopePlanning';
import {
    incrementAgreementCounter,
    incrementClientAgreementCounter,
    incrementInvoiceCounter,
    incrementMasterAgreementCounter,
    incrementModifiedAgreementCounter,
    incrementPOCounter,
} from './counterUtils';

export function createCalc(number = 0) {
    const data = {
        number,
        templates: [
            {
                title: 'In progress',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Completed',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Stamped',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Submitted',
                isDone: false,
                isAttachment: false
            }
        ]
    };
    return (new Calc(data)).save();
}

export async function createKeyword(obj) {
    return (new Keyword(obj)).save();
}

export function createScopePlanning(number = 0) {
    const data = {
        number,
        templates: [
            {
                title: 'Research',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Planning',
                isDone: false,
                isAttachment: false
            }, {
                title: 'In Progress',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Draft To PM',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Complete',
                isDone: false,
                isAttachment: false
            }
        ]
    };
    return (new ScopePlanning(data)).save();
}

export function createDrawing(number = 0) {
    const data = {
        number,
        templates: [
            {
                title: 'In drafting',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Draft complete',
                isDone: false,
                isAttachment: false
            }, {
                title: 'To customer for review',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Customer comments',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Approved by customer',
                isDone: false,
                isAttachment: false
            }, {
                title: 'Submitted',
                isDone: false,
                isAttachment: false
            }
        ]
    };
    return (new Drawing(data)).save();
}

export function createCustDrawing(number = 0) {
    const data = {
        number,
        templates: [
            {
                title: 'In drafting',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Draft complete',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'To customer for review',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Customer comments',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Approved by customer',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Submitted',
                isDone: false,
                isAttachment: false
            }
        ]
    };
    return (new CustDrawing(data)).save();
}

export function createLetter(number = 0) {
    const data = {
        number,
        templates: [
            {
                title: 'In progress',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Completed',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Stamped',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Submitted',
                isDone: false,
                isAttachment: false
            }
        ]
    };
    return (new Letter(data)).save();
}

export function createTabData(number = 0) {
    const data = {
        number,
        templates: [
            {
                title: 'In Progress',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Complete',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Submitted',
                isDone: false,
                isAttachment: false
            }
        ]
    };
    return (new TabData(data)).save();
}

export async function createScopeKeyword(itemTypeId) {

    const itemType = await ItemType.findById(itemTypeId).populate({ path: 'keywords' }).exec();

    const customKeyword = await createKeyword({ title: '' });

    let data = {
        number: 0,
        itemTypeKeywords: itemType.keywords.map(item => ({ keyword: item._id, isDone: false })),
        customKeyword: {
            keyword: customKeyword._id,
            isDone: false
        }
    };
    return await (new ScopeKeyword(data)).save();
}

export async function createScope(scope, milestoneNumber = 0, isRevScope) {

    const promises = [
        createCalc(milestoneNumber),
        createDrawing(milestoneNumber)
    ];

    !isRevScope && promises.push(createScopeKeyword(scope.itemType));

    const data = await Promise.all(promises).catch(() => { throw 'error in scope initial setup' });

    const calc = data[0];
    const drawing = data[1];

    if (!drawing || !calc) {
        throw 'error in scope initial setup'
    }
    scope.drawings = [drawing._id];
    scope.calcs = [calc._id];

    if (!isRevScope) {
        const scopeKeyword = data[2];
        if (!scopeKeyword) {
            throw 'error in scope initial setup'
        }
        scope.scopeKeywords = [scopeKeyword._id];
    }

    return (new Scope(scope)).save();
}

export async function createTaskGroupScope(scope, milestoneNumber = 0) {
    const scopePlanning = await createScopePlanning(milestoneNumber).catch(() => { throw 'error in scope initial setup' });
    if (!scopePlanning) {
        throw 'error in scope initial setup'
    }
    scope.scopePlannings = [scopePlanning._id];
    return (new Scope(scope)).save();
}

export async function createPurchaseOrder(taskID, bodyPurchaseOrder = null) {
    const { po } = await incrementPOCounter(taskID);
    const purchaseOrder = {
        name: bodyPurchaseOrder && bodyPurchaseOrder.name || '',
        notes: bodyPurchaseOrder && bodyPurchaseOrder.notes || '',
        signature: bodyPurchaseOrder && bodyPurchaseOrder.signature || '',
        selectedScopes: [],
        number: po,
        templates: [
            {
                isDone: false,
                isAttachment: false,
                title: 'Scope:'
            },
            {
                isDone: false,
                isAttachment: false,
                title: 'PO requested'
            },
            {
                isDone: false,
                isAttachment: false,
                title: 'Received'
            },
            {
                isDone: false,
                isAttachment: false,
                title: 'PO'
            },
            {
                isDone: false,
                isAttachment: false,
                title: 'Amount'
            }
        ]
    };
    return (new PurchaseOrder(purchaseOrder)).save()
}

export async function createInvoice(taskID, fields) {
    const { invoice } = await incrementInvoiceCounter(taskID);
    const data = {
        number: invoice,
        ...fields
    };

    if (!data.templates) {
        data.templates = [
            {
                title: "Scope:",
                isDone: false,
                isAttachment: false
            },
            {
                title: "Amount:",
                isDone: false,
                isAttachment: false
            },
            {
                title: "Draft created",
                isDone: false,
                isAttachment: false
            },
            {
                title: "Submitted to DHC accounting",
                isDone: false,
                isAttachment: false
            },
            {
                title: "On Hold",
                isDone: false,
                isAttachment: false
            },
            {
                title: "Submitted to customer",
                isDone: false,
                isAttachment: false
            },
            {
                "title": "Paid",
                "isDone": false,
                "isAttachment": false
            }
        ]
    }
    return (new Invoice(data)).save()
}

export async function createAgreement(taskID, fields, val) {
    const { agreement } = await incrementAgreementCounter(taskID);
    const data = {
        number: agreement,
        templates: [
            {
                title: "Scope:",
                isDone: val,
                isAttachment: false
            },
            {
                title: "Draft created",
                isDone: false,
                isAttachment: false
            },
            {
                title: "Emailed to customer",
                isDone: false,
                isAttachment: true
            },
            {
                title: "CSA signed",
                isDone: false,
                isAttachment: false
            }
        ],
        ...fields
    };
    return (new Agreement(data)).save()
}

export async function createMasterAgreement(taskID, fields) {
    const { masterAgreement } = await incrementMasterAgreementCounter(taskID);
    const data = {
        number: masterAgreement,
        templates: [
            {
                title: 'Scope:',
                isDone: true,
                isAttachment: false
            }, {
                title: 'Signed',
                isDone: true,
                isAttachment: false
            }
        ],
        ...fields
    };
    return (new MasterAgreement(data)).save()
}

export async function createModifiedAgreement(taskID, fields) {
    const { modifiedAgreement } = await incrementModifiedAgreementCounter(taskID);
    const data = {
        number: modifiedAgreement,
        templates: [
            {
                title: 'Scope:',
                isDone: true,
                isAttachment: false
            },
            {
                title: 'Draft created',
                isDone: false,
                isAttachment: false
            },
            {
                title: 'Emailed to customer',
                isDone: false,
                isAttachment: true
            },
            {
                title: 'MCSA signed',
                isDone: false,
                isAttachment: false
            }
        ],
        ...fields
    };
    return (new ModifiedAgreement(data)).save()
}

export async function createClientAgreement(taskID, fields) {
    const { clientAgreement } = await incrementClientAgreementCounter(taskID);
    const data = {
        number: clientAgreement,
        templates: [
            {
                title: 'Scope:',
                isDone: true,
                isAttachment: false
            }, {
                title: 'Signed',
                isDone: false,
                isAttachment: false
            }
        ],
        ...fields
    };
    return (new ClientAgreement(data)).save()
}

export async function addTaskMilestone(type, id, milestone) {
    const select = {
        id: 1
    };
    let task = await Task.findOneAndUpdate({ id }, { $push: { [type]: milestone._id } }, { new: true, select }).lean().exec();
    task[type] = [milestone];
    return task;
}

export async function removeTaskMilestone(type, id, milestone) {
    const select = {
        id: 1
    };
    return Task.findOneAndUpdate({ id }, { $pull: { [type]: milestone._id } }, { new: true, select }).lean().exec();
}
