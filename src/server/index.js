import bugsnag from '@bugsnag/js';
import * as bugsnagExpress from '@bugsnag/plugin-express';
import bodyParser from 'body-parser';
import compression from 'compression';
import timeout from 'connect-timeout';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import express from 'express';
import { Server } from 'http';
import jwt from 'jsonwebtoken';
import _ from 'lodash';
import mongoose from 'mongoose';
import morgan from 'morgan';
import path from 'path';
// import expressValidator from 'express-validator';

import accessibility from './api/accessibility';
import agreement from './api/agreement';
import agreement_new from './api/agreement_new';
import appSetting from './api/appSetting';
import box from './api/box';
import calc from './api/calc';
import clientAgreement from './api/clientAgreement';
import clientAgreement_new from './api/clientAgreement_new';
import counter from './api/counter';
import custDrawing from './api/custDrawing';
import customerImage from './api/customerImage';
import drawing from './api/drawing';
import ftp from './api/ftp';
import ftp_new from './api/ftp_new';
import hourTracker from './api/hourTracker';
import hourTracker_new from './api/hourTracker_new';
import invoice from './api/invoice';
import invoice_new from './api/invoice_new';
import itemType from './api/itemType';
import keyword from './api/keyword';
import letter from './api/letter';
import location from './api/location';
import masterAgreement from './api/masterAgreement';
import masterAgreement_new from './api/masterAgreement_new';
import modifiedAgreement from './api/modifiedAgreement';
import modifiedAgreement_new from './api/modifiedAgreement_new';
import office from './api/office';
import permission from './api/permission';
import purchaseOrder from './api/purchaseOrder';
import reports from './api/reports';
import role from './api/role';
import roleLevel from './api/roleLevel';
import scope from './api/scope';
import scopeNew from './api/scope_new';
import scopeKeyword from './api/scopeKeyword';
import scopePlanning from './api/scopePlanning';
import sftp from './api/sftp';
import slack from './api/slack';
import tabData from './api/tabData';
import tag from './api/tag';
import task from './api/task';
import taskNew from './api/task_new';
import taskGroup from './api/taskGroup';
import timeInTimeOut from './api/timeInTimeOut';
import utilApi from './api/utilApi';
import user from './api/user';
import workload from './api/workload';
import Accessibility from './models/Accessibility';
import User from './models/User';
import constants from './utils/constants';
import logger from './utils/logger';

if (process.env.NODE_ENV !== 'production') {
    dotenv.load();
}
let app = express();
let server = Server(app);
let io = require('socket.io')(server);
app.set('socketio', io);

app.use(compression());
app.use(timeout('80s'));
app.use(express.static('dist'));
app.use(express.static('public'));
// app.use(expressValidator());

app.use(morgan('dev', {
    stream: logger.stream
}));

app.use(cookieParser());
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    limit: '20mb',
    extended: true
}));

// configure our app to handle CORS requests
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers',
        'Content-Type, Content-Disposition, Content-Description, Content-Range, Authorization, X-Requested-With, Cache-Control, Accept, Origin, X-Session-ID, socketID');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

// redirect to https on production
app.use(function (req, res, next) {
    let sslUrl;
    if (process.env.NODE_ENV === 'production' &&
        req.headers['x-forwarded-proto'] !== 'https') {
        sslUrl = ['https://', req.hostname, req.url].join('');
        return res.redirect(sslUrl);
    }
    return next();
});

app.use(function (req, res, next) {
    req.isAuthenticated = () => {
        try {
            let authToken = req.headers.authorization ? req.headers.authorization.split(' ')[1] : '';
            return jwt.verify(authToken, process.env.TOKEN_SECRET);
        } catch (err) {
            return false;
        }
    };
    // next();

    let endpoints = [
        '/api/accessibilities',
        '/api/app-settings',
        '/api/itemTypes',
        '/api/locations',
        '/api/roles',
        '/api/roleLevels',
        '/api/Users',
        '/api/workloads'
        // '/api/users'
    ];
    const endpoint = endpoints.find(item => req.url.includes(item));

    if (endpoint && (req.method === 'PUT' || req.method === 'POST')) {
        if (req.isAuthenticated()) {
            let payload = req.isAuthenticated();
            User.findOne({
                id: payload.sub,
                isActive: true
            })
                .populate('role')
                .lean()
                .exec(function (err, user) {
                    if (user) {
                        Accessibility
                            .findOne({
                                role: user.role._id,
                                level: user.roleLevel
                            })
                            .populate('permissions.permission')
                            .exec(function (err, access) {
                                if (access) {
                                    user.permissions = access.permissions;
                                }
                                req.user = user;
                                next();
                            });
                    } else {
                        res.status(401).send('User is Unauthorized');
                    }
                });
        } else {
            res.status(401).send('User is Unauthorized');
        }
    } else {
        next();
    }
});

// routes
app.use('/api/users', user);
app.use('/api/permissions', permission);
app.use('/api/roles', role);
app.use('/api/locations', location);
app.use('/api/workloads', workload);
app.use('/api/app-settings', appSetting);
app.use('/api/roleLevels', roleLevel);
app.use('/api/itemTypes', itemType);
app.use('/api/accessibilities', accessibility);
app.use('/api/box', box);
app.use('/api/task-groups', taskGroup);
app.use('/api/tasks', task);
app.use('/api/scope', scope);
app.use('/api/office', office);
app.use('/api/hourTracker', hourTracker);
app.use('/api/slack', slack);
app.use('/api/ftp', ftp);
app.use('/api/tag', tag);
app.use('/api/purchaseOrder', purchaseOrder);
app.use('/api/agreement', agreement);
app.use('/api/modifiedAgreement', modifiedAgreement);
app.use('/api/masterAgreement', masterAgreement);
app.use('/api/clientAgreement', clientAgreement);
app.use('/api/invoice', invoice);
app.use('/api/drawing', drawing);
app.use('/api/custDrawing', custDrawing);
app.use('/api/letter', letter);
app.use('/api/tabData', tabData);
app.use('/api/calc', calc);
app.use('/api/scopePlanning', scopePlanning);
app.use('/api/sftp', sftp);
app.use('/api/counter', counter);
app.use('/api/reports', reports);
app.use('/api/customerImage', customerImage);
app.use('/api/scope_new', scopeNew);
app.use('/api/task_new', taskNew);
app.use('/api/timeInTimeOut', timeInTimeOut);
app.use('/api/hourTracker_new', hourTracker_new);
app.use('/api/ftp_new', ftp_new);
app.use('/api/invoice_new', invoice_new);
app.use('/api/agreement_new', agreement_new);
app.use('/api/clientAgreement_new', clientAgreement_new);
app.use('/api/masterAgreement_new', masterAgreement_new);
app.use('/api/modifiedAgreement_new', modifiedAgreement_new);
app.use('/api/keyword', keyword);
app.use('/api/scopeKeyword', scopeKeyword);
app.use('/api/utilApi', utilApi);

mongoose.connect(process.env.MONGO_URI,
    {
        server: {
            socketOptions: {
                socketTimeoutMS: 0,
                connectionTimeout: 0
            }
        },
        useNewUrlParser: true

    })
    .then(() => console.log('Succeeded connected to: ' + process.env.MONGO_URI))
    .catch(error => console.log('ERROR connecting to: ' + process.env.MONGO_URI + '. ' + error));

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close().then(() => process.exit(0));
});

// app.use('/*', (req, res) => res.sendFile(path.resolve('dist/index.html')));

io.on('connection', client => {
    console.log('socket io connection successfull, socket id : ' + client.id);
    client.on('login', function (userId) {
        console.log('new logged in user: ' + userId);
        client.userId = userId;
        client.broadcast.emit('user singin', { userId: userId });
        let userIds = _.map(io.sockets.sockets, userSocket => userSocket.userId);
        console.log('online users', userIds);
        setTimeout(() => io.emit('users singin', { userIds: userIds }), 10000);
    });

    client.on('disconnect', () => {
        let userExists = _.find(io.sockets.sockets, { userId: client.userId });
        if (!userExists) {
            io.emit('user singout', { userId: client.userId });
        }
    });
});

app.use(function (err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    if (app.get('env') !== 'development') {
        delete err.stack;
    }
    res.send(constants.RESPONSE_MESSAGES.ERROR);
});

const BUGSNAG_API_KEY = '51ebcfd49166bfe382546051ab18f49a';
const bugsnagClient = bugsnag(BUGSNAG_API_KEY);
bugsnagClient.use(bugsnagExpress);
const bugsnagMiddleware = bugsnagClient.getPlugin('express');
if (app.get('env') !== 'development') {
    app.use(bugsnagMiddleware.requestHandler);
    app.use(bugsnagMiddleware.errorHandler);
}

server.listen(process.env.PORT || 8080, function () {
    console.log('Express server listening on port ' + (process.env.PORT || 8080));
});

module.exports = app;

