var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

var taskCounterSchema = new mongoose.Schema({
    nextTask: {
        type: Number,
        default: 0
    },
    nextBiddingTask: {
        type: Number,
        default: 0
    },
    nextTaskGroupTask: {
        type: Number,
        default: 0
    },

    _id: String
    
}, schemaOptions);

var TaskCounter = mongoose.model('TaskCounter', taskCounterSchema);

module.exports = TaskCounter;
