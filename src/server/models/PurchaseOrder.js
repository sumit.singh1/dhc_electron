var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

let purchaseOrderSchema = new mongoose.Schema({
    id: String,
    number: Number,
    name: String,
    notes: String,
    signature: String,
    poValue: String,        // Purchase Order value  *New Field*
    amountValue: Number,     // Amount Value *New Field*
    templates: [{
        title: {
            type: String,
            required: true
        },
        isDone: {
            type: Boolean,
            default: false
        },
        isAttachment: {
            type: Boolean,
            default: false
        }
    }],
    isArchived: {
        type: Boolean,
        default: false
    },
    selectedScopes: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Scope',
            required: true
        }
    ],
    isUpdated: false
}, schemaOptions);

purchaseOrderSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var PurchaseOrder = mongoose.model('PurchaseOrder', purchaseOrderSchema);

module.exports = PurchaseOrder;
