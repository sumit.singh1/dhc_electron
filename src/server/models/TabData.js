var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var tabDataSchema = new mongoose.Schema({
    id: String,
    number: Number,
    templates: [{
        title: {
            type: String,
            required: true
        },
        isDone: {
            type: Boolean,
            default: false
        },
        isAttachment: {
            type: Boolean,
            default: false
        }
    }],
    isArchived: {
        type: Boolean,
        default: false
    }
}, schemaOptions);

tabDataSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var TabData = mongoose.model('TabData', tabDataSchema);

module.exports = TabData;
