var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var clientAgreementSchema = new mongoose.Schema({
    id: String,
    number: Number,
    templates: [{
        title: {
            type: String,
            required: true
        },
        isDone: {
            type: Boolean,
            default: false
        }
    }],
    isArchived: {
        type: Boolean,
        default: false
    },
    isUpdated: {
        type: Boolean,
        default: false
    },
    selectedScopes: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Scope',
            required: true
        }
    ]

}, schemaOptions);

clientAgreementSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var ClientAgreement = mongoose.model('ClientAgreement', clientAgreementSchema);

module.exports = ClientAgreement;
