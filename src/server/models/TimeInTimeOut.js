const mongoose = require('mongoose');
const shortid = require('shortid');

const timeInTimeOut = new mongoose.Schema({
    id: String,
    timeIn: {
        type: Number,
        default: 0
    },
    timeOut: {
        type: Number,
        default: 0
    },
    lunchIn: {
        type: Number,
        default: 0
    },
    lunchOut: {
        type: Number,
        default: 0
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    date: {
        type: Number,
        required: true
    }
}, { timestamps: true });

timeInTimeOut.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

const TimeInTimeOut = mongoose.model('TimeInTimeOut', timeInTimeOut);
module.exports = TimeInTimeOut;
