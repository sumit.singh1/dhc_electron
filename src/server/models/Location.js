var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

var locationSchema = new mongoose.Schema({
    id: String,
    name: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        required: true
    },
    iconColor: String
}, schemaOptions);

locationSchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var Location = mongoose.model('Location', locationSchema);

module.exports = Location;
