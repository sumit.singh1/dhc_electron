var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

var appSettingSchema = new mongoose.Schema({
    id: {
        type: String,
        unique: true
    },
    taskFormName: String,
    slackTeamId: {
        type: String
    },
    attachments: [
        {
            name: {
                type: String,
                required: true
            }
        }
    ]
}, schemaOptions);

appSettingSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var AppSetting = mongoose.model('AppSetting', appSettingSchema);

module.exports = AppSetting;
