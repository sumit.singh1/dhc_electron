let mongoose = require('mongoose');
let shortid = require('shortid');

let schemaOptions = {
    timestamps: true
};

let scopeSchema = new mongoose.Schema({
    number: String,
    id: String,
    group: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TaskGroup',
        required: true
    },
    engineerDetails: {
        engineer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        status: String,
        urgentHours: Number,
        nonUrgentHours: Number
    },
    drafterDetails: {
        drafter: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        status: String,
        urgentHours: Number,
        nonUrgentHours: Number
    },
    managerDetails: {
        manager: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        urgentHours: {
            type: Number,
            default: 0
        },
        nonUrgentHours: {
            type: Number,
            default: 0
        }
    },
    definition: {
        type: String,
        default: ''
    },
    note: {
        type: String
    },
    drawings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Drawing',
        required: true
    }],
    custDrawings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CustDrawing'
    }],
    letters: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Letter'
    }],
    tabData: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'TabData'
    }],
    calcs: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Calc',
        required: true
    }],
    scopePlannings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ScopePlanning'
    }],
    price: Number,
    createdAt: {
        type: Date,
        default: new Date()
    },
    dueDate: Date,
    completedDate: {
        type: Date,
        default: null
    },
    itemType: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ItemType'
    },
    scopeKeywords: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ScopeKeyword'
    }],
    customerContact: {
        name: {
            type: String
        },
        id: {
            type: String
        }
    },
    hourTrackers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'HourTracker'
        }
    ],
    isArchived: {
        type: Boolean,
        default: false
    },
    task: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Task'
    },
    additionalNotes: String,
    status: String,
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Scope'
    },
    highlights: [
        {
            user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            color: {
                type: String,
                required: true
            }
        }
    ]
}, schemaOptions);

scopeSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate();
    }
    next();
});

let Scope = mongoose.model('Scope', scopeSchema);

module.exports = Scope;
