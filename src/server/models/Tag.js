let mongoose = require('mongoose');
let shortid = require('shortid');

let schemaOptions = {
    timestamps: true
};

let tagSchema = new mongoose.Schema({
    id: String,
    name: {
        type: String,
        required: true
    }
}, schemaOptions);

tagSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate();
    }
    next();
});

let Tag = mongoose.model('Tag', tagSchema);

module.exports = Tag;
