var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var itemTypeSchema = new mongoose.Schema({
    id: String,
    name: {
        type: String,
        required: true
    },
    keywords: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Keyword'
    }],
}, schemaOptions);

itemTypeSchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var ItemType = mongoose.model('ItemType', itemTypeSchema);

module.exports = ItemType;
