var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

var roleLevelSchema = new mongoose.Schema({
    id: String,
    name: {
        type: String,
        required: true
    }
}, schemaOptions);

roleLevelSchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var RoleLevel = mongoose.model('RoleLevel', roleLevelSchema);

module.exports = RoleLevel;
