let mongoose = require('mongoose');
let shortid = require('shortid');

let schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

let taskSchema = new mongoose.Schema({
    id: String,
    taskNumber: String,
    title: {
        type: String,
        required: true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    teamMembers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        }
    ],
    comments: [
        {
            id: {
                type: String,
                required: true
            },
            postedBy: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            date: {
                type: Date,
                required: true
            },
            comment: {
                type: String,
                required: true
            },
            isEdited: {
                type: Boolean,
                default: false
            }
        }
    ],
    tags: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Tag'
        }
    ],
    attachments: [
        {
            attachmentId: String,
            files: [
                {
                    name: String
                }
            ]
        }
    ],
    remainingHours: {
        type: Number,
        default: 0
    },
    contractedHours: {
        type: Number,
        default: 0
    },
    contractor: {
        name: {
            type: String
        },
        id: {
            type: String
        },
        // companyNameFromNotes: {
        //     type: String
        // },
        company: {
            type: String
        },
        billBranch: {
            type: String,
            default: ''
        },
        poRequired: {
            type: Boolean
        },
        includeContacts: {
            type: Boolean
        },
        invoiceContact: {
            type: String
        },
        includeClientJobNo: {
            type: Boolean
        },
        contract: {
            type: String,
            default: ''
        },
        updatedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        updatedAt: {
            type: Date
        }
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    additionalNote: String,
    slackChannelId: String,
    scopes: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Scope',
            required: true
        }
    ],
    purchaseOrders: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'PurchaseOrder'
    }],
    agreements: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Agreement'
    }],
    modifiedAgreements: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ModifiedAgreement'
    }],
    masterAgreements: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'MasterAgreement'
    }],
    clientAgreements: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ClientAgreement'
    }],
    invoices: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Invoice'
    }],
    isBidding: {
        type: Boolean,
        default: false
    },
    isFromTaskGroup: {
        type: Boolean,
        default: false
    }
}, schemaOptions);

taskSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

let Task = mongoose.model('Task', taskSchema);

module.exports = Task;
