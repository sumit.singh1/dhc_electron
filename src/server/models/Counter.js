let mongoose = require('mongoose');

let schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

let counterSchema = new mongoose.Schema({
    taskID: String,
    po: {
        type: Number,
        default: 0
    },
    invoice: {
        type: Number,
        default: 0
    },
    agreement: {
        type: Number,
        default: 0
    },
    modifiedAgreement: {
        type: Number,
        default: 0
    },
    masterAgreement: {
        type: Number,
        default: 0
    },
    clientAgreement: {
        type: Number,
        default: 0
    },
    scopeNumber: {
        type: Number,
        default: 0
    }

}, schemaOptions);

let Counter = mongoose.model('Counter', counterSchema);

module.exports = Counter;
