var mongoose = require('mongoose');

var schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

var revScopeounterSchema = new mongoose.Schema({
    scopeID: String,
    nextRev: {
        type: Number,
        default: 0
    }
    
}, schemaOptions);

var RevScopeCounter = mongoose.model('RevScopeCounter', revScopeounterSchema);

module.exports = RevScopeCounter;
