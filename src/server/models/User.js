const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const shortid = require('shortid');

const schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

const userSchema = new mongoose.Schema({
    id: String,
    employeeCode: String,
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: false
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    picture: String,
    password: {
        type: String,
        required: true
    },
    title: {
        type: String,
        default: ''
    },
    signature: String,
    initials: String,
    titleBlock: String,
    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
        required: true
    },
    roleLevel: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RoleLevel'
    },
    locationInfo: {
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        },
        extension: String
        // from: {
        //     type: Date
        // },
        // to: {
        //     type: Date
        // }
    },
    availability: String,
    skypeId: String,
    slackId: String,
    scopeHighlights: [
        {
            scope: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Scope'
            },
            color: String
        }
    ],
    scopesSortBy: [
        {
            group: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'TaskGroup'
            },
            sortBy: String,
            ascending: Boolean
        }
    ],
    passwordResetToken: String,
    passwordResetExpires: Date,
    payDayFormDate: {
        type: Date,
        default: new Date("2018-01-01T00:00:00.000Z")
    },
    companyName: {
        type: String,
        default: ''
    },
    companyId: {
        type: String,
        default: ''
    },
    canViewPaydayReport: {
        type: Boolean,
        default: false
    },
    scopeVisibilityPermission: [],
    refreshToken: String
    

}, schemaOptions);

userSchema.pre('save', function (next) {
    const user = this;

    if (!user.id) {
        user.id = shortid.generate() + shortid.generate();
    }

    if (!user.isModified('password')) {
        return next();
    }

    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, null, function (err, hash) {
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        cb(err, isMatch);
    });
};

userSchema.options.toJSON = {
    transform: function (doc, ret) {
        delete ret.password;
        delete ret.passwordResetToken;
        delete ret.passwordResetExpires;
    }
};

const User = mongoose.model('User', userSchema);

module.exports = User;
