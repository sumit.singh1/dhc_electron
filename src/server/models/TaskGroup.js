var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var taskGroupSchema = new mongoose.Schema({
    id: String,
    title: {
        type: String,
        required: true
    }
}, schemaOptions);

taskGroupSchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var TaskGroup = mongoose.model('TaskGroup', taskGroupSchema);

module.exports = TaskGroup;
