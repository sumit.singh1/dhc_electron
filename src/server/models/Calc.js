let mongoose = require('mongoose');
let shortid = require('shortid');

let schemaOptions = {
    timestamps: true
};

let CalcSchema = new mongoose.Schema({
    id: String,
    number: Number,
    templates: [{
        title: {
            type: String,
            required: true
        },
        isDone: {
            type: Boolean,
            default: false
        },
        isAttachment: {
            type: Boolean,
            default: false
        }
    }],
    isArchived: {
        type: Boolean,
        default: false
    }
}, schemaOptions);

CalcSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate();
    }
    next();
});

let Calc = mongoose.model('Calc', CalcSchema);

module.exports = Calc;
