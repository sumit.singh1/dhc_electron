var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var keywordSchema = new mongoose.Schema({
    id: String,
    title: {
        type: String,
        // required: true,
        default: ''
    },
}, schemaOptions);

keywordSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate()
    }
    next();
});

var Keyword = mongoose.model('Keyword', keywordSchema);

module.exports = Keyword;
