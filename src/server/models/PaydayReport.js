const mongoose = require('mongoose');
const shortid = require('shortid');

const paydayReport = new mongoose.Schema({
    id: String,
    employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    payDate: {
        type: Date,
        default: null
    },
    note: {
        type: String,
        default: ''
    }
}, { timestamps: true });

paydayReport.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

const PaydayReport = mongoose.model('PaydayReport', paydayReport);

module.exports = PaydayReport;
