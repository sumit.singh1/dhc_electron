import mongoose from 'mongoose';
import shortid from 'shortid';

const schemaOptions = {
    timestamps: true
};

const invoiceSchema = new mongoose.Schema({
    id: String,
    number: Number,
    hold: {
        type: String,
        default: 'N'
    },
    sent: {
        type: String,
        default: 'N'
    },
    totalCost: Number,
    toAccounting: {
        type: String,
        default: 'N'
    },
    paid: {
        type: String,
        default: 'N'
    },
    poNumber: String,
    isVisibleOnAdminPage: {
        type: Boolean,
        default: false
    },
    templates: [{
        title: {
            type: String,
            required: true
        },
        isDone: {
            type: Boolean,
            default: false
        }
    }],
    isArchived: {
        type: Boolean,
        default: false
    },
    lastModifiedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    contact: {
        type: String,
        default: ''
    },
    clientJobNo: String,
    company: String,
    selectedScopes: [
        {
            scope: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Scope',
                required: true
            },
            isPartial: {
                type: Boolean,
                default: false
            },
            amount: {
                type: Number,
                default: 0
            },
            description: {
                type: String,
                default: ''
            },
            oldPrice: {
                type: Number,
                default: 0
            }
        }
    ],
    sentDate: {
        type: Date,
        default: null
    },
    paidDate: {
        type: Date,
        default: null
    },
    isUpdated: {
        type: Boolean,
        default: false
    },
    isDownloaded: {
        type: Boolean,
        default: false
    }
}, schemaOptions);

invoiceSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

const Invoice = mongoose.model('Invoice', invoiceSchema);

module.exports = Invoice;
