var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
};

var workloadSchema = new mongoose.Schema({
    id: {
      type: String,
      unique: true,
    },
    name: {
        type: String,
        required: true
    },
    color: String
}, schemaOptions);

workloadSchema.pre('save', function(next) {
    if(!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var Workload = mongoose.model('Workload', workloadSchema);

module.exports = Workload;
