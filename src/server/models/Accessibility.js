var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var accessibilitySchema = new mongoose.Schema({
    id: String,
    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Role',
        required: true
    },
    level: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RoleLevel'
    },
    permissions: [{
        permission: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Permission'
        },
        read: {
            type: Boolean,
            default: false
        },
        write: {
            type: Boolean,
            default: false
        }
    }]
}, schemaOptions);

accessibilitySchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var Accessibility = mongoose.model('Accessibility', accessibilitySchema);

module.exports = Accessibility;
