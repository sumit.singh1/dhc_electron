var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var ScopePlanningSchema = new mongoose.Schema({
    id: String,
    number: Number,
    title: {type: String, default: 'Task'},
    templates: [{
        title: {
            type: String,
            required: true
        },
        isDone: {
            type: Boolean,
            default: false
        },
        isAttachment: {
            type: Boolean,
            default: false
        }
    }],
    isArchived: {
        type: Boolean,
        default: false
    }
}, schemaOptions);

ScopePlanningSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var ScopePlanning = mongoose.model('ScopePlanning', ScopePlanningSchema);

module.exports = ScopePlanning;
