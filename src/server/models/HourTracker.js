var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var hourTrackerSchema = new mongoose.Schema({
    id: String,
    employee: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    hoursSpent: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    note: String,
    other: {
        type: String,
        default: ''
    },
    revNumber: String
}, schemaOptions);

hourTrackerSchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var HourTracker = mongoose.model('HourTracker', hourTrackerSchema);

module.exports = HourTracker;
