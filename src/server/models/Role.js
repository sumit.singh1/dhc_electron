var mongoose = require('mongoose');
var shortid = require('shortid');

var schemaOptions = {
    timestamps: true
};

var roleSchema = new mongoose.Schema({
    id: String,
    name: {
        type: String,
        required: true
    },
    workload: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Workload'
    }
}, schemaOptions);

roleSchema.pre('save', function(next) {
    if (!this.id) {
        this.id = shortid.generate() + shortid.generate();
    }
    next();
});

var Role = mongoose.model('Role', roleSchema);

module.exports = Role;
