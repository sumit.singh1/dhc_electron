let mongoose = require('mongoose');
let shortid = require('shortid');

let schemaOptions = {
    timestamps: true
};


let customerImageSchema = new mongoose.Schema({
    id: String,
    name: String,
    logo: String,
    poster: String
});

customerImageSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate();
    }
    next();
}, schemaOptions);

let customerImage = mongoose.model('customerImage', customerImageSchema);

module.exports = customerImage;
