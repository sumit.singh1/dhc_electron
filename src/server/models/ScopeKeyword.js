let mongoose = require('mongoose');
let shortid = require('shortid');

let schemaOptions = {
    timestamps: true
};

let ScopeKeywordSchema = new mongoose.Schema({
    id: String,
    number: Number,
    enabled: {
        type: Boolean,
        default: false
    },
    itemTypeKeywords: [{
        keyword: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Keyword'
        },
        isDone: false
    }],
    customKeyword: {
        keyword: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Keyword'
        },
        isDone: false
    },
    isArchived: {
        type: Boolean,
        default: false
    }
}, schemaOptions);

ScopeKeywordSchema.pre('save', function (next) {
    if (!this.id) {
        this.id = shortid.generate();
    }
    next();
});

let ScopeKeyword = mongoose.model('ScopeKeyword', ScopeKeywordSchema);

module.exports = ScopeKeyword;
