echo "Let's get into the dev Directory....!!!"
cd dev/teamzy
pwd 

echo "Statshing changes...!!"
git stash save
echo ""

echo "Pulling latest..!!"
git pull origin development
echo ""

echo "Let's check the node version...!!!"
nodejs -v

echo "Removing Previous build if exist..!!"
sudo rm -rf build
echo ""

echo "Let's install the newly added modules..!!"
sudo npm install
echo ""

echo "Starting the build..!!"
sudo npm run build

if [ $? -eq 0 ]
then
    echo "Build Succeeded"
else
    echo "Something went wrong...!!"
    echo "Exiting Now..!!"
    exit 1