echo "Deployment of UI started for new-scope-table Branch...!!"
echo ""

set -e
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

chmod 400 deploy/PLP_linux_server.pem

ssh -i deploy/PLP_linux_server.pem ubuntu@ec2-54-193-0-31.us-west-1.compute.amazonaws.com < ./deploy/updateAndRestart.sh

echo ""
echo "Deployed Successfully...!!"